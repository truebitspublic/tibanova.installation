-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: apps.truebitstech.com    Database: tibanova
-- ------------------------------------------------------
-- Server version	5.7.30-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_admission`
--

DROP TABLE IF EXISTS `tbl_admission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_admission` (
  `Admision_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Hospital_Ward_ID` int(11) DEFAULT NULL,
  `Bed_ID` int(11) DEFAULT NULL,
  `Admission_Employee_ID` int(11) DEFAULT NULL,
  `Admission_Supervisor_ID` int(11) DEFAULT NULL,
  `Discharge_Employee_ID` int(11) DEFAULT NULL,
  `Discharge_Supervisor_ID` int(11) DEFAULT NULL,
  `Admission_Date_Time` datetime DEFAULT NULL,
  `Discharge_Date_Time` datetime DEFAULT NULL,
  `Admission_Status` varchar(20) DEFAULT NULL,
  `Discharge_Clearance_Status` varchar(20) NOT NULL DEFAULT 'not cleared',
  `Clearance_Date_Time` datetime DEFAULT NULL,
  `Cash_Bill_Status` varchar(20) NOT NULL DEFAULT 'pending',
  `Credit_Bill_Status` varchar(20) NOT NULL DEFAULT 'pending',
  `Cash_Clearer_ID` int(11) DEFAULT NULL COMMENT 'Employee cleared Cash bill',
  `Credit_Clearer_ID` int(11) DEFAULT NULL COMMENT 'Employee cleared Credit bill',
  `Discharge_Reason_ID` int(11) DEFAULT NULL,
  `pending_set_time` datetime DEFAULT NULL,
  `pending_setter` int(11) DEFAULT NULL,
  `District` varchar(50) DEFAULT NULL,
  `Ward` varchar(50) NOT NULL,
  `Admission_Claim_Form_Number` varchar(30) DEFAULT NULL,
  `Discharge_Claim_Form_Number` varchar(20) DEFAULT NULL,
  `Folio_Number` int(11) NOT NULL,
  `Office_Area` varchar(50) DEFAULT NULL,
  `Office_Plot_Number` varchar(50) DEFAULT NULL,
  `Office_Street` varchar(50) DEFAULT NULL,
  `Office_Phone` varchar(50) DEFAULT NULL,
  `Kin_Name` varchar(60) NOT NULL,
  `Kin_Relationship` varchar(20) NOT NULL,
  `Kin_Phone` varchar(20) NOT NULL,
  `Kin_Area` varchar(50) DEFAULT NULL,
  `Kin_Street` varchar(30) DEFAULT NULL,
  `Kin_Plot_Number` varchar(30) DEFAULT NULL,
  `Kin_Address` varchar(30) DEFAULT NULL,
  `Check_In_ID` int(11) NOT NULL,
  `Cash_Clearance_Date_Time` datetime DEFAULT NULL,
  `Credit_Clearance_Date_Time` datetime DEFAULT NULL,
  `Approval_Date_Time` datetime DEFAULT NULL,
  `Approver_ID` int(11) DEFAULT NULL,
  `by_pass_cash_bill` varchar(1) DEFAULT '0',
  PRIMARY KEY (`Admision_ID`),
  KEY `Hospital_Ward_ID` (`Hospital_Ward_ID`),
  KEY `District_ID` (`District`),
  KEY `Admission_Employee_ID` (`Admission_Employee_ID`),
  KEY `Admission_Supervisor_ID` (`Admission_Supervisor_ID`),
  KEY `Discharge_Employee_ID` (`Discharge_Employee_ID`),
  KEY `Discharge_Supervisor_ID` (`Discharge_Supervisor_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Discharge_Reason_ID` (`Discharge_Reason_ID`),
  KEY `Hospital_Ward_ID_2` (`Hospital_Ward_ID`),
  KEY `Bed_ID` (`Bed_ID`),
  KEY `Admission_Employee_ID_2` (`Admission_Employee_ID`),
  KEY `Admission_Supervisor_ID_2` (`Admission_Supervisor_ID`),
  KEY `Discharge_Employee_ID_2` (`Discharge_Employee_ID`),
  KEY `Discharge_Supervisor_ID_2` (`Discharge_Supervisor_ID`),
  KEY `Cash_Clearer_ID` (`Cash_Clearer_ID`),
  KEY `Credit_Clearer_ID` (`Credit_Clearer_ID`),
  KEY `pending_setter` (`pending_setter`),
  KEY `admission_check_in_id_detials_foreign` (`Check_In_ID`),
  CONSTRAINT `admission_check_in_detials_id_foreign` FOREIGN KEY (`Check_In_ID`) REFERENCES `tbl_check_in_details` (`Check_In_ID`),
  CONSTRAINT `tbl_admission_ibfk_1` FOREIGN KEY (`Registration_ID`) REFERENCES `tbl_patient_registration` (`Registration_ID`),
  CONSTRAINT `tbl_admission_ibfk_10` FOREIGN KEY (`Credit_Clearer_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_admission_ibfk_2` FOREIGN KEY (`Hospital_Ward_ID`) REFERENCES `tbl_hospital_ward` (`Hospital_Ward_ID`),
  CONSTRAINT `tbl_admission_ibfk_3` FOREIGN KEY (`Bed_ID`) REFERENCES `tbl_beds` (`Bed_ID`),
  CONSTRAINT `tbl_admission_ibfk_4` FOREIGN KEY (`Admission_Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_admission_ibfk_5` FOREIGN KEY (`Discharge_Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_admission_ibfk_6` FOREIGN KEY (`Discharge_Supervisor_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_admission_ibfk_7` FOREIGN KEY (`Discharge_Reason_ID`) REFERENCES `tbl_discharge_reason` (`Discharge_Reason_ID`),
  CONSTRAINT `tbl_admission_ibfk_8` FOREIGN KEY (`Admission_Supervisor_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_admission_ibfk_9` FOREIGN KEY (`Cash_Clearer_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_approval_employee`
--

DROP TABLE IF EXISTS `tbl_approval_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_approval_employee` (
  `Employee_ID` int(11) NOT NULL DEFAULT '0',
  `Approval_ID` int(11) NOT NULL DEFAULT '0',
  `Procurement_Type` varchar(50) NOT NULL,
  PRIMARY KEY (`Employee_ID`,`Approval_ID`),
  KEY `Approval_ID` (`Approval_ID`),
  CONSTRAINT `tbl_approval_employee_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_approval_employee_ibfk_2` FOREIGN KEY (`Approval_ID`) REFERENCES `tbl_approval_level` (`Approval_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_approval_level`
--

DROP TABLE IF EXISTS `tbl_approval_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_approval_level` (
  `Approval_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Approval_Title` varchar(30) NOT NULL,
  `Approval_Number` int(11) NOT NULL,
  `Procurement_Type` varchar(50) NOT NULL,
  PRIMARY KEY (`Approval_ID`),
  UNIQUE KEY `Approval_ID` (`Approval_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_attachment`
--

DROP TABLE IF EXISTS `tbl_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_attachment` (
  `Attachment_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Description` text,
  `Check_In_Type` varchar(50) DEFAULT NULL,
  `Attachment_Url` varchar(200) DEFAULT NULL,
  `Attachment_Date` datetime DEFAULT NULL,
  `item_list_cache_id` int(11) NOT NULL,
  `payment_item_list_id` int(11) NOT NULL,
  `image_name_flag` varchar(200) NOT NULL,
  PRIMARY KEY (`Attachment_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  CONSTRAINT `tbl_attachment_ibfk_1` FOREIGN KEY (`Registration_ID`) REFERENCES `tbl_patient_registration` (`Registration_ID`),
  CONSTRAINT `tbl_attachment_ibfk_2` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_audit_log`
--

DROP TABLE IF EXISTS `tbl_audit_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_audit_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=976 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_audit_trail`
--

DROP TABLE IF EXISTS `tbl_audit_trail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_audit_trail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `audit_log_id` int(10) unsigned NOT NULL,
  `employee_id` int(10) NOT NULL,
  `pre_data` text COLLATE utf8_unicode_ci,
  `post_data` text COLLATE utf8_unicode_ci,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mac_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pc_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_and_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `audit_trail_audit_log_id_foreign` (`audit_log_id`),
  CONSTRAINT `audit_trail_audit_log_id_foreign` FOREIGN KEY (`audit_log_id`) REFERENCES `tbl_audit_log` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3036 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_baby_care`
--

DROP TABLE IF EXISTS `tbl_baby_care`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_baby_care` (
  `baby_care_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `consultation_id` int(11) DEFAULT NULL,
  `babywho` varchar(50) NOT NULL,
  `dob` varchar(50) NOT NULL,
  `time_of_birth` varchar(20) NOT NULL,
  `apgarscore` varchar(50) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `length` varchar(100) NOT NULL,
  `type_of_delivery` varchar(100) NOT NULL,
  `head_circum_chest` varchar(100) NOT NULL,
  `saved_date` datetime DEFAULT NULL,
  `Admission_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`baby_care_id`),
  KEY `parent_id` (`parent_id`,`employee_id`,`consultation_id`),
  KEY `consultation_id` (`consultation_id`),
  KEY `employee_id` (`employee_id`),
  KEY `baby_care_admission_id_foreign` (`Admission_ID`),
  CONSTRAINT `baby_care_admission_id_foreign` FOREIGN KEY (`Admission_ID`) REFERENCES `tbl_admission` (`Admision_ID`),
  CONSTRAINT `tbl_baby_care_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `tbl_patient_registration` (`Registration_ID`),
  CONSTRAINT `tbl_baby_care_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_baby_care_ibfk_3` FOREIGN KEY (`consultation_id`) REFERENCES `tbl_consultation` (`consultation_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_baby_care_exam`
--

DROP TABLE IF EXISTS `tbl_baby_care_exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_baby_care_exam` (
  `baby_care_id` int(11) NOT NULL,
  `head_eyes_nose_ears_mouth` varchar(100) NOT NULL,
  `neck` varchar(100) NOT NULL,
  `chest` varchar(100) NOT NULL,
  `abdomen` varchar(100) NOT NULL,
  `cord` varchar(100) NOT NULL,
  `genital` varchar(100) NOT NULL,
  `limbs` varchar(100) NOT NULL,
  `eyes` varchar(100) NOT NULL,
  `hips` varchar(100) NOT NULL,
  `anus` varchar(100) NOT NULL,
  `neonatal_relexes` varchar(100) NOT NULL,
  `feeding` varchar(100) NOT NULL,
  `urine` varchar(100) NOT NULL,
  `stool` varchar(100) NOT NULL,
  `mouth` varchar(100) NOT NULL,
  `comments` tinytext NOT NULL,
  KEY `baby_care_id` (`baby_care_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_baby_care_health`
--

DROP TABLE IF EXISTS `tbl_baby_care_health`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_baby_care_health` (
  `baby_care_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date_saved` datetime NOT NULL,
  `room_temp` varchar(100) NOT NULL,
  `rect_temp` varchar(100) NOT NULL,
  `bowel` varchar(100) NOT NULL,
  `urine` varchar(100) NOT NULL,
  `vomiting` varchar(100) NOT NULL,
  `feed` varchar(100) NOT NULL,
  `treatment` varchar(100) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `tbl_baby_care_health_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_beds`
--

DROP TABLE IF EXISTS `tbl_beds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_beds` (
  `Bed_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Bed_Name` varchar(50) NOT NULL,
  `Status` varchar(30) NOT NULL DEFAULT 'available',
  `Ward_ID` int(11) NOT NULL COMMENT 'Hospital Ward ID',
  PRIMARY KEY (`Bed_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_branch_employee`
--

DROP TABLE IF EXISTS `tbl_branch_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_branch_employee` (
  `Employee_ID` int(11) NOT NULL DEFAULT '0',
  `Branch_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Employee_ID`,`Branch_ID`),
  KEY `Branch_ID` (`Branch_ID`),
  CONSTRAINT `tbl_branch_employee_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_branch_employee_ibfk_2` FOREIGN KEY (`Branch_ID`) REFERENCES `tbl_branches` (`Branch_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_branches`
--

DROP TABLE IF EXISTS `tbl_branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_branches` (
  `Branch_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Branch_Name` varchar(300) NOT NULL,
  `BannerLink` varchar(200) DEFAULT NULL,
  `Company_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Branch_ID`),
  UNIQUE KEY `Branch_Name` (`Branch_Name`),
  KEY `Company_ID` (`Company_ID`),
  CONSTRAINT `tbl_branches_ibfk_2` FOREIGN KEY (`Company_ID`) REFERENCES `tbl_company` (`Company_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_broadcast_message`
--

DROP TABLE IF EXISTS `tbl_broadcast_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_broadcast_message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `status` int(1) NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_budget`
--

DROP TABLE IF EXISTS `tbl_budget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_budget` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `budget_name` varchar(50) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_budget_items`
--

DROP TABLE IF EXISTS `tbl_budget_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_budget_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Item_ID` int(11) unsigned NOT NULL,
  `Budget_ID` int(11) unsigned NOT NULL,
  `quantity` varchar(50) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `budget_items_budget_id_foreign` (`Budget_ID`),
  KEY `budget_items_item_id_foreign` (`Item_ID`),
  CONSTRAINT `budget_items_budget_id_foreign` FOREIGN KEY (`Budget_ID`) REFERENCES `tbl_budget` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_check_in`
--

DROP TABLE IF EXISTS `tbl_check_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_check_in` (
  `Check_In_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `Visit_Date` date DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Check_In_Date_And_Time` datetime DEFAULT NULL,
  `Check_In_Status` varchar(50) DEFAULT 'pending',
  `Branch_ID` int(11) DEFAULT NULL,
  `Saved_Date_And_Time` datetime DEFAULT NULL,
  `Check_In_Date` date NOT NULL,
  `Type_Of_Check_In` varchar(20) NOT NULL,
  `Folio_Status` varchar(15) DEFAULT 'pending',
  `AuthorizationNo` varchar(50) DEFAULT NULL,
  `CardStatus` varchar(50) DEFAULT NULL,
  `msamaha_Items` int(11) DEFAULT NULL,
  `Anayependekeza_Msamaha` int(11) DEFAULT NULL,
  `Sub_Department_ID` int(10) unsigned DEFAULT NULL,
  `Patient_File_Status` int(11) DEFAULT '1',
  `track_number_status` int(2) DEFAULT '1',
  PRIMARY KEY (`Check_In_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Branch_ID` (`Branch_ID`),
  KEY `msamaha_Items` (`msamaha_Items`,`Anayependekeza_Msamaha`),
  KEY `Anayependekeza_Msamaha` (`Anayependekeza_Msamaha`),
  CONSTRAINT `tbl_check_in_ibfk_1` FOREIGN KEY (`Registration_ID`) REFERENCES `tbl_patient_registration` (`Registration_ID`),
  CONSTRAINT `tbl_check_in_ibfk_2` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_check_in_ibfk_3` FOREIGN KEY (`Branch_ID`) REFERENCES `tbl_branches` (`Branch_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_check_in_details`
--

DROP TABLE IF EXISTS `tbl_check_in_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_check_in_details` (
  `Check_In_Details_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Check_In_ID` int(11) DEFAULT NULL,
  `Claim_Form_Number` varchar(100) NOT NULL,
  `Folio_Number` int(11) DEFAULT NULL,
  `Sponsor_ID` int(11) NOT NULL,
  `ToBe_Admitted` varchar(10) NOT NULL DEFAULT 'no',
  `Admission_ID` int(11) DEFAULT NULL,
  `Admit_Status` varchar(20) NOT NULL DEFAULT 'not admitted',
  `ToBe_Admitted_Reason` varchar(255) NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Employee_Cancel` int(11) DEFAULT NULL COMMENT 'Employee cancel admission',
  `Date_Cancelled` datetime DEFAULT NULL,
  `Consultant_ID` int(11) DEFAULT NULL,
  `Check_In_Type` int(1) DEFAULT NULL,
  `Payment_Cache_ID` int(11) DEFAULT NULL,
  `Patient_Direction` varchar(15) DEFAULT NULL,
  `auth_referral_no` varchar(255) DEFAULT NULL,
  `auth_number` varchar(255) DEFAULT NULL,
  `remarks` text,
  `auth_status` varchar(255) DEFAULT NULL,
  `auth_remark` text,
  `member_card_no` varchar(255) DEFAULT NULL,
  `auth_visit_type` varchar(255) DEFAULT NULL,
  `tracking_number` int(11) DEFAULT NULL,
  `claim_status` int(11) DEFAULT '1',
  `claim_progress_status` int(11) DEFAULT '0',
  `claim_progress_final` int(11) DEFAULT '0',
  `claim_id` varchar(255) DEFAULT NULL,
  `claim_batch_id` int(11) DEFAULT NULL,
  `claim_reference_no` varchar(255) DEFAULT NULL,
  `claim_valid_scheme` varchar(2) DEFAULT '1',
  `claim_patient_name` int(11) DEFAULT NULL,
  `claim_patient_signature` int(11) DEFAULT NULL,
  `claim_patient_sign_date` int(11) DEFAULT NULL,
  `claim_patient_phone_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`Check_In_Details_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Sponsor_ID` (`Sponsor_ID`),
  KEY `Check_In_ID` (`Check_In_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `Employee_Cancel` (`Employee_Cancel`),
  KEY `Admission_ID` (`Admission_ID`),
  KEY `check_in_details_claim_batch_id_foreign` (`claim_batch_id`),
  CONSTRAINT `check_in_details_claim_batch_id_foreign` FOREIGN KEY (`claim_batch_id`) REFERENCES `tbl_claim_batch` (`id`),
  CONSTRAINT `tbl_check_in_details_ibfk_1` FOREIGN KEY (`Registration_ID`) REFERENCES `tbl_patient_registration` (`Registration_ID`),
  CONSTRAINT `tbl_check_in_details_ibfk_2` FOREIGN KEY (`Sponsor_ID`) REFERENCES `tbl_sponsor` (`Sponsor_ID`),
  CONSTRAINT `tbl_check_in_details_ibfk_3` FOREIGN KEY (`consultation_ID`) REFERENCES `tbl_consultation` (`consultation_ID`),
  CONSTRAINT `tbl_check_in_details_ibfk_4` FOREIGN KEY (`Check_In_ID`) REFERENCES `tbl_check_in` (`Check_In_ID`),
  CONSTRAINT `tbl_check_in_details_ibfk_5` FOREIGN KEY (`Employee_Cancel`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_check_in_details_ibfk_6` FOREIGN KEY (`Admission_ID`) REFERENCES `tbl_admission` (`Admision_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_claim_batch`
--

DROP TABLE IF EXISTS `tbl_claim_batch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_claim_batch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsor_id` int(11) NOT NULL,
  `claim_batch` text,
  `claim_month` int(11) DEFAULT NULL,
  `claim_year` int(11) DEFAULT NULL,
  `claim_month_serial` text,
  `claim_status` varchar(1) DEFAULT '1',
  `claim_system` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `claim_batch_sponsor_id_foreign` (`sponsor_id`),
  CONSTRAINT `claim_batch_sponsor_id_foreign` FOREIGN KEY (`sponsor_id`) REFERENCES `tbl_sponsor` (`Sponsor_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_claim_item`
--

DROP TABLE IF EXISTS `tbl_claim_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_claim_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsor_id` int(11) NOT NULL,
  `item_code` text,
  `item_name` text,
  `item_strength` varchar(100) DEFAULT NULL,
  `item_dosage` varchar(100) DEFAULT NULL,
  `need_approval` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `claim_item_sponsor_id_foreign` (`sponsor_id`),
  CONSTRAINT `claim_item_sponsor_id_foreign` FOREIGN KEY (`sponsor_id`) REFERENCES `tbl_sponsor` (`Sponsor_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2609 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_claim_item_price`
--

DROP TABLE IF EXISTS `tbl_claim_item_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_claim_item_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_item_id` int(11) NOT NULL,
  `benefit_package_code` varchar(100) DEFAULT NULL,
  `price` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `claim_item__price_claim_item_id_foreign` (`claim_item_id`),
  CONSTRAINT `claim_item__price_claim_item_id_foreign` FOREIGN KEY (`claim_item_id`) REFERENCES `tbl_claim_item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1302 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_claim_process_claim_progress_status`
--

DROP TABLE IF EXISTS `tbl_claim_process_claim_progress_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_claim_process_claim_progress_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_in_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `progress_status` int(11) NOT NULL,
  `progress_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `claim_process_claim_progress_status_check_in_id_foreign` (`check_in_id`),
  KEY `claim_process_claim_progress_status_employee_id_foreign` (`employee_id`),
  CONSTRAINT `claim_process_claim_progress_status_check_in_id_foreign` FOREIGN KEY (`check_in_id`) REFERENCES `tbl_check_in` (`Check_In_ID`),
  CONSTRAINT `claim_process_claim_progress_status_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_claim_process_map_sponsor_items`
--

DROP TABLE IF EXISTS `tbl_claim_process_map_sponsor_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_claim_process_map_sponsor_items` (
  `sponsor_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `mapped_item_id` int(11) NOT NULL,
  KEY `claim_process_map_sponsor_items_sponsor_id_foreign` (`sponsor_id`),
  KEY `claim_process_map_sponsor_items_item_id_foreign` (`item_id`),
  CONSTRAINT `claim_process_map_sponsor_items_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `tbl_items` (`Item_ID`),
  CONSTRAINT `claim_process_map_sponsor_items_sponsor_id_foreign` FOREIGN KEY (`sponsor_id`) REFERENCES `tbl_sponsor` (`Sponsor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_claim_process_status_employee`
--

DROP TABLE IF EXISTS `tbl_claim_process_status_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_claim_process_status_employee` (
  `employee_id` int(11) NOT NULL,
  `claim_process_status` int(11) NOT NULL,
  KEY `claim_process_status_employee_employee_id_foreign` (`employee_id`),
  CONSTRAINT `claim_process_status_employee_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_clinic`
--

DROP TABLE IF EXISTS `tbl_clinic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_clinic` (
  `Clinic_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Clinic_Name` varchar(150) NOT NULL,
  `Nature_Of_Clinic` varchar(120) NOT NULL,
  `Clinic_Status` varchar(20) NOT NULL DEFAULT 'Available',
  PRIMARY KEY (`Clinic_ID`),
  UNIQUE KEY `Clinic_Name` (`Clinic_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_clinic_assign_consultation_category_fee`
--

DROP TABLE IF EXISTS `tbl_clinic_assign_consultation_category_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_clinic_assign_consultation_category_fee` (
  `id` int(11) NOT NULL,
  `Clinic_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_clinic_employee`
--

DROP TABLE IF EXISTS `tbl_clinic_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_clinic_employee` (
  `Clinic_ID` int(11) NOT NULL DEFAULT '0',
  `Employee_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Clinic_ID`,`Employee_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  CONSTRAINT `tbl_clinic_employee_ibfk_1` FOREIGN KEY (`Clinic_ID`) REFERENCES `tbl_clinic` (`Clinic_ID`),
  CONSTRAINT `tbl_clinic_employee_ibfk_2` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_company`
--

DROP TABLE IF EXISTS `tbl_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_company` (
  `Company_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Company_Name` varchar(300) NOT NULL,
  PRIMARY KEY (`Company_ID`),
  UNIQUE KEY `Company_Name` (`Company_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_configuration`
--

DROP TABLE IF EXISTS `tbl_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_configuration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_configuration_option`
--

DROP TABLE IF EXISTS `tbl_configuration_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_configuration_option` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_value` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `configuration_id` int(10) unsigned NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `configuration_option_configuration_id_foreign` (`configuration_id`),
  CONSTRAINT `configuration_option_configuration_id_foreign` FOREIGN KEY (`configuration_id`) REFERENCES `tbl_configuration` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3168 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_consultation`
--

DROP TABLE IF EXISTS `tbl_consultation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_consultation` (
  `consultation_ID` int(11) NOT NULL AUTO_INCREMENT,
  `employee_ID` int(11) DEFAULT NULL,
  `Registration_ID` int(11) DEFAULT NULL,
  `Patient_Payment_Item_List_ID` int(11) DEFAULT NULL,
  `maincomplain` text,
  `firstsymptom_date` text,
  `history_present_illness` text,
  `review_of_other_systems` text,
  `general_observation` text,
  `systemic_observation` text,
  `provisional_diagnosis` int(11) DEFAULT NULL,
  `diferential_diagnosis` int(11) DEFAULT NULL,
  `Comment_For_Laboratory` text,
  `Comment_For_Radiology` text,
  `Comments_For_Procedure` text NOT NULL,
  `Comments_For_Surgery` text NOT NULL,
  `investigation_comments` text,
  `diagnosis` int(11) DEFAULT NULL,
  `remarks` text,
  `course_of_injuries` int(11) DEFAULT NULL,
  `Process_Status` varchar(20) NOT NULL DEFAULT 'pending',
  `Consultation_Date_And_Time` datetime DEFAULT NULL,
  `Patient_Type` varchar(20) DEFAULT NULL,
  `Payment_Cache_ID` int(11) DEFAULT NULL,
  `Comments_For_Pharmacy` text NOT NULL,
  `Require_Spectacle` varchar(2) NOT NULL,
  `Check_In_ID` int(11) DEFAULT NULL,
  `Cash_Payment_Cache_ID` int(11) DEFAULT NULL,
  `Need_Dialysis` varchar(2) DEFAULT NULL,
  `Need_ED` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`consultation_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Patient_Payment_Item_List_ID` (`Patient_Payment_Item_List_ID`),
  KEY `employee_ID` (`employee_ID`),
  KEY `Cash_Payment_Cache_ID` (`Cash_Payment_Cache_ID`),
  KEY `Check_In_ID` (`Check_In_ID`),
  KEY `Payment_Cache_ID` (`Payment_Cache_ID`),
  CONSTRAINT `tbl_consultation_ibfk_1` FOREIGN KEY (`Registration_ID`) REFERENCES `tbl_patient_registration` (`Registration_ID`),
  CONSTRAINT `tbl_consultation_ibfk_2` FOREIGN KEY (`Patient_Payment_Item_List_ID`) REFERENCES `tbl_patient_payment_item_list` (`Patient_Payment_Item_List_ID`),
  CONSTRAINT `tbl_consultation_ibfk_3` FOREIGN KEY (`employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_consultation_ibfk_4` FOREIGN KEY (`Cash_Payment_Cache_ID`) REFERENCES `tbl_payment_cache` (`Payment_Cache_ID`),
  CONSTRAINT `tbl_consultation_ibfk_5` FOREIGN KEY (`Check_In_ID`) REFERENCES `tbl_check_in` (`Check_In_ID`),
  CONSTRAINT `tbl_consultation_ibfk_6` FOREIGN KEY (`Payment_Cache_ID`) REFERENCES `tbl_payment_cache` (`Payment_Cache_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_country`
--

DROP TABLE IF EXISTS `tbl_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_country` (
  `Country_ID` int(12) NOT NULL AUTO_INCREMENT,
  `Country_Name` varchar(100) NOT NULL,
  `Default_Status` varchar(1) DEFAULT 'N',
  `phone_code` varchar(3) DEFAULT NULL,
  `phone_prefix` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`Country_ID`),
  UNIQUE KEY `Country_ID` (`Country_ID`),
  UNIQUE KEY `Country_Name` (`Country_Name`),
  UNIQUE KEY `Country_Name_2` (`Country_Name`),
  UNIQUE KEY `Country_Name_3` (`Country_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_currency`
--

DROP TABLE IF EXISTS `tbl_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(100) NOT NULL,
  `currency_code` varchar(100) NOT NULL,
  `currency_symbol` varchar(100) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`currency_id`),
  UNIQUE KEY `currency_name` (`currency_name`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_debt`
--

DROP TABLE IF EXISTS `tbl_debt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_debt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsor_id` int(11) NOT NULL,
  `invoice_number` varchar(50) DEFAULT NULL,
  `invoice_date` date NOT NULL,
  `invoice_amount` float NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `delivery_date` date DEFAULT NULL,
  `extended_due_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `debt_created_by_foreign` (`created_by`),
  KEY `debt_sponsor_id_foreign` (`sponsor_id`),
  CONSTRAINT `debt_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `debt_sponsor_id_foreign` FOREIGN KEY (`sponsor_id`) REFERENCES `tbl_sponsor` (`Sponsor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_department`
--

DROP TABLE IF EXISTS `tbl_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_department` (
  `Department_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Department_Name` varchar(100) NOT NULL,
  `Department_Location` varchar(100) NOT NULL,
  `Branch_ID` int(11) NOT NULL,
  PRIMARY KEY (`Department_ID`),
  UNIQUE KEY `Department_Name` (`Department_Name`),
  KEY `Branch_ID` (`Branch_ID`),
  KEY `Branch_ID_2` (`Branch_ID`),
  CONSTRAINT `tbl_department_ibfk_1` FOREIGN KEY (`Branch_ID`) REFERENCES `tbl_branches` (`Branch_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_dialysis_details`
--

DROP TABLE IF EXISTS `tbl_dialysis_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dialysis_details` (
  `dialysis_details_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Patient_reg` int(11) NOT NULL,
  `Attendance_Date` date NOT NULL,
  `bpPrevious_Post_sit` varchar(40) NOT NULL,
  `bpPrevious_Post_stand` varchar(40) NOT NULL,
  `Weight_Previous_Post_sit` varchar(40) NOT NULL,
  `Weight_Previous_Post_stand` varchar(40) NOT NULL,
  `Weight_Gain` varchar(40) NOT NULL,
  `Time_On` varchar(40) NOT NULL,
  `bpPre_sit` varchar(40) NOT NULL,
  `bpPre_stand` varchar(40) NOT NULL,
  `Weight_Pre_sit` varchar(40) NOT NULL,
  `Weight_Pre_stand` varchar(40) NOT NULL,
  `Weight_removal` varchar(40) NOT NULL,
  `Time_Off` varchar(40) NOT NULL,
  `bpPost_sit` varchar(40) NOT NULL,
  `bpPost_stand` varchar(40) NOT NULL,
  `Weight_Post_sit` varchar(40) NOT NULL,
  `Weight_Post_stand` varchar(40) NOT NULL,
  `Area` varchar(40) NOT NULL,
  `Station` varchar(40) NOT NULL,
  `Machine` varchar(40) NOT NULL,
  `Hosp` varchar(10) NOT NULL,
  `Conductivity_Machine` varchar(50) NOT NULL,
  `Conductivity_manual` varchar(50) NOT NULL,
  `pH_Machine` varchar(50) NOT NULL,
  `pH_Manual` varchar(50) NOT NULL,
  `Temperature_Machine` varchar(50) NOT NULL,
  `Temperature_Initial` varchar(50) NOT NULL,
  `Alarm_Test` varchar(50) NOT NULL,
  `Air_Detector` varchar(50) NOT NULL,
  `Positive_Presence` varchar(50) NOT NULL,
  `Negative_Residual` varchar(50) NOT NULL,
  `Dialyzer_ID` varchar(50) NOT NULL,
  `UF_System` varchar(20) NOT NULL,
  `UF_System_initial` varchar(50) NOT NULL,
  `Type` varchar(40) NOT NULL,
  `Initial_Bolus` varchar(40) NOT NULL,
  `Unit_Hr` varchar(40) NOT NULL,
  `Stop_time` varchar(40) NOT NULL,
  `CVC_Pos` varchar(40) NOT NULL,
  `Arterial` varchar(40) NOT NULL,
  `Infusion_Bolus` varchar(40) NOT NULL,
  `Venous` varchar(40) NOT NULL,
  `Dialyzer_1` varchar(40) NOT NULL,
  `Dialyzer_2` varchar(40) NOT NULL,
  `Dialyzer_3` varchar(40) NOT NULL,
  `Dialyzer_4` varchar(40) NOT NULL,
  `Dialysate_1` varchar(40) NOT NULL,
  `Dialysate_2` varchar(40) NOT NULL,
  `Dialysate_3` varchar(40) NOT NULL,
  `Dialysate_4` varchar(40) NOT NULL,
  `Sodium_1` varchar(40) NOT NULL,
  `Sodium_2` varchar(40) NOT NULL,
  `Sodium_3` varchar(40) NOT NULL,
  `Sodium_4` varchar(40) NOT NULL,
  `UD_1` varchar(40) NOT NULL,
  `UD_2` varchar(40) NOT NULL,
  `UD_3` varchar(40) NOT NULL,
  `UD_4` varchar(40) NOT NULL,
  `Temp_1` varchar(40) NOT NULL,
  `Temp_2` varchar(40) NOT NULL,
  `Temp_3` varchar(40) NOT NULL,
  `Temp_4` varchar(40) NOT NULL,
  `AccessOrdersNotes` text NOT NULL,
  `Arterial_Needle_Gauge` varchar(40) NOT NULL,
  `Arterial_Needle_Length` varchar(40) NOT NULL,
  `Arterial_Static_Pressuer` varchar(40) NOT NULL,
  `Arterial_Bleeding_Stopped` varchar(40) NOT NULL,
  `Venous_Needle_Gauge` varchar(40) NOT NULL,
  `Venous_Needle_Length` varchar(40) NOT NULL,
  `Venous_Static_Pressuer` varchar(40) NOT NULL,
  `Venous_Bleeding_Stopped` varchar(40) NOT NULL,
  `Canulated` int(11) NOT NULL,
  `Notes` text NOT NULL,
  `Temp_Pre_Assessment` varchar(40) NOT NULL,
  `Temp_Time` varchar(40) NOT NULL,
  `Temp_Initials` varchar(40) NOT NULL,
  `Temp_Post` varchar(40) NOT NULL,
  `Temp_Initials2` varchar(40) NOT NULL,
  `Resp_Pre_Assessment` varchar(40) NOT NULL,
  `Resp_Time` varchar(40) NOT NULL,
  `Resp_Initials` varchar(40) NOT NULL,
  `Resp_Post` varchar(40) NOT NULL,
  `Resp_Initials2` varchar(40) NOT NULL,
  `GI_Pre_Assessment` varchar(40) NOT NULL,
  `GI_Time` varchar(40) NOT NULL,
  `GI_Initials` varchar(40) NOT NULL,
  `GI_Post` varchar(40) NOT NULL,
  `GI_Initials2` varchar(40) NOT NULL,
  `Cardiac_Pre_Assessment` varchar(40) NOT NULL,
  `Cardiac_Time` varchar(40) NOT NULL,
  `Cardiac_Initials` varchar(40) NOT NULL,
  `Cardiac_Post` varchar(40) NOT NULL,
  `Cardiac_Initials2` varchar(40) NOT NULL,
  `Edema_Pre_Assessment` varchar(40) NOT NULL,
  `Edema_Time` varchar(40) NOT NULL,
  `Edema_Initials` varchar(40) NOT NULL,
  `Edema_Post` varchar(40) NOT NULL,
  `Edema_Initials2` varchar(40) NOT NULL,
  `Mental_Pre_Assessment` varchar(40) NOT NULL,
  `Mental_Time` varchar(40) NOT NULL,
  `Mental_Initials` varchar(40) NOT NULL,
  `Mental_Post` varchar(40) NOT NULL,
  `Mental_Initials2` varchar(40) NOT NULL,
  `Mobility_Pre_Assessment` varchar(40) NOT NULL,
  `Mobility_Time` varchar(40) NOT NULL,
  `Mobility_Initials` varchar(40) NOT NULL,
  `Mobility_Post` varchar(40) NOT NULL,
  `Mobility_Initials2` varchar(40) NOT NULL,
  `Access_Pre_Assessment` varchar(40) NOT NULL,
  `Access_Time` varchar(40) NOT NULL,
  `Access_Initials` varchar(40) NOT NULL,
  `Access_Post` varchar(40) NOT NULL,
  `Access_Initials2` varchar(40) NOT NULL,
  `Time_1` varchar(40) NOT NULL,
  `Time_2` varchar(40) NOT NULL,
  `Time_3` varchar(40) NOT NULL,
  `Time_4` varchar(40) NOT NULL,
  `Time_5` varchar(40) NOT NULL,
  `Time_6` varchar(40) NOT NULL,
  `Time_7` varchar(40) NOT NULL,
  `BP_1` varchar(40) NOT NULL,
  `BP_2` varchar(40) NOT NULL,
  `BP_3` varchar(40) NOT NULL,
  `BP_4` varchar(40) NOT NULL,
  `BP_5` varchar(40) NOT NULL,
  `BP_6` varchar(40) NOT NULL,
  `BP_7` varchar(40) NOT NULL,
  `HR_1` varchar(40) NOT NULL,
  `HR_2` varchar(40) NOT NULL,
  `HR_3` varchar(40) NOT NULL,
  `HR_4` varchar(40) NOT NULL,
  `HR_5` varchar(40) NOT NULL,
  `HR_6` varchar(40) NOT NULL,
  `HR_7` varchar(40) NOT NULL,
  `QB_1` varchar(40) NOT NULL,
  `QB_2` varchar(40) NOT NULL,
  `QB_3` varchar(40) NOT NULL,
  `QB_4` varchar(40) NOT NULL,
  `QB_5` varchar(40) NOT NULL,
  `QB_6` varchar(40) NOT NULL,
  `QB_7` varchar(40) NOT NULL,
  `QD_1` varchar(40) NOT NULL,
  `QD_2` varchar(40) NOT NULL,
  `QD_3` varchar(40) NOT NULL,
  `QD_4` varchar(40) NOT NULL,
  `QD_5` varchar(40) NOT NULL,
  `QD_6` varchar(40) NOT NULL,
  `QD_7` varchar(40) NOT NULL,
  `AP_1` varchar(40) NOT NULL,
  `AP_2` varchar(40) NOT NULL,
  `AP_3` varchar(40) NOT NULL,
  `AP_4` varchar(40) NOT NULL,
  `AP_5` varchar(40) NOT NULL,
  `AP_6` varchar(40) NOT NULL,
  `AP_7` varchar(40) NOT NULL,
  `VP_1` varchar(40) NOT NULL,
  `VP_2` varchar(40) NOT NULL,
  `VP_3` varchar(40) NOT NULL,
  `VP_4` varchar(40) NOT NULL,
  `VP_5` varchar(40) NOT NULL,
  `VP_6` varchar(40) NOT NULL,
  `VP_7` varchar(40) NOT NULL,
  `FldRmvd_1` varchar(40) NOT NULL,
  `FldRmvd_2` varchar(40) NOT NULL,
  `FldRmvd_3` varchar(40) NOT NULL,
  `FldRmvd_4` varchar(40) NOT NULL,
  `FldRmvd_5` varchar(40) NOT NULL,
  `FldRmvd_6` varchar(40) NOT NULL,
  `FldRmvd_7` varchar(40) NOT NULL,
  `Heparin_1` varchar(40) NOT NULL,
  `Heparin_2` varchar(40) NOT NULL,
  `Heparin_3` varchar(40) NOT NULL,
  `Heparin_4` varchar(40) NOT NULL,
  `Heparin_5` varchar(40) NOT NULL,
  `Heparin_6` varchar(40) NOT NULL,
  `Heparin_7` varchar(40) NOT NULL,
  `Saline_1` varchar(40) NOT NULL,
  `Saline_2` varchar(40) NOT NULL,
  `Saline_3` varchar(40) NOT NULL,
  `Saline_4` varchar(40) NOT NULL,
  `Saline_5` varchar(40) NOT NULL,
  `Saline_6` varchar(40) NOT NULL,
  `Saline_7` varchar(40) NOT NULL,
  `UFR_1` varchar(40) NOT NULL,
  `UFR_2` varchar(40) NOT NULL,
  `UFR_3` varchar(40) NOT NULL,
  `UFR_4` varchar(40) NOT NULL,
  `UFR_5` varchar(40) NOT NULL,
  `UFR_6` varchar(40) NOT NULL,
  `UFR_7` varchar(40) NOT NULL,
  `TMP_1` varchar(40) NOT NULL,
  `TMP_2` varchar(40) NOT NULL,
  `TMP_3` varchar(40) NOT NULL,
  `TMP_4` varchar(40) NOT NULL,
  `TMP_5` varchar(40) NOT NULL,
  `TMP_6` varchar(40) NOT NULL,
  `TMP_7` varchar(40) NOT NULL,
  `BVP_1` varchar(40) NOT NULL,
  `BVP_2` varchar(40) NOT NULL,
  `BVP_3` varchar(40) NOT NULL,
  `BVP_4` varchar(40) NOT NULL,
  `BVP_5` varchar(40) NOT NULL,
  `BVP_6` varchar(40) NOT NULL,
  `BVP_7` varchar(40) NOT NULL,
  `Access_1` varchar(40) NOT NULL,
  `Access_2` varchar(40) NOT NULL,
  `Access_3` varchar(40) NOT NULL,
  `Access_4` varchar(40) NOT NULL,
  `Access_5` varchar(40) NOT NULL,
  `Access_6` varchar(40) NOT NULL,
  `Access_7` varchar(40) NOT NULL,
  `Notes_1` varchar(40) NOT NULL,
  `Notes_2` varchar(40) NOT NULL,
  `Notes_3` varchar(40) NOT NULL,
  `Notes_4` varchar(40) NOT NULL,
  `Notes_5` varchar(40) NOT NULL,
  `Notes_6` varchar(40) NOT NULL,
  `Notes_7` varchar(40) NOT NULL,
  `Ancillary_1` varchar(40) NOT NULL,
  `Ancillary_2` varchar(40) NOT NULL,
  `Ancillary_3` varchar(40) NOT NULL,
  `Ancillary_4` varchar(40) NOT NULL,
  `Ancillary_5` varchar(40) NOT NULL,
  `Ancillary_6` varchar(40) NOT NULL,
  `Ancillary_7` varchar(40) NOT NULL,
  `Indication_1` varchar(40) NOT NULL,
  `Indication_2` varchar(40) NOT NULL,
  `Indication_3` varchar(40) NOT NULL,
  `Indication_4` varchar(40) NOT NULL,
  `Indication_5` varchar(40) NOT NULL,
  `Indication_6` varchar(40) NOT NULL,
  `Indication_7` varchar(40) NOT NULL,
  `Dose_1` varchar(40) NOT NULL,
  `Dose_2` varchar(40) NOT NULL,
  `Dose_3` varchar(40) NOT NULL,
  `Dose_4` varchar(40) NOT NULL,
  `Dose_5` varchar(40) NOT NULL,
  `Dose_6` varchar(40) NOT NULL,
  `Dose_7` varchar(40) NOT NULL,
  `Route_1` varchar(40) NOT NULL,
  `Route_2` varchar(40) NOT NULL,
  `Route_3` varchar(40) NOT NULL,
  `Route_4` varchar(40) NOT NULL,
  `Route_5` varchar(40) NOT NULL,
  `Route_6` varchar(40) NOT NULL,
  `Route_7` varchar(40) NOT NULL,
  `Time_chart_1` varchar(40) NOT NULL,
  `Time_chart_2` varchar(40) NOT NULL,
  `Time_chart_3` varchar(40) NOT NULL,
  `Time_chart_4` varchar(40) NOT NULL,
  `Time_chart_5` varchar(40) NOT NULL,
  `Time_chart_6` varchar(40) NOT NULL,
  `Time_chart_7` varchar(40) NOT NULL,
  `Initials_charts_1` varchar(40) NOT NULL,
  `Initials_charts_2` varchar(40) NOT NULL,
  `Initials_charts_3` varchar(40) NOT NULL,
  `Initials_charts_4` varchar(40) NOT NULL,
  `Initials_charts_5` varchar(40) NOT NULL,
  `Initials_charts_6` varchar(40) NOT NULL,
  `Initials_charts_7` varchar(40) NOT NULL,
  `Ancillary_2_1` varchar(40) NOT NULL,
  `Ancillary_2_2` varchar(40) NOT NULL,
  `Ancillary_2_3` varchar(40) NOT NULL,
  `Ancillary_2_4` varchar(40) NOT NULL,
  `Ancillary_2_5` varchar(40) NOT NULL,
  `Ancillary_2_6` varchar(40) NOT NULL,
  `Ancillary_2_7` varchar(40) NOT NULL,
  `Indication_1_1` varchar(40) NOT NULL,
  `Indication_1_2` varchar(40) NOT NULL,
  `Indication_1_3` varchar(40) NOT NULL,
  `Indication_1_4` varchar(40) NOT NULL,
  `Indication_1_5` varchar(40) NOT NULL,
  `Indication_1_6` varchar(40) NOT NULL,
  `Indication_1_7` varchar(40) NOT NULL,
  `Dose_1_1` varchar(40) NOT NULL,
  `Dose_1_2` varchar(40) NOT NULL,
  `Dose_1_3` varchar(40) NOT NULL,
  `Dose_1_4` varchar(40) NOT NULL,
  `Dose_1_5` varchar(40) NOT NULL,
  `Dose_1_6` varchar(40) NOT NULL,
  `Dose_1_7` varchar(40) NOT NULL,
  `Route_1_1` varchar(40) NOT NULL,
  `Route_1_2` varchar(40) NOT NULL,
  `Route_1_3` varchar(40) NOT NULL,
  `Route_1_4` varchar(40) NOT NULL,
  `Route_1_5` varchar(40) NOT NULL,
  `Route_1_6` varchar(40) NOT NULL,
  `Route_1_7` varchar(40) NOT NULL,
  `Time_1_1` varchar(40) NOT NULL,
  `Time_1_2` varchar(40) NOT NULL,
  `Time_1_3` varchar(40) NOT NULL,
  `Time_1_4` varchar(40) NOT NULL,
  `Time_1_5` varchar(40) NOT NULL,
  `Time_1_6` varchar(40) NOT NULL,
  `Time_1_7` varchar(40) NOT NULL,
  `Initials_1` varchar(40) NOT NULL,
  `Initials_2` varchar(40) NOT NULL,
  `Initials_3` varchar(40) NOT NULL,
  `Initials_4` varchar(40) NOT NULL,
  `Initials_5` varchar(40) NOT NULL,
  `Initials_6` varchar(40) NOT NULL,
  `Initials_7` varchar(40) NOT NULL,
  PRIMARY KEY (`dialysis_details_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_dialysis_dialysis_orders`
--

DROP TABLE IF EXISTS `tbl_dialysis_dialysis_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dialysis_dialysis_orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Employee_ID` int(11) NOT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `dialyzer` varchar(50) NOT NULL,
  `dialysate` varchar(50) NOT NULL,
  `cannula_size` varchar(50) NOT NULL,
  `sodium_remodelling` varchar(50) NOT NULL,
  `potassium` varchar(50) NOT NULL,
  `hours_of_dialysis` varchar(50) NOT NULL,
  `weight_loss` varchar(50) NOT NULL,
  `transfusion` varchar(50) NOT NULL,
  `blood_group` varchar(50) NOT NULL,
  `no_of_units` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Payment_Item_Cache_List_ID` (`Payment_Item_Cache_List_ID`),
  CONSTRAINT `tbl_dialysis_dialysis_orders_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_dialysis_dialysis_orders_ibfk_2` FOREIGN KEY (`Payment_Item_Cache_List_ID`) REFERENCES `tbl_item_list_cache` (`Payment_Item_Cache_List_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_dialysis_heparain`
--

DROP TABLE IF EXISTS `tbl_dialysis_heparain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dialysis_heparain` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Employee_ID` int(11) NOT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `type` varchar(50) NOT NULL,
  `initial_bolus_unit` varchar(50) NOT NULL,
  `unit_hr` varchar(50) NOT NULL,
  `infusion_bolus` varchar(50) NOT NULL,
  `stop_time` varchar(50) NOT NULL,
  `cvc_post_instil` varchar(50) NOT NULL,
  `arterial` varchar(50) NOT NULL,
  `venous` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Payment_Item_Cache_List_ID` (`Payment_Item_Cache_List_ID`),
  CONSTRAINT `tbl_dialysis_heparain_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_dialysis_heparain_ibfk_2` FOREIGN KEY (`Payment_Item_Cache_List_ID`) REFERENCES `tbl_item_list_cache` (`Payment_Item_Cache_List_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_dialysis_machine_assessment`
--

DROP TABLE IF EXISTS `tbl_dialysis_machine_assessment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dialysis_machine_assessment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Employee_ID` int(11) NOT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `conductivity_machine` varchar(50) NOT NULL,
  `ph_machine` varchar(50) NOT NULL,
  `temperature_machine` varchar(50) NOT NULL,
  `positive_preference_test` varchar(50) NOT NULL,
  `negative_residual_test` varchar(50) NOT NULL,
  `dialyzer` varchar(50) NOT NULL,
  `alarm_test` varchar(50) NOT NULL,
  `air_detector_on` varchar(50) NOT NULL,
  `uf_system` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Payment_Item_Cache_List_ID` (`Payment_Item_Cache_List_ID`),
  CONSTRAINT `tbl_dialysis_machine_assessment_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_dialysis_machine_assessment_ibfk_2` FOREIGN KEY (`Payment_Item_Cache_List_ID`) REFERENCES `tbl_item_list_cache` (`Payment_Item_Cache_List_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_dialysis_notes`
--

DROP TABLE IF EXISTS `tbl_dialysis_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dialysis_notes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  `notes` text NOT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `Payment_Item_Cache_List_ID` (`Payment_Item_Cache_List_ID`),
  CONSTRAINT `tbl_dialysis_notes_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_dialysis_notes_ibfk_2` FOREIGN KEY (`Payment_Item_Cache_List_ID`) REFERENCES `tbl_item_list_cache` (`Payment_Item_Cache_List_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_dialysis_observation_chart`
--

DROP TABLE IF EXISTS `tbl_dialysis_observation_chart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dialysis_observation_chart` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Employee_id` int(11) NOT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `time_of_observation` text,
  `bp` text,
  `ap` text,
  `vp` text,
  `pulse` text,
  `blood_flow` text,
  `res` text,
  `temp` text,
  `uf_rate` text,
  `uf_removal` text,
  `iv_po` text,
  `rbg` text,
  `spO2` text,
  `pump_speed` text,
  `volume` text,
  `heparin` text,
  `cond` text,
  `remarks` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Employee_id` (`Employee_id`),
  KEY `Payment_Item_Cache_List_ID` (`Payment_Item_Cache_List_ID`),
  CONSTRAINT `tbl_dialysis_observation_chart_ibfk_1` FOREIGN KEY (`Employee_id`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_dialysis_observation_chart_ibfk_2` FOREIGN KEY (`Payment_Item_Cache_List_ID`) REFERENCES `tbl_item_list_cache` (`Payment_Item_Cache_List_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_dialysis_patient_assessment`
--

DROP TABLE IF EXISTS `tbl_dialysis_patient_assessment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dialysis_patient_assessment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Employee_ID` int(11) NOT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `pre_dialysis_dry_weight` varchar(50) NOT NULL,
  `pre_dialysis_additional_weight` varchar(50) NOT NULL,
  `pre_dialysis_pre_weight` varchar(50) NOT NULL,
  `pre_dialysis_height` varchar(50) NOT NULL,
  `pre_dialysis_standing_bp` varchar(50) NOT NULL,
  `pre_dialysis_standing_pulse_rate` varchar(50) NOT NULL,
  `pre_dialysis_resting_bp` varchar(50) NOT NULL,
  `pre_dialysis_resting_pulse_rate` varchar(50) NOT NULL,
  `pre_dialysis_resting_respiration_rate` varchar(50) NOT NULL,
  `pre_dialysis_temperature` varchar(50) NOT NULL,
  `pre_dialysis_saturation_of_oxygen` varchar(50) NOT NULL,
  `pre_dialysis_time_on` varchar(50) NOT NULL,
  `post_dialysis_post_weight` varchar(50) NOT NULL,
  `post_dialysis_weight_loss` varchar(50) NOT NULL,
  `post_dialysis_standing_bp` varchar(50) NOT NULL,
  `post_dialysis_standing_pulse_rate` varchar(50) NOT NULL,
  `post_dialysis_resting_bp` varchar(50) NOT NULL,
  `post_dialysis_resting_pulse_rate` varchar(50) NOT NULL,
  `post_dialysis_resting_respiration_rate` varchar(50) NOT NULL,
  `post_dialysis_temperature` varchar(50) NOT NULL,
  `post_dialysis_saturation_of_oxygen` varchar(50) NOT NULL,
  `post_dialysis_time_off` varchar(50) NOT NULL,
  `post_dialysis_elective_time` varchar(50) NOT NULL,
  `post_dialysis_ocm_archived` varchar(50) NOT NULL,
  `av_fistula` varchar(50) NOT NULL,
  `av_graft` varchar(50) NOT NULL,
  `temporary_catheter` varchar(50) NOT NULL,
  `permanent_catheter` varchar(50) NOT NULL,
  `iron` varchar(50) DEFAULT NULL,
  `epo` varchar(50) DEFAULT NULL,
  `calcium` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Payment_Item_Cache_List_ID` (`Payment_Item_Cache_List_ID`),
  CONSTRAINT `tbl_dialysis_patient_assessment_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_dialysis_patient_assessment_ibfk_2` FOREIGN KEY (`Payment_Item_Cache_List_ID`) REFERENCES `tbl_item_list_cache` (`Payment_Item_Cache_List_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_directory`
--

DROP TABLE IF EXISTS `tbl_directory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_directory` (
  `Directory_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Office_Name` varchar(50) DEFAULT NULL,
  `Exit_No` int(11) DEFAULT NULL,
  PRIMARY KEY (`Directory_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_discharge_reason`
--

DROP TABLE IF EXISTS `tbl_discharge_reason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_discharge_reason` (
  `Discharge_Reason_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Discharge_Reason` varchar(200) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Date_Added` datetime NOT NULL,
  PRIMARY KEY (`Discharge_Reason_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  CONSTRAINT `tbl_discharge_reason_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_discharge_summary`
--

DROP TABLE IF EXISTS `tbl_discharge_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_discharge_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_id` int(11) NOT NULL,
  `check_in_id` int(11) NOT NULL,
  `patient_name` varchar(50) NOT NULL,
  `patient_address` varchar(255) NOT NULL,
  `company` varchar(50) DEFAULT NULL,
  `age` varchar(50) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `admission_date` datetime NOT NULL,
  `discharge_date` datetime NOT NULL,
  `attending_consultant_id` int(11) DEFAULT NULL,
  `discharge_consultant_id` int(11) NOT NULL,
  `differential_diagnosis` text,
  `final_diagnosis` text NOT NULL,
  `recommendation` text NOT NULL,
  `instructions` text NOT NULL,
  `history_notes` text NOT NULL,
  `investigation_summary` text,
  `surgery_procedure_summary` text,
  `medication_summary` text,
  `discharge_need_ed` varchar(6) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `discharge_summary_created_by_foreign` (`created_by`),
  KEY `discharge_summary_check_in_id_foreign` (`check_in_id`),
  KEY `discharge_summary_registration_id_foreign` (`registration_id`),
  KEY `discharge_summary_attending_consultant_id_foreign` (`attending_consultant_id`),
  KEY `discharge_summary_discharge_consultant_id_foreign` (`discharge_consultant_id`),
  CONSTRAINT `discharge_summary_attending_consultant_id_foreign` FOREIGN KEY (`attending_consultant_id`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `discharge_summary_check_in_id_foreign` FOREIGN KEY (`check_in_id`) REFERENCES `tbl_check_in` (`Check_In_ID`),
  CONSTRAINT `discharge_summary_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `discharge_summary_discharge_consultant_id_foreign` FOREIGN KEY (`discharge_consultant_id`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `discharge_summary_registration_id_foreign` FOREIGN KEY (`registration_id`) REFERENCES `tbl_patient_registration` (`Registration_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_disease`
--

DROP TABLE IF EXISTS `tbl_disease`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disease` (
  `disease_ID` int(11) NOT NULL AUTO_INCREMENT,
  `disease_code` varchar(20) DEFAULT NULL,
  `nhif_code` varchar(20) DEFAULT NULL,
  `disease_name` varchar(120) DEFAULT NULL,
  `subcategory_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Date_And_Time` datetime DEFAULT NULL,
  `Disease_status` varchar(15) NOT NULL,
  `icd_standard` int(11) NOT NULL,
  PRIMARY KEY (`disease_ID`),
  KEY `subcategory_ID` (`subcategory_ID`),
  CONSTRAINT `tbl_disease_ibfk_1` FOREIGN KEY (`subcategory_ID`) REFERENCES `tbl_disease_subcategory` (`subcategory_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20818 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_disease_category`
--

DROP TABLE IF EXISTS `tbl_disease_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disease_category` (
  `disease_category_ID` int(11) NOT NULL AUTO_INCREMENT,
  `category_discreption` varchar(120) DEFAULT NULL,
  `main_category_id` int(11) NOT NULL,
  PRIMARY KEY (`disease_category_ID`),
  UNIQUE KEY `category_discreption` (`category_discreption`),
  KEY `main_category_id` (`main_category_id`),
  CONSTRAINT `tbl_disease_category_ibfk_1` FOREIGN KEY (`main_category_id`) REFERENCES `tbl_disease_maincategory` (`main_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_disease_consultation`
--

DROP TABLE IF EXISTS `tbl_disease_consultation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disease_consultation` (
  `Disease_Consultation_ID` int(11) NOT NULL AUTO_INCREMENT,
  `disease_ID` int(11) DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `employee_ID` int(11) DEFAULT NULL,
  `diagnosis_type` varchar(30) NOT NULL,
  `Disease_Consultation_Date_And_Time` datetime NOT NULL,
  PRIMARY KEY (`Disease_Consultation_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `disease_ID` (`disease_ID`),
  KEY `employee_ID` (`employee_ID`),
  CONSTRAINT `tbl_disease_consultation_ibfk_1` FOREIGN KEY (`disease_ID`) REFERENCES `tbl_disease` (`disease_ID`),
  CONSTRAINT `tbl_disease_consultation_ibfk_2` FOREIGN KEY (`consultation_ID`) REFERENCES `tbl_consultation` (`consultation_ID`),
  CONSTRAINT `tbl_disease_consultation_ibfk_3` FOREIGN KEY (`employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_disease_group`
--

DROP TABLE IF EXISTS `tbl_disease_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disease_group` (
  `disease_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `Dental_disease_Form_Id` int(11) DEFAULT NULL,
  `disease_group_name` varchar(120) DEFAULT NULL,
  `Opd_disease_Form_Id` int(11) DEFAULT NULL,
  `Gender_Type` varchar(20) NOT NULL,
  `Age_60_Years_And_Above` varchar(10) NOT NULL,
  `Age_Between_1_Year_But_Below_5_Year` varchar(10) NOT NULL,
  `Five_Years_Or_Below_Sixty_Years` varchar(10) NOT NULL,
  `Age_Between_1_Month_But_Below_1_Year` varchar(10) NOT NULL,
  `Age_Below_1_Month` varchar(10) NOT NULL,
  `Opd_Report` varchar(10) NOT NULL DEFAULT 'No',
  `Ipd_Report` varchar(10) NOT NULL DEFAULT 'No',
  `Ipd_disease_Form_Id` int(11) DEFAULT NULL,
  `Dental_Report` varchar(20) NOT NULL DEFAULT 'No',
  `Weekly_Report_Form_Id` int(11) DEFAULT NULL,
  `Weekly_Report` varchar(20) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`disease_group_id`),
  UNIQUE KEY `disease_group_name` (`disease_group_name`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_disease_group_mapping`
--

DROP TABLE IF EXISTS `tbl_disease_group_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disease_group_mapping` (
  `disease_group_id` int(11) NOT NULL DEFAULT '0',
  `disease_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`disease_group_id`,`disease_id`),
  KEY `disease_id` (`disease_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_disease_maincategory`
--

DROP TABLE IF EXISTS `tbl_disease_maincategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disease_maincategory` (
  `main_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `maincategory_name` varchar(200) NOT NULL,
  PRIMARY KEY (`main_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_disease_sponsor_icd_standard`
--

DROP TABLE IF EXISTS `tbl_disease_sponsor_icd_standard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disease_sponsor_icd_standard` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sponsor_id` int(11) DEFAULT NULL,
  `icd_standard` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sponsor_id` (`sponsor_id`),
  CONSTRAINT `tbl_disease_sponsor_icd_standard_ibfk_1` FOREIGN KEY (`sponsor_id`) REFERENCES `tbl_sponsor` (`Sponsor_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_disease_subcategory`
--

DROP TABLE IF EXISTS `tbl_disease_subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disease_subcategory` (
  `subcategory_ID` int(11) NOT NULL AUTO_INCREMENT,
  `disease_category_ID` int(11) DEFAULT NULL,
  `subcategory_description` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`subcategory_ID`),
  UNIQUE KEY `subcategory_description` (`subcategory_description`),
  KEY `category_ID` (`disease_category_ID`),
  CONSTRAINT `tbl_disease_subcategory_ibfk_1` FOREIGN KEY (`disease_category_ID`) REFERENCES `tbl_disease_category` (`disease_category_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1114 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_disposal`
--

DROP TABLE IF EXISTS `tbl_disposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disposal` (
  `Disposal_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Disposal_Status` varchar(30) NOT NULL DEFAULT 'pending',
  `Sub_Department_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Created_Date` date NOT NULL,
  `Created_Date_And_Time` datetime NOT NULL,
  `Disposed_Date` date NOT NULL,
  `Supervisor_ID` int(11) DEFAULT NULL,
  `Disposal_Description` varchar(200) DEFAULT NULL,
  `Branch_ID` int(11) DEFAULT NULL,
  `Previous_Disposal_Data` text,
  PRIMARY KEY (`Disposal_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Supervisor_ID` (`Supervisor_ID`),
  KEY `Sub_Department_ID` (`Sub_Department_ID`),
  KEY `Branch_ID` (`Branch_ID`),
  CONSTRAINT `tbl_disposal_ibfk_2` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_disposal_ibfk_3` FOREIGN KEY (`Supervisor_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_disposal_ibfk_4` FOREIGN KEY (`Sub_Department_ID`) REFERENCES `tbl_sub_department` (`Sub_Department_ID`),
  CONSTRAINT `tbl_disposal_ibfk_5` FOREIGN KEY (`Branch_ID`) REFERENCES `tbl_branches` (`Branch_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_disposal_items`
--

DROP TABLE IF EXISTS `tbl_disposal_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disposal_items` (
  `Disposal_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_ID` int(11) NOT NULL,
  `Quantity_Disposed` int(11) NOT NULL,
  `Buying_Price` double NOT NULL DEFAULT '0',
  `Disposal_ID` int(11) NOT NULL,
  `Item_Remark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Disposal_Item_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `Disposal_ID` (`Disposal_ID`),
  CONSTRAINT `tbl_disposal_items_ibfk_1` FOREIGN KEY (`Disposal_ID`) REFERENCES `tbl_disposal` (`Disposal_ID`),
  CONSTRAINT `tbl_disposal_items_ibfk_2` FOREIGN KEY (`Item_ID`) REFERENCES `tbl_items` (`Item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_district`
--

DROP TABLE IF EXISTS `tbl_district`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_district` (
  `District_ID` int(11) NOT NULL AUTO_INCREMENT,
  `District_Name` varchar(70) NOT NULL,
  `Region_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`District_ID`),
  UNIQUE KEY `District_Name` (`District_Name`),
  KEY `Region_ID` (`Region_ID`),
  CONSTRAINT `tbl_district_ibfk_1` FOREIGN KEY (`Region_ID`) REFERENCES `tbl_regions` (`Region_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_doctor_consultation_fee`
--

DROP TABLE IF EXISTS `tbl_doctor_consultation_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_doctor_consultation_fee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Category_name` varchar(50) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  `Consultation_Fee_Type` int(11) NOT NULL,
  `Doctor_Level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Item_ID` (`Item_ID`),
  CONSTRAINT `tbl_doctor_consultation_fee_ibfk_1` FOREIGN KEY (`Item_ID`) REFERENCES `tbl_items` (`Item_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_doctor_rch`
--

DROP TABLE IF EXISTS `tbl_doctor_rch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_doctor_rch` (
  `doctor_RCH_ID` int(11) NOT NULL AUTO_INCREMENT,
  `patient_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Consultation_ID` int(11) NOT NULL,
  `attend_Date` datetime NOT NULL,
  `Status` varchar(20) NOT NULL DEFAULT 'No',
  PRIMARY KEY (`doctor_RCH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_document_library`
--

DROP TABLE IF EXISTS `tbl_document_library`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_document_library` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `content_type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `file_relevant_path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `module_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `module_primary_key` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `open_field` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uuid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_email_recepients`
--

DROP TABLE IF EXISTS `tbl_email_recepients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_email_recepients` (
  `Recepient_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Recepient_Name` varchar(200) NOT NULL,
  `Recepient_Email` varchar(200) NOT NULL,
  `Date_Created` datetime NOT NULL,
  `Created_By` int(11) NOT NULL,
  PRIMARY KEY (`Recepient_ID`),
  UNIQUE KEY `Recepient_Email` (`Recepient_Email`),
  KEY `Created_By` (`Created_By`),
  KEY `Created_By_2` (`Created_By`),
  CONSTRAINT `tbl_email_recepients_ibfk_1` FOREIGN KEY (`Created_By`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_employee`
--

DROP TABLE IF EXISTS `tbl_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_employee` (
  `Employee_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Employee_Name` varchar(300) NOT NULL,
  `Employee_Type` varchar(300) NOT NULL,
  `Employee_Number` varchar(150) NOT NULL,
  `Employee_Title` varchar(150) NOT NULL,
  `Employee_Job_Code` varchar(150) NOT NULL,
  `Employee_Branch_Name` varchar(150) NOT NULL,
  `Department_ID` int(11) DEFAULT NULL,
  `Account_Status` varchar(40) NOT NULL DEFAULT 'active',
  `Branch_ID` int(10) unsigned NOT NULL,
  `user_role_id` int(10) unsigned NOT NULL,
  `Phone_Number` varchar(30) DEFAULT NULL,
  `Registration_Number` varchar(30) DEFAULT NULL,
  `written_employee_name` int(11) DEFAULT NULL,
  `written_employee_signature` int(11) DEFAULT NULL,
  `written_employee_qualification` int(11) DEFAULT NULL,
  `written_employee_mct_registration_no` int(11) DEFAULT NULL,
  `written_employee_phone_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`Employee_ID`),
  KEY `Department_ID` (`Department_ID`),
  CONSTRAINT `tbl_employee_ibfk_1` FOREIGN KEY (`Department_ID`) REFERENCES `tbl_department` (`Department_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_employee_assign_consultation_category_fee`
--

DROP TABLE IF EXISTS `tbl_employee_assign_consultation_category_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_employee_assign_consultation_category_fee` (
  `id` int(11) unsigned NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  KEY `Employee_ID` (`Employee_ID`),
  KEY `id` (`id`),
  CONSTRAINT `tbl_employee_assign_consultation_category_fee_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_employee_assign_consultation_category_fee_ibfk_2` FOREIGN KEY (`id`) REFERENCES `tbl_doctor_consultation_fee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_employee_document`
--

DROP TABLE IF EXISTS `tbl_employee_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_employee_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `document_type` int(11) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_document_employee_id_foreign` (`employee_id`),
  KEY `employee_document_document_id_foreign` (`document_id`),
  CONSTRAINT `employee_document_document_id_foreign` FOREIGN KEY (`document_id`) REFERENCES `tbl_document_library` (`id`),
  CONSTRAINT `employee_document_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_employee_sub_department`
--

DROP TABLE IF EXISTS `tbl_employee_sub_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_employee_sub_department` (
  `Employee_ID` int(11) NOT NULL DEFAULT '0',
  `Sub_Department_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Employee_ID`,`Sub_Department_ID`),
  KEY `Sub_Department_ID` (`Sub_Department_ID`),
  CONSTRAINT `tbl_employee_sub_department_ibfk_1` FOREIGN KEY (`Sub_Department_ID`) REFERENCES `tbl_sub_department` (`Sub_Department_ID`),
  CONSTRAINT `tbl_employee_sub_department_ibfk_2` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_employee_sub_department_ibfk_3` FOREIGN KEY (`Sub_Department_ID`) REFERENCES `tbl_sub_department` (`Sub_Department_ID`),
  CONSTRAINT `tbl_employee_sub_department_ibfk_4` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_employee_sub_department_ibfk_5` FOREIGN KEY (`Sub_Department_ID`) REFERENCES `tbl_sub_department` (`Sub_Department_ID`),
  CONSTRAINT `tbl_employee_sub_department_ibfk_6` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_employee_type`
--

DROP TABLE IF EXISTS `tbl_employee_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_employee_type` (
  `Type_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Type_Name` varchar(100) NOT NULL,
  PRIMARY KEY (`Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_excuse_of_duty`
--

DROP TABLE IF EXISTS `tbl_excuse_of_duty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_excuse_of_duty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Patient_Name` text NOT NULL,
  `Patient_Address` text NOT NULL,
  `Company` text,
  `Age` varchar(10) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `Number_Of_Days` int(11) NOT NULL,
  `Reasons` text NOT NULL,
  `Consultation_ID` int(11) NOT NULL,
  `Round_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  `ed_date` date NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_family_social_history`
--

DROP TABLE IF EXISTS `tbl_family_social_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_family_social_history` (
  `Family_social_history_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Family_social_history` text,
  `Family_social_history_date` datetime DEFAULT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Registration_ID` int(11) NOT NULL,
  PRIMARY KEY (`Family_social_history_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  CONSTRAINT `tbl_family_social_history_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_family_social_history_ibfk_2` FOREIGN KEY (`Registration_ID`) REFERENCES `tbl_patient_registration` (`Registration_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_folio`
--

DROP TABLE IF EXISTS `tbl_folio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_folio` (
  `Folio_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Folio_Number` int(11) NOT NULL,
  `Folio_date` date DEFAULT NULL,
  `Sponsor_ID` int(11) DEFAULT NULL,
  `Branch_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Folio_ID`),
  KEY `Sponsor_ID` (`Sponsor_ID`),
  KEY `Branch_ID` (`Branch_ID`),
  CONSTRAINT `tbl_folio_ibfk_1` FOREIGN KEY (`Sponsor_ID`) REFERENCES `tbl_sponsor` (`Sponsor_ID`),
  CONSTRAINT `tbl_folio_ibfk_2` FOREIGN KEY (`Branch_ID`) REFERENCES `tbl_branches` (`Branch_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_folio_number`
--

DROP TABLE IF EXISTS `tbl_folio_number`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_folio_number` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `month_year` varchar(6) NOT NULL,
  `folio_number` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_general_item_price`
--

DROP TABLE IF EXISTS `tbl_general_item_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_general_item_price` (
  `Item_ID` int(11) NOT NULL DEFAULT '0',
  `Items_Price` float DEFAULT NULL,
  `Currency_ID` varchar(11) DEFAULT '3166',
  KEY `Item_ID` (`Item_ID`),
  CONSTRAINT `tbl_general_item_price_ibfk_1` FOREIGN KEY (`Item_ID`) REFERENCES `tbl_items` (`Item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_git_post_operative_notes`
--

DROP TABLE IF EXISTS `tbl_git_post_operative_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_git_post_operative_notes` (
  `Git_Post_operative_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL COMMENT 'foreign key from tbl_item_list_cache',
  `Surgery_Date` date NOT NULL,
  `Surgery_Date_Time` datetime DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Registration_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL COMMENT 'Doctor ID',
  `Indication_Of_Procedure` varchar(200) DEFAULT NULL,
  `Others` text NOT NULL,
  `Comorbidities` varchar(100) DEFAULT NULL,
  `Management` varchar(255) DEFAULT NULL,
  `Recommendations` text,
  `Medication_Used` varchar(50) DEFAULT NULL,
  `Biopsy_Tailen` varchar(255) DEFAULT NULL,
  `Upper_Point` text,
  `Middle_Point` text NOT NULL,
  `OG_Junction` text,
  `Hiatus_Hernia` text,
  `Other_Lesson` text,
  `Cardia` text,
  `Fundus` text,
  `Body` text,
  `Antrum` text,
  `Pyloms` text,
  `D1` text,
  `D2` text,
  `D3` text,
  PRIMARY KEY (`Git_Post_operative_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `Payment_Item_Cache_List_ID` (`Payment_Item_Cache_List_ID`),
  KEY `Registration_ID` (`Registration_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_git_post_operative_participant`
--

DROP TABLE IF EXISTS `tbl_git_post_operative_participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_git_post_operative_participant` (
  `Participant_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Git_Post_operative_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Employee_Type` varchar(50) DEFAULT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  PRIMARY KEY (`Participant_ID`),
  KEY `Git_Post_operative_ID` (`Git_Post_operative_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  CONSTRAINT `tbl_git_post_operative_participant_ibfk_1` FOREIGN KEY (`Git_Post_operative_ID`) REFERENCES `tbl_git_post_operative_notes` (`Git_Post_operative_ID`),
  CONSTRAINT `tbl_git_post_operative_participant_ibfk_2` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_grn_issue_note`
--

DROP TABLE IF EXISTS `tbl_grn_issue_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_grn_issue_note` (
  `Grn_Issue_Note_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Created_Date` date NOT NULL,
  `Created_Date_Time` datetime NOT NULL,
  `Employee_ID` int(45) NOT NULL,
  `Issue_ID` int(11) NOT NULL,
  `Issue_Description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`Grn_Issue_Note_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Purchase_Order_ID` (`Issue_Description`),
  KEY `Issue_ID` (`Issue_ID`),
  CONSTRAINT `tbl_grn_issue_note_ibfk_1` FOREIGN KEY (`Issue_ID`) REFERENCES `tbl_issues` (`Issue_ID`),
  CONSTRAINT `tbl_grn_issue_note_ibfk_2` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_grn_open_balance`
--

DROP TABLE IF EXISTS `tbl_grn_open_balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_grn_open_balance` (
  `Grn_Open_Balance_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Employee_ID` int(11) NOT NULL,
  `Branch_ID` int(11) NOT NULL,
  `Created_Date_Time` datetime NOT NULL,
  `Sub_Department_ID` int(11) NOT NULL COMMENT 'Receiver',
  `Grn_Open_Balance_Status` varchar(20) NOT NULL DEFAULT 'pending',
  `Saved_Date_Time` datetime DEFAULT NULL,
  `Grn_Open_Balance_Description` varchar(200) DEFAULT NULL,
  `Supervisor_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Grn_Open_Balance_ID`),
  KEY `receiver` (`Sub_Department_ID`),
  KEY `Supervisor_ID` (`Supervisor_ID`),
  KEY `tbl_grn_open_balance_ibfk_1` (`Employee_ID`),
  CONSTRAINT `tbl_grn_open_balance_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_grn_open_balance_ibfk_2` FOREIGN KEY (`Supervisor_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_grn_open_balance_ibfk_3` FOREIGN KEY (`Sub_Department_ID`) REFERENCES `tbl_sub_department` (`Sub_Department_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_grn_open_balance_items`
--

DROP TABLE IF EXISTS `tbl_grn_open_balance_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_grn_open_balance_items` (
  `Open_Balance_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Grn_Open_Balance_ID` int(11) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  `Items_Per_Container` int(11) NOT NULL DEFAULT '0',
  `Container_Qty` int(11) NOT NULL DEFAULT '0',
  `Item_Quantity` int(11) NOT NULL,
  `Buying_Price` float DEFAULT NULL,
  `Item_Remark` varchar(100) DEFAULT NULL,
  `Manufacture_Date` date DEFAULT NULL,
  `Expire_Date` date DEFAULT NULL,
  PRIMARY KEY (`Open_Balance_Item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_grn_purchase_cache`
--

DROP TABLE IF EXISTS `tbl_grn_purchase_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_grn_purchase_cache` (
  `Purchase_Cache_ID` int(30) NOT NULL AUTO_INCREMENT,
  `Quantity_Required` int(11) NOT NULL DEFAULT '0',
  `Item_Remark` varchar(200) DEFAULT NULL,
  `Item_ID` int(11) DEFAULT NULL,
  `Container_Qty` int(11) NOT NULL DEFAULT '0',
  `Items_Per_Container` int(11) NOT NULL DEFAULT '0',
  `Price` double NOT NULL DEFAULT '0',
  `Employee_ID` int(11) DEFAULT NULL,
  `Expire_Date` date NOT NULL,
  PRIMARY KEY (`Purchase_Cache_ID`),
  KEY `item_id` (`Item_ID`),
  KEY `Employee_ID` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_grn_purchase_order`
--

DROP TABLE IF EXISTS `tbl_grn_purchase_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_grn_purchase_order` (
  `Grn_Purchase_Order_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Created_Date` date NOT NULL,
  `Created_Date_Time` datetime NOT NULL,
  `Employee_ID` int(45) NOT NULL,
  `Purchase_Order_ID` int(11) NOT NULL,
  `Debit_Note_Number` varchar(255) DEFAULT NULL,
  `Invoice_Number` varchar(255) DEFAULT NULL,
  `Delivery_Date` date DEFAULT NULL,
  `Delivery_Person` varchar(255) DEFAULT NULL,
  `Grn_Description` varchar(255) NOT NULL,
  PRIMARY KEY (`Grn_Purchase_Order_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Purchase_Order_ID` (`Purchase_Order_ID`),
  CONSTRAINT `tbl_grn_purchase_order_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_grn_purchase_order_ibfk_2` FOREIGN KEY (`Purchase_Order_ID`) REFERENCES `tbl_purchase_order` (`Purchase_Order_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_grn_without_purchase_order`
--

DROP TABLE IF EXISTS `tbl_grn_without_purchase_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_grn_without_purchase_order` (
  `Grn_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Grn_Date_And_Time` datetime NOT NULL,
  `Supervisor_ID` int(11) DEFAULT NULL,
  `Supervisor_Comment` varchar(300) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Sub_Department_ID` int(11) DEFAULT NULL,
  `Supplier_ID` int(11) DEFAULT NULL,
  `Debit_Note_Number` varchar(255) DEFAULT NULL,
  `Invoice_Number` varchar(255) DEFAULT NULL,
  `Delivery_Date` date DEFAULT NULL,
  `Grn_Status` varchar(50) NOT NULL,
  `Grn_Description` varchar(255) NOT NULL,
  PRIMARY KEY (`Grn_ID`),
  KEY `Supplier_ID` (`Supplier_ID`),
  KEY `Sub_Department_ID` (`Sub_Department_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Supervisor_ID` (`Supervisor_ID`),
  CONSTRAINT `tbl_grn_without_purchase_order_ibfk_1` FOREIGN KEY (`Supplier_ID`) REFERENCES `tbl_supplier` (`Supplier_ID`),
  CONSTRAINT `tbl_grn_without_purchase_order_ibfk_2` FOREIGN KEY (`Sub_Department_ID`) REFERENCES `tbl_sub_department` (`Sub_Department_ID`),
  CONSTRAINT `tbl_grn_without_purchase_order_ibfk_3` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_grn_without_purchase_order_ibfk_4` FOREIGN KEY (`Supervisor_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_grn_without_purchase_order_items`
--

DROP TABLE IF EXISTS `tbl_grn_without_purchase_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_grn_without_purchase_order_items` (
  `Purchase_Order_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Grn_ID` int(11) DEFAULT NULL,
  `Item_ID` int(11) NOT NULL,
  `Quantity_Required` int(11) DEFAULT NULL,
  `Container_Qty` int(11) DEFAULT NULL,
  `Items_Per_Container` int(11) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Expire_Date` date DEFAULT NULL,
  `Item_Remark` varchar(150) NOT NULL,
  PRIMARY KEY (`Purchase_Order_Item_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `Grn_ID` (`Grn_ID`),
  CONSTRAINT `tbl_grn_without_purchase_order_items_ibfk_1` FOREIGN KEY (`Item_ID`) REFERENCES `tbl_items` (`Item_ID`),
  CONSTRAINT `tbl_grn_without_purchase_order_items_ibfk_2` FOREIGN KEY (`Grn_ID`) REFERENCES `tbl_grn_without_purchase_order` (`Grn_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_grn_without_purchase_order_items_history`
--

DROP TABLE IF EXISTS `tbl_grn_without_purchase_order_items_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_grn_without_purchase_order_items_history` (
  `Purchase_Order_Item_ID` int(11) NOT NULL,
  `Grn_ID` int(11) DEFAULT NULL,
  `Item_ID` int(11) NOT NULL,
  `Quantity_Required` int(11) DEFAULT NULL,
  `Container_Qty` int(11) DEFAULT NULL,
  `Items_Per_Container` int(11) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Expire_Date` date DEFAULT NULL,
  KEY `Item_ID` (`Item_ID`),
  KEY `Grn_ID` (`Grn_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_grnissue`
--

DROP TABLE IF EXISTS `tbl_grnissue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_grnissue` (
  `grn_issue_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `supplier` varchar(45) NOT NULL,
  `receiver` varchar(45) NOT NULL,
  PRIMARY KEY (`grn_issue_id`),
  UNIQUE KEY `issue_id` (`issue_id`),
  KEY `supplier` (`supplier`),
  KEY `receiver` (`receiver`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_gti_post_operative_diagnosis`
--

DROP TABLE IF EXISTS `tbl_gti_post_operative_diagnosis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_gti_post_operative_diagnosis` (
  `Diagnosis_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Disease_ID` int(11) DEFAULT NULL,
  `Git_Post_operative_ID` int(11) DEFAULT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  PRIMARY KEY (`Diagnosis_ID`),
  KEY `Disease_ID` (`Disease_ID`),
  KEY `Git_Post_operative_ID` (`Git_Post_operative_ID`),
  CONSTRAINT `tbl_gti_post_operative_diagnosis_ibfk_1` FOREIGN KEY (`Disease_ID`) REFERENCES `tbl_disease` (`disease_ID`),
  CONSTRAINT `tbl_gti_post_operative_diagnosis_ibfk_2` FOREIGN KEY (`Git_Post_operative_ID`) REFERENCES `tbl_git_post_operative_notes` (`Git_Post_operative_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_hospital_course_injuries`
--

DROP TABLE IF EXISTS `tbl_hospital_course_injuries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_hospital_course_injuries` (
  `hosp_course_injury_ID` int(11) NOT NULL AUTO_INCREMENT,
  `course_injury` varchar(100) NOT NULL,
  `Branch_ID` int(11) NOT NULL,
  `date_saved` datetime NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  PRIMARY KEY (`hosp_course_injury_ID`),
  UNIQUE KEY `consultation_Type` (`course_injury`),
  UNIQUE KEY `consultation_Type_2` (`course_injury`),
  KEY `Branch_ID` (`Branch_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  CONSTRAINT `tbl_hospital_course_injuries_ibfk_1` FOREIGN KEY (`Branch_ID`) REFERENCES `tbl_branches` (`Branch_ID`),
  CONSTRAINT `tbl_hospital_course_injuries_ibfk_2` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_hospital_ward`
--

DROP TABLE IF EXISTS `tbl_hospital_ward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_hospital_ward` (
  `Hospital_Ward_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Hospital_Ward_Name` varchar(50) DEFAULT NULL,
  `Number_of_Beds` int(11) NOT NULL DEFAULT '30',
  `Branch_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Hospital_Ward_ID`),
  KEY `Branch_ID` (`Branch_ID`),
  CONSTRAINT `tbl_hospital_ward_ibfk_1` FOREIGN KEY (`Branch_ID`) REFERENCES `tbl_branches` (`Branch_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_input_output_nursecommunication`
--

DROP TABLE IF EXISTS `tbl_input_output_nursecommunication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_input_output_nursecommunication` (
  `inputOutput_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `employee_ID` int(11) DEFAULT NULL,
  `Date` datetime NOT NULL,
  `Day_Fluid` varchar(25) NOT NULL,
  `Amount_Fluid` varchar(25) NOT NULL,
  `Amount_Oral` varchar(25) NOT NULL,
  `Urine` varchar(25) NOT NULL,
  `Stool` varchar(25) NOT NULL,
  `Vomit` varchar(25) NOT NULL,
  `Remarks` varchar(25) NOT NULL,
  PRIMARY KEY (`inputOutput_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `employee_ID` (`employee_ID`),
  CONSTRAINT `tbl_input_output_nursecommunication_ibfk_1` FOREIGN KEY (`Registration_ID`) REFERENCES `tbl_patient_registration` (`Registration_ID`),
  CONSTRAINT `tbl_input_output_nursecommunication_ibfk_2` FOREIGN KEY (`consultation_ID`) REFERENCES `tbl_consultation` (`consultation_ID`),
  CONSTRAINT `tbl_input_output_nursecommunication_ibfk_3` FOREIGN KEY (`employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_issuemanual_items`
--

DROP TABLE IF EXISTS `tbl_issuemanual_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_issuemanual_items` (
  `Requisition_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Quantity_Required` int(11) NOT NULL,
  `Item_Remark` varchar(60) DEFAULT NULL,
  `Balance_Before_Issue` int(11) DEFAULT NULL,
  `Item_ID` int(11) DEFAULT NULL,
  `Quantity_Issued` int(11) NOT NULL,
  `Buying_Price` double NOT NULL DEFAULT '0',
  `Issue_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Requisition_Item_ID`),
  KEY `item_id` (`Item_ID`),
  KEY `Issue_ID` (`Issue_ID`),
  KEY `Item_ID_2` (`Item_ID`),
  KEY `Issue_ID_2` (`Issue_ID`),
  CONSTRAINT `tbl_issuemanual_items_ibfk_1` FOREIGN KEY (`Item_ID`) REFERENCES `tbl_items` (`Item_ID`),
  CONSTRAINT `tbl_issuemanual_items_ibfk_2` FOREIGN KEY (`Issue_ID`) REFERENCES `tbl_issuesmanual` (`Issue_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_issues`
--

DROP TABLE IF EXISTS `tbl_issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_issues` (
  `Issue_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Requisition_ID` int(11) DEFAULT NULL,
  `Issue_Date_And_Time` datetime NOT NULL,
  `Issue_Date` date NOT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Issue_Status` varchar(30) DEFAULT 'pending',
  `Issue_Description` varchar(200) DEFAULT NULL,
  `Branch_ID` int(11) DEFAULT NULL,
  `Receiving_Officer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Issue_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Requisition_ID` (`Requisition_ID`),
  CONSTRAINT `tbl_issues_ibfk_1` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_issues_ibfk_2` FOREIGN KEY (`Requisition_ID`) REFERENCES `tbl_requisition` (`Requisition_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_issuesmanual`
--

DROP TABLE IF EXISTS `tbl_issuesmanual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_issuesmanual` (
  `Issue_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Issue_Date_And_Time` datetime NOT NULL,
  `Employee_Issuing` int(11) DEFAULT NULL,
  `Employee_Receiving` int(11) DEFAULT NULL,
  `Store_Issuing` int(11) DEFAULT NULL,
  `Store_Need` int(11) DEFAULT NULL,
  `Branch_ID` int(11) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'pending',
  `Created_Date_And_Time` datetime NOT NULL,
  `Previous_Issue_Note_Manual_Data` text,
  `Reference_No` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Issue_ID`),
  KEY `Employee_ID` (`Employee_Issuing`),
  KEY `Employee_Receiving` (`Employee_Receiving`),
  KEY `Employee_Issuing` (`Employee_Issuing`),
  KEY `Store_Issuing` (`Store_Issuing`),
  KEY `Store_Need` (`Store_Need`),
  KEY `Employee_Receiving_2` (`Employee_Receiving`),
  KEY `Employee_Issuing_2` (`Employee_Issuing`),
  KEY `Branch_ID` (`Branch_ID`),
  CONSTRAINT `tbl_issuesmanual_ibfk_1` FOREIGN KEY (`Employee_Issuing`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_issuesmanual_ibfk_2` FOREIGN KEY (`Employee_Receiving`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `tbl_issuesmanual_ibfk_3` FOREIGN KEY (`Store_Issuing`) REFERENCES `tbl_sub_department` (`Sub_Department_ID`),
  CONSTRAINT `tbl_issuesmanual_ibfk_4` FOREIGN KEY (`Store_Need`) REFERENCES `tbl_sub_department` (`Sub_Department_ID`),
  CONSTRAINT `tbl_issuesmanual_ibfk_5` FOREIGN KEY (`Branch_ID`) REFERENCES `tbl_branches` (`Branch_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_item_category`
--

DROP TABLE IF EXISTS `tbl_item_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_item_category` (
  `Item_Category_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_Category_Name` varchar(150) NOT NULL,
  `Category_Type` varchar(20) NOT NULL DEFAULT 'Service',
  `Revenue_Category` varchar(3) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`Item_Category_ID`),
  UNIQUE KEY `Item_Category_Name` (`Item_Category_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_item_list_cache`
--

DROP TABLE IF EXISTS `tbl_item_list_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_item_list_cache` (
  `Payment_Item_Cache_List_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Check_In_Type` varchar(60) DEFAULT NULL,
  `Category` varchar(100) DEFAULT 'indirect cash',
  `Item_ID` int(11) DEFAULT NULL,
  `Item_Name` varchar(200) DEFAULT NULL,
  `Discount` int(11) DEFAULT NULL,
  `Price` float DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Patient_Direction` varchar(150) DEFAULT NULL,
  `Consultant` varchar(100) DEFAULT NULL,
  `Consultant_ID` int(11) DEFAULT NULL,
  `Status` varchar(40) DEFAULT 'active',
  `Employee_Created` int(11) DEFAULT NULL COMMENT 'Employee Added Item',
  `Created_Date_Time` datetime DEFAULT NULL,
  `Approved_By` int(11) DEFAULT NULL,
  `Approval_Date_Time` datetime DEFAULT NULL,
  `Dispensor` int(11) DEFAULT NULL,
  `Dispense_Date_Time` datetime DEFAULT NULL,
  `Payment_Cache_ID` int(11) DEFAULT NULL,
  `Transaction_Date_And_Time` datetime NOT NULL,
  `Process_Status` varchar(40) NOT NULL DEFAULT 'not served',
  `Doctor_Comment` varchar(500) DEFAULT NULL,
  `Sub_Department_ID` int(11) DEFAULT NULL,
  `Transaction_Type` varchar(10) DEFAULT NULL,
  `payment_type` enum('post','pre') NOT NULL DEFAULT 'post',
  `Service_Date_And_Time` datetime DEFAULT NULL,
  `Edited_Quantity` int(11) NOT NULL DEFAULT '0',
  `Patient_Payment_ID` int(50) DEFAULT NULL,
  `Payment_Date_And_Time` date DEFAULT NULL,
  `Employee_Remove` int(11) DEFAULT NULL,
  `Remove_Date` datetime DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL COMMENT 'Department Employee ID where Patient was saved',
  `Priority` varchar(20) DEFAULT NULL,
  `romoving_staff` int(11) DEFAULT NULL,
  `removing_status` varchar(20) NOT NULL DEFAULT 'No',
  `removing_date` datetime DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `ServedDateTime` datetime DEFAULT NULL,
  `ServedBy` int(11) DEFAULT NULL,
  `Surgery_hour` int(11) DEFAULT NULL,
  `Surgery_min` int(11) DEFAULT NULL,
  `Procedure_Location` varchar(10) DEFAULT 'Others',
  `Transaction_ID` int(11) NOT NULL DEFAULT '0',
  `ePayment_Status` varchar(15) NOT NULL DEFAULT 'pending',
  `Payment_Status` int(1) DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Round_ID` int(11) DEFAULT NULL,
  `previous_payment_status` int(1) DEFAULT NULL,
  `previous_process_status` int(1) DEFAULT NULL,
  PRIMARY KEY (`Payment_Item_Cache_List_ID`),
  KEY `Patient_Payment_ID` (`Payment_Cache_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `Item_ID_2` (`Item_ID`),
  KEY `Payment_Cache_ID` (`Payment_Cache_ID`),
  KEY `Dispensor` (`Dispensor`),
  KEY `romoving_staff` (`romoving_staff`),
  KEY `Sub_Department_ID` (`Sub_Department_ID`),
  KEY `Consultant_ID` (`Consultant_ID`),
  KEY `Employee_Remove` (`Employee_Remove`),
  KEY `Dispensor_2` (`Dispensor`),
  KEY `Payment_Cache_ID_2` (`Payment_Cache_ID`),
  KEY `Employee_Created` (`Employee_Created`),
  KEY `Patient_Payment_ID_2` (`Patient_Payment_ID`) USING BTREE,
  KEY `item_list_cache_consultation_id_foreign` (`consultation_ID`),
  KEY `item_list_cache_round_id_foreign` (`Round_ID`),
  KEY `idx_doctor_orders` (`Transaction_Date_And_Time`,`Payment_Cache_ID`,`Check_In_Type`,`Item_ID`,`Consultant`,`Priority`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_item_price`
--

DROP TABLE IF EXISTS `tbl_item_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_item_price` (
  `Sponsor_ID` int(11) NOT NULL DEFAULT '0',
  `Item_ID` int(11) NOT NULL DEFAULT '0',
  `Items_Price` float DEFAULT NULL,
  `Currency_ID` varchar(11) DEFAULT '3166',
  PRIMARY KEY (`Sponsor_ID`,`Item_ID`),
  KEY `Item_ID` (`Item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_item_report`
--

DROP TABLE IF EXISTS `tbl_item_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_item_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `payment_item_cache_list_id` int(11) NOT NULL,
  `consultation_type` varchar(100) NOT NULL,
  `item_report_template_id` int(11) DEFAULT NULL,
  `item_report` text,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `payment_item_cache_list_id` (`payment_item_cache_list_id`),
  KEY `item_report_item_report_template_id_foreign` (`item_report_template_id`),
  KEY `item_report_created_by_foreign` (`created_by`),
  KEY `item_report_modified_by_foreign` (`modified_by`),
  KEY `item_report_item_id_foreign` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_item_report_template`
--

DROP TABLE IF EXISTS `tbl_item_report_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_item_report_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `item_report_template` text,
  `template_name` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_name` (`template_name`),
  KEY `item_report_template_created_by_foreign` (`created_by`),
  KEY `item_report_template_modified_by_foreign` (`modified_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_item_subcategory`
--

DROP TABLE IF EXISTS `tbl_item_subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_item_subcategory` (
  `Item_Subcategory_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_Subcategory_Name` varchar(150) NOT NULL,
  `Item_category_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Item_Subcategory_ID`),
  UNIQUE KEY `Item_Subcategory_Name` (`Item_Subcategory_Name`),
  KEY `Item_category_ID` (`Item_category_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_items`
--

DROP TABLE IF EXISTS `tbl_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_items` (
  `Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_Type` varchar(100) NOT NULL,
  `NHIFItem_Type` int(11) DEFAULT '3',
  `Product_Code` varchar(100) DEFAULT NULL,
  `_old_Product_Code` varchar(100) DEFAULT NULL,
  `Unit_Of_Measure` varchar(40) DEFAULT NULL,
  `Product_Name` varchar(700) NOT NULL,
  `Item_Subcategory_ID` int(11) DEFAULT NULL,
  `Status` varchar(30) DEFAULT NULL,
  `Reoder_Level` varchar(50) DEFAULT NULL,
  `Consultation_Type` varchar(30) NOT NULL,
  `Can_Be_Substituted_In_Doctors_Page` varchar(10) NOT NULL DEFAULT 'no',
  `Visible_Status` varchar(30) NOT NULL DEFAULT 'all',
  `Ward_Round_Item` varchar(10) NOT NULL DEFAULT 'no',
  `item_kind` varchar(20) NOT NULL,
  `Last_Buy_Price` int(11) NOT NULL DEFAULT '0',
  `CASH` int(11) NOT NULL,
  `CREDIT` int(11) NOT NULL,
  `NHIF` int(11) NOT NULL,
  `Classification` varchar(50) NOT NULL,
  `Can_Be_Sold` varchar(10) NOT NULL DEFAULT 'no',
  `Can_Be_Stocked` varchar(10) NOT NULL DEFAULT 'no',
  `allow_zero_price` bit(1) NOT NULL DEFAULT b'0',
  `Ct_Scan_Item` varchar(10) NOT NULL DEFAULT 'no',
  `consultation_Item` varchar(10) NOT NULL,
  `Seen_On_Allpayments` varchar(10) NOT NULL DEFAULT 'yes',
  `Particular_Type` varchar(15) NOT NULL DEFAULT 'Others',
  `special_appointment_item` varchar(2) DEFAULT 'N',
  PRIMARY KEY (`Item_ID`),
  UNIQUE KEY `Product_Name` (`Product_Name`),
  KEY `Item_Subcategory_ID` (`Item_Subcategory_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2099 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_items_balance`
--

DROP TABLE IF EXISTS `tbl_items_balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_items_balance` (
  `Item_ID` int(11) NOT NULL DEFAULT '0',
  `Item_Balance` int(11) NOT NULL DEFAULT '0',
  `Item_Temporary_Balance` int(11) NOT NULL DEFAULT '0',
  `Sub_Department_ID` int(11) NOT NULL DEFAULT '0',
  `Sub_Department_Type` varchar(40) DEFAULT NULL,
  `Reorder_Level` int(11) NOT NULL DEFAULT '0',
  `Reorder_Level_Status` varchar(20) NOT NULL DEFAULT 'normal',
  PRIMARY KEY (`Item_ID`,`Sub_Department_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `Sub_Department_ID` (`Sub_Department_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_items_template`
--

DROP TABLE IF EXISTS `tbl_items_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_items_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Item_ID` int(11) NOT NULL,
  `Template_Number` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_items_template_results`
--

DROP TABLE IF EXISTS `tbl_items_template_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_items_template_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  `Template_Number` int(2) NOT NULL,
  `Template_Field` int(2) NOT NULL,
  `Result` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_laboratory_parameters`
--

DROP TABLE IF EXISTS `tbl_laboratory_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_laboratory_parameters` (
  `Laboratory_Parameter_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Laboratory_Parameter_Name` varchar(100) NOT NULL,
  PRIMARY KEY (`Laboratory_Parameter_ID`),
  UNIQUE KEY `Laboratory_Parameter_Name` (`Laboratory_Parameter_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_laboratory_specimen`
--

DROP TABLE IF EXISTS `tbl_laboratory_specimen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_laboratory_specimen` (
  `Specimen_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Specimen_Name` varchar(45) DEFAULT NULL,
  `Sample_Container` varchar(100) NOT NULL,
  `Status` varchar(50) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`Specimen_ID`),
  UNIQUE KEY `Sample_Name_2` (`Specimen_Name`),
  KEY `Sample_Name` (`Specimen_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_laboratory_test_parameters`
--

DROP TABLE IF EXISTS `tbl_laboratory_test_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_laboratory_test_parameters` (
  `Laboratory_Test_Parameter_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Laboratory_Parameter_ID` int(11) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  `Unit_Of_Measure` varchar(45) NOT NULL,
  `Lower_Value` decimal(11,0) NOT NULL,
  `Operator` varchar(45) NOT NULL,
  `Higher_Value` decimal(11,0) NOT NULL,
  `Normal_Value` varchar(20) NOT NULL,
  `Result_Type` varchar(20) NOT NULL,
  PRIMARY KEY (`Laboratory_Test_Parameter_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `Laboratory_Parameter_ID` (`Laboratory_Parameter_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_laboratory_test_specimen`
--

DROP TABLE IF EXISTS `tbl_laboratory_test_specimen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_laboratory_test_specimen` (
  `Laboratory_Test_specimen_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_ID` int(11) NOT NULL,
  `Specimen_ID` int(11) NOT NULL,
  `Laboratory_Template_ID` int(11) NOT NULL,
  PRIMARY KEY (`Laboratory_Test_specimen_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `fk_tbl_laboratory_test_specimen_tbl_laboratory_specimen1_idx` (`Specimen_ID`),
  KEY `Item_ID_2` (`Item_ID`),
  KEY `Specimen_ID` (`Specimen_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_labour`
--

DROP TABLE IF EXISTS `tbl_labour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_labour` (
  `labour_ID` int(11) NOT NULL AUTO_INCREMENT,
  `admitted_from` varchar(50) NOT NULL,
  `reffered_from` varchar(50) NOT NULL,
  `danger_signs` varchar(50) NOT NULL,
  `patient_ID` int(11) NOT NULL,
  `partner_name` varchar(50) NOT NULL,
  `gravida` int(11) NOT NULL,
  `parity` int(11) NOT NULL,
  `current_children` int(11) NOT NULL,
  `abortion` int(11) NOT NULL,
  `lnmp` varchar(50) NOT NULL,
  `edd` varchar(50) NOT NULL,
  `ga` varchar(50) NOT NULL,
  `obste_history_1` varchar(50) NOT NULL,
  `obste_history_2` varchar(50) NOT NULL,
  `obste_history_3` varchar(50) NOT NULL,
  `visits` int(11) NOT NULL,
  `IPT_doses` varchar(50) NOT NULL,
  `TT_doses` varchar(50) NOT NULL,
  `ITN_doses` varchar(50) NOT NULL,
  `bloodgroup` varchar(20) NOT NULL,
  `last_Hb` varchar(20) NOT NULL,
  `pmtct` varchar(20) NOT NULL,
  `art` varchar(20) NOT NULL,
  `vdrl` varchar(20) NOT NULL,
  `labour_onset` varchar(20) NOT NULL,
  `membranes_rapture` varchar(20) NOT NULL,
  `fetal_mvt` varchar(20) NOT NULL,
  `general` varchar(50) NOT NULL,
  `pulse_rate` int(11) NOT NULL,
  `rufaasababu` text NOT NULL,
  `maoni` text NOT NULL,
  PRIMARY KEY (`labour_ID`),
  KEY `ITN_doses` (`ITN_doses`),
  KEY `patient_ID` (`patient_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_labour_ward_notes`
--

DROP TABLE IF EXISTS `tbl_labour_ward_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_labour_ward_notes` (
  `Note_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Consultation_ID` int(11) DEFAULT NULL,
  `Admision_ID` int(11) DEFAULT NULL,
  `Note_Date_Time` datetime DEFAULT NULL,
  `Palpation` varchar(20) NOT NULL,
  `Presentation` varchar(20) NOT NULL,
  `Position` varchar(20) NOT NULL,
  `Contraction` varchar(20) NOT NULL,
  `Liquir` varchar(20) NOT NULL,
  `Colour` varchar(30) DEFAULT NULL,
  `Pv_Examination` varchar(60) NOT NULL,
  `OS` varchar(20) NOT NULL,
  `Membrane` varchar(20) NOT NULL,
  `Temperature` double DEFAULT NULL,
  `Purse` double DEFAULT NULL,
  `Respiration` double DEFAULT NULL,
  `BP` double DEFAULT NULL,
  `FHR` double DEFAULT NULL,
  `BMI` varchar(10) DEFAULT NULL,
  `Exp` text,
  `Remarks` text,
  PRIMARY KEY (`Note_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Consultation_ID` (`Consultation_ID`),
  KEY `Admision_ID` (`Admision_ID`),
  KEY `Registration_ID` (`Registration_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_labour_ward_notes_history`
--

DROP TABLE IF EXISTS `tbl_labour_ward_notes_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_labour_ward_notes_history` (
  `History_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Note_ID` int(11) NOT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Note_Date_Time` datetime DEFAULT NULL,
  `Palpation` varchar(20) NOT NULL,
  `Presentation` varchar(20) NOT NULL,
  `Position` varchar(20) NOT NULL,
  `Contraction` varchar(20) NOT NULL,
  `Liquir` varchar(20) NOT NULL,
  `Colour` varchar(30) DEFAULT NULL,
  `Pv_Examination` varchar(60) NOT NULL,
  `OS` varchar(20) NOT NULL,
  `Membrane` varchar(20) NOT NULL,
  `Temperature` double DEFAULT NULL,
  `Purse` double DEFAULT NULL,
  `Respiration` double DEFAULT NULL,
  `BP` double DEFAULT NULL,
  `FHR` double DEFAULT NULL,
  `BMI` varchar(10) DEFAULT NULL,
  `Exp` text,
  `Remarks` text,
  PRIMARY KEY (`History_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Note_ID` (`Note_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_license`
--

DROP TABLE IF EXISTS `tbl_license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_license` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `license` varchar(200) NOT NULL,
  `license_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `license` (`license`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_message`
--

DROP TABLE IF EXISTS `tbl_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `message_type` int(11) NOT NULL,
  `message` text NOT NULL,
  `message_date` datetime NOT NULL,
  `check_in_id` int(11) DEFAULT NULL,
  `registration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `message_check_in_id_foreign` (`check_in_id`),
  KEY `message_employee_id_foreign` (`employee_id`),
  KEY `message_registration_id_foreign` (`registration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_mulnutrition_observation`
--

DROP TABLE IF EXISTS `tbl_mulnutrition_observation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mulnutrition_observation` (
  `mulnutrition_observation_id` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `employee_ID` int(11) DEFAULT NULL,
  `date_time` datetime NOT NULL,
  `temp` int(11) NOT NULL,
  `Pr` int(11) NOT NULL,
  `Resp` int(11) NOT NULL,
  `So` int(11) NOT NULL,
  `daily_bwt` varchar(100) NOT NULL,
  `feed_amount` varchar(100) NOT NULL,
  `cup_Amount` varchar(100) NOT NULL,
  `oral_taken` varchar(100) NOT NULL,
  `ngt_taken` varchar(100) NOT NULL,
  `vomitted_mls` varchar(100) NOT NULL,
  `diarrhoea` varchar(100) NOT NULL,
  `Remarks` varchar(200) NOT NULL,
  PRIMARY KEY (`mulnutrition_observation_id`),
  KEY `Registration_ID` (`Registration_ID`,`consultation_ID`,`employee_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `employee_ID` (`employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_notification`
--

DROP TABLE IF EXISTS `tbl_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `message` text,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_notification_template`
--

DROP TABLE IF EXISTS `tbl_notification_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `template` text,
  `title` varchar(50) DEFAULT NULL,
  `template_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_notification_user`
--

DROP TABLE IF EXISTS `tbl_notification_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `registration_id` int(11) DEFAULT NULL,
  `notification_id` int(11) DEFAULT NULL,
  `read_flag` int(11) DEFAULT NULL,
  `send_flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `registration_id` (`registration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_nurse`
--

DROP TABLE IF EXISTS `tbl_nurse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nurse` (
  `Nurse_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Allegies` varchar(100) DEFAULT NULL,
  `Special_Condition` varchar(100) DEFAULT NULL,
  `bmi` varchar(50) NOT NULL,
  `Nurse_DateTime` datetime DEFAULT NULL,
  `Patient_Payment_Item_List_ID` int(11) DEFAULT NULL,
  `nurse_comment` varchar(200) NOT NULL,
  `emergency` varchar(10) NOT NULL,
  `modeoftransprot` varchar(200) NOT NULL,
  `accomaniedby` varchar(200) NOT NULL,
  `pastmedhist` varchar(200) NOT NULL,
  `current_medications` varchar(200) NOT NULL,
  PRIMARY KEY (`Nurse_ID`),
  KEY `fk_tbl_Nurse_tbl_employee1_idx` (`Employee_ID`),
  KEY `fk_tbl_Nurse_tbl_patient_registration1_idx` (`Registration_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Patient_Payment_Item_List_ID` (`Patient_Payment_Item_List_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_nurse_care`
--

DROP TABLE IF EXISTS `tbl_nurse_care`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nurse_care` (
  `Nurse_Care_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Status` varchar(25) NOT NULL,
  `Medication` varchar(25) NOT NULL,
  `Medication_last` varchar(25) NOT NULL,
  `nowTime` datetime NOT NULL,
  `Scan` varchar(25) NOT NULL,
  `Notscan` varchar(25) NOT NULL,
  `Administer_Medication` time NOT NULL,
  `TimeElapse` varchar(25) NOT NULL,
  `Discontinue_Medication` varchar(25) NOT NULL,
  `Time_medication` time NOT NULL,
  PRIMARY KEY (`Nurse_Care_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_nurse_notes`
--

DROP TABLE IF EXISTS `tbl_nurse_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nurse_notes` (
  `Notes_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Admision_ID` int(11) DEFAULT NULL,
  `Consultation_ID` int(11) DEFAULT NULL,
  `Notes_Date_Time` datetime DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Notes` text NOT NULL,
  `Registration_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Notes_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Admision_ID` (`Admision_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Consultation_ID` (`Consultation_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_nursecommunication_observation`
--

DROP TABLE IF EXISTS `tbl_nursecommunication_observation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nursecommunication_observation` (
  `Observation_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `employee_ID` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `Blood_Pressure` varchar(20) NOT NULL,
  `Pulse_Blood` varchar(20) NOT NULL,
  `Temperature` varchar(20) NOT NULL,
  `Resp_Bpressure` varchar(20) NOT NULL,
  `Fluid_Drug` varchar(20) NOT NULL,
  `Oral_Fluid` varchar(20) NOT NULL,
  `Drainage` varchar(20) NOT NULL,
  `Gas_Tric` varchar(20) NOT NULL,
  `Urine` varchar(20) NOT NULL,
  PRIMARY KEY (`Observation_ID`),
  KEY `consultation_ID` (`consultation_ID`,`employee_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `employee_ID` (`employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_nursecommunication_paediatric`
--

DROP TABLE IF EXISTS `tbl_nursecommunication_paediatric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_nursecommunication_paediatric` (
  `nursecom_paediatric_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `employee_ID` int(11) DEFAULT NULL,
  `date_time` datetime NOT NULL,
  `temp` int(11) NOT NULL,
  `Pr` int(11) NOT NULL,
  `Resp` int(11) NOT NULL,
  `So` int(11) NOT NULL,
  `patient_Problem` varchar(100) NOT NULL,
  `nursing_diagnosis` varchar(100) NOT NULL,
  `expected_outcome` varchar(100) NOT NULL,
  `implementation` varchar(100) NOT NULL,
  `outcome` varchar(100) NOT NULL,
  `investigation` varchar(100) NOT NULL,
  `Remarks` varchar(200) NOT NULL,
  PRIMARY KEY (`nursecom_paediatric_ID`),
  KEY `Registration_ID` (`Registration_ID`,`consultation_ID`,`employee_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `employee_ID` (`employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_ogd_post_operative_diagnosis`
--

DROP TABLE IF EXISTS `tbl_ogd_post_operative_diagnosis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ogd_post_operative_diagnosis` (
  `Diagnosis_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Disease_ID` int(11) DEFAULT NULL,
  `Ogd_Post_operative_ID` int(11) DEFAULT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  PRIMARY KEY (`Diagnosis_ID`),
  KEY `Post_operative_ID` (`Ogd_Post_operative_ID`),
  KEY `Disease_ID` (`Disease_ID`),
  KEY `Payment_Item_Cache_List_ID` (`Payment_Item_Cache_List_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_ogd_post_operative_notes`
--

DROP TABLE IF EXISTS `tbl_ogd_post_operative_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ogd_post_operative_notes` (
  `Ogd_Post_operative_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL COMMENT 'foreign key from tbl_item_list_cache',
  `Surgery_Date` date NOT NULL,
  `Surgery_Date_Time` datetime DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Registration_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL COMMENT 'Doctor ID',
  `Indication_Of_Procedure` varchar(100) DEFAULT NULL,
  `Others` text NOT NULL,
  `Quality_Of_Bowel` varchar(20) DEFAULT NULL,
  `Endoscorpic_Internvention` varchar(255) DEFAULT NULL,
  `Type_And_Dose` varchar(100) DEFAULT NULL,
  `Adverse_Event_Resulting` varchar(100) DEFAULT NULL,
  `Management_Recommendation` varchar(500) DEFAULT NULL,
  `Extent_Of_Examination` varchar(100) DEFAULT NULL,
  `Commobility` varchar(100) DEFAULT NULL,
  `Anal_lessor` text,
  `Haemoral` text,
  `PR` text,
  `Symd` text,
  `Dex_colon` text,
  `Splenic` text,
  `Ple_Tran_Col` text,
  `Status` varchar(10) NOT NULL DEFAULT 'pending',
  `Hepatic_Flexure` text,
  `Ascending_Colon` text,
  `Caecum` text,
  `Terminal_Ileum` text,
  `rectum` text NOT NULL,
  PRIMARY KEY (`Ogd_Post_operative_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `Payment_Item_Cache_List_ID` (`Payment_Item_Cache_List_ID`),
  KEY `Registration_ID` (`Registration_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_optical_items_list_cache`
--

DROP TABLE IF EXISTS `tbl_optical_items_list_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_optical_items_list_cache` (
  `Item_Cache_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Claim_Form_Number` varchar(50) DEFAULT NULL,
  `Billing_Type` varchar(50) NOT NULL,
  `Type_Of_Check_In` varchar(30) NOT NULL,
  `Folio_Number` int(11) DEFAULT NULL,
  `Sponsor_Name` varchar(100) NOT NULL,
  `Sponsor_ID` int(11) NOT NULL,
  `Sub_Department_ID` int(11) DEFAULT NULL,
  `Item_ID` int(11) DEFAULT NULL,
  `Discount` int(11) NOT NULL DEFAULT '0',
  `Price` float NOT NULL DEFAULT '0',
  `Quantity` int(11) NOT NULL DEFAULT '0',
  `Comment` varchar(500) DEFAULT NULL,
  `Patient_Direction` varchar(150) DEFAULT NULL,
  `Consultant_ID` int(11) NOT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Registration_ID` int(11) DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Transaction_Date_Time` datetime DEFAULT NULL,
  PRIMARY KEY (`Item_Cache_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `tbl_optical_items_list_cache_ibfk_1` (`Registration_ID`),
  KEY `tbl_optical_items_list_cache_ibfk_2` (`Employee_ID`),
  KEY `tbl_optical_items_list_cache_ibfk_3` (`Consultant_ID`),
  KEY `tbl_optical_items_list_cache_ibfk_4` (`Sponsor_ID`),
  KEY `tbl_optical_items_list_cache_ibfk_5` (`Sub_Department_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_paediatric`
--

DROP TABLE IF EXISTS `tbl_paediatric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_paediatric` (
  `Paediatric_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Have_TT` varchar(45) DEFAULT NULL,
  `Current_Hiv_Status` varchar(45) DEFAULT NULL,
  `Date_Of_BCG_Vaccination` date DEFAULT NULL,
  `Date_Of_Pentavalent_Vaccination` date DEFAULT NULL,
  `Date_Of_Pnuemoccocal_Vaccination` date DEFAULT NULL,
  `Date_Of_OPC0_Vaccination` date DEFAULT NULL,
  `Date_Of_Measles_Vaccination` date DEFAULT NULL,
  `Vitamin_Six_Months` varchar(45) DEFAULT NULL,
  `Vitamin_Twelve_Months` varchar(45) DEFAULT NULL,
  `Vitamin_Eighteen_Months` varchar(45) DEFAULT NULL,
  `Vitamin_Twenty_Four_Months` varchar(45) DEFAULT NULL,
  `Vitamin_Thirty_Six_Months` varchar(45) DEFAULT NULL,
  `Vitamin_Fourty_Eight_Months` varchar(45) DEFAULT NULL,
  `Fifty_Nine_Months` varchar(45) DEFAULT NULL,
  `Abendazole_Six_Months` varchar(45) DEFAULT NULL,
  `Abendazole_Twelve_Months` varchar(45) DEFAULT NULL,
  `Abendazole_Eighteen_Months` varchar(45) DEFAULT NULL,
  `Abendazole_Twenty_Four_Months` varchar(45) DEFAULT NULL,
  `Abendazole_Thirty_Months` varchar(45) DEFAULT NULL,
  `Received_IPT` varchar(45) DEFAULT NULL,
  `Vaccination_Comment` text,
  `Partiner_Name` varchar(45) DEFAULT NULL,
  `Planning_Consultation` varchar(45) DEFAULT NULL,
  `Previous_Preginancies` varchar(45) DEFAULT NULL,
  `Number_Of_Births` varchar(45) DEFAULT NULL,
  `Aborted_Preginancies` varchar(45) DEFAULT NULL,
  `Children_Died_Under_Seven` varchar(45) DEFAULT NULL,
  `Number_Of_Current_Children` varchar(45) DEFAULT NULL,
  `Age_Of_Last_Child` varchar(45) DEFAULT NULL,
  `Contraceptive_Method` varchar(45) DEFAULT NULL,
  `Recommended_Date_For_Review_Consultation` varchar(45) DEFAULT NULL,
  `Other_Comments` varchar(45) DEFAULT NULL,
  `General_Appearance` varchar(45) DEFAULT NULL,
  `Ent` varchar(45) DEFAULT NULL,
  `Neck` varchar(45) DEFAULT NULL,
  `Heart` varchar(45) DEFAULT NULL,
  `Lungs` varchar(45) DEFAULT NULL,
  `Abdomen` varchar(45) DEFAULT NULL,
  `Spine` varchar(45) DEFAULT NULL,
  `Extremities` varchar(45) DEFAULT NULL,
  `Genitalia` int(11) DEFAULT NULL,
  `Skin` varchar(45) DEFAULT NULL,
  `Neurological` varchar(45) DEFAULT NULL,
  `Physical_Health` varchar(45) DEFAULT NULL,
  `Hearing` varchar(45) DEFAULT NULL,
  `Speech` varchar(45) DEFAULT NULL,
  `Vision` varchar(45) DEFAULT NULL,
  `Vomiting` varchar(45) DEFAULT NULL,
  `Social_Behavior` varchar(45) DEFAULT NULL,
  `Posture` varchar(45) DEFAULT NULL,
  `Large_Movoment` varchar(45) DEFAULT NULL,
  `Fine_Movement` varchar(45) DEFAULT NULL,
  `Recognition` varchar(45) DEFAULT NULL,
  `Comprehension` varchar(45) DEFAULT NULL,
  `Interaction` varchar(45) DEFAULT NULL,
  `Milestone_Comment` varchar(45) DEFAULT NULL,
  `Breast_Feeding_Month1` varchar(45) DEFAULT NULL,
  `Breast_Feeding_Month2` varchar(45) DEFAULT NULL,
  `Breast_Feeding_Month3` varchar(45) DEFAULT NULL,
  `Breast_Feeding_Month4` varchar(45) DEFAULT NULL,
  `Breast_Feeding_Month5` varchar(45) DEFAULT NULL,
  `Breast_Feeding_Month6` varchar(45) DEFAULT NULL,
  `Formula_Month1` varchar(45) DEFAULT NULL,
  `Formula_Month2` varchar(45) DEFAULT NULL,
  `Formula_Month3` varchar(45) DEFAULT NULL,
  `Formula_Month4` varchar(45) DEFAULT NULL,
  `Formula_Month5` varchar(45) DEFAULT NULL,
  `Formula_Month6` varchar(45) DEFAULT NULL,
  `Formula_Breast_Month1` varchar(45) DEFAULT NULL,
  `Formula_Breast_Month2` varchar(45) DEFAULT NULL,
  `Formula_Breast_Month3` varchar(45) DEFAULT NULL,
  `Formula_Breast_Month4` varchar(45) DEFAULT NULL,
  `Formula_Breast_Month5` varchar(45) DEFAULT NULL,
  `Formula_Breast_Month6` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type1_1` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type2_2` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type1_3` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type1_5` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type1_7` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type1_9` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type1_11` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type2_4` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type2_6` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type2_8` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type2_10` varchar(45) DEFAULT NULL,
  `Prophylaxis_Cotrimoxazole_Type2_12` varchar(45) DEFAULT NULL,
  `ARV_Therapy` varchar(45) DEFAULT NULL,
  `Child_Hiv_Status_Four_Weeks` varchar(45) DEFAULT NULL,
  `Child_Hiv_Status_Six_Weeks` varchar(45) DEFAULT NULL,
  `Hiv_Related_Comments` varchar(45) DEFAULT NULL,
  `Diahorrea_Duration` varchar(45) DEFAULT NULL,
  `Dehydration_Degree` varchar(45) DEFAULT NULL,
  `Stool_Blood` varchar(45) DEFAULT NULL,
  `Fever` varchar(45) DEFAULT NULL,
  `Vomiting2` varchar(45) DEFAULT NULL,
  `Anything_Else` varchar(45) DEFAULT NULL,
  `Treatment` varchar(45) DEFAULT NULL,
  `Oral_Rehydration_Solution` varchar(45) DEFAULT NULL,
  `Iv_Medication` varchar(45) DEFAULT NULL,
  `Zinc` varchar(45) DEFAULT NULL,
  `Anyother_Treatment` varchar(45) DEFAULT NULL,
  `Treatment_Duration` varchar(45) DEFAULT NULL,
  `Outcome` varchar(45) DEFAULT NULL,
  `Paediatric_Registration_Date` date DEFAULT NULL,
  PRIMARY KEY (`Paediatric_ID`),
  KEY `fk_tbl_paediatric_tbl_patient_registration1_idx` (`Registration_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_paediatric_history`
--

DROP TABLE IF EXISTS `tbl_paediatric_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_paediatric_history` (
  `Paediatric_history_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Paediatric_history` text,
  `Paediatric_date` datetime DEFAULT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Registration_ID` int(11) NOT NULL,
  PRIMARY KEY (`Paediatric_history_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Registration_ID` (`Registration_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_paediatric_observation`
--

DROP TABLE IF EXISTS `tbl_paediatric_observation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_paediatric_observation` (
  `paediatric_observation_Id` int(11) NOT NULL AUTO_INCREMENT,
  `observation_admission_Id` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  `temp` varchar(50) NOT NULL,
  `Pr` varchar(50) NOT NULL,
  `Resp` varchar(50) NOT NULL,
  `So` varchar(50) NOT NULL,
  `problem` varchar(50) NOT NULL,
  `diagnosis` varchar(50) NOT NULL,
  `exp_outcome` varchar(50) NOT NULL,
  `implememntation` varchar(50) NOT NULL,
  `outcome` varchar(50) NOT NULL,
  `investigation` varchar(50) NOT NULL,
  `remarks` varchar(50) NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Registration_ID` int(11) NOT NULL,
  PRIMARY KEY (`paediatric_observation_Id`),
  KEY `paediatric_observation_employee_id_foreign` (`Employee_ID`),
  KEY `paediatric_observation_registration_id_foreign` (`Registration_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_parameters`
--

DROP TABLE IF EXISTS `tbl_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_parameters` (
  `parameter_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Parameter_Name` varchar(50) NOT NULL,
  `unit_of_measure` varchar(30) NOT NULL,
  `lower_value` varchar(100) NOT NULL,
  `operator` varchar(10) NOT NULL,
  `higher_value` varchar(100) NOT NULL,
  `result_type` varchar(60) NOT NULL,
  `Normal_value` varchar(50) NOT NULL,
  `show_status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`parameter_ID`),
  UNIQUE KEY `Parameter_Name` (`Parameter_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=746 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_bill`
--

DROP TABLE IF EXISTS `tbl_patient_bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_bill` (
  `Patient_Bill_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `Patient_Status` varchar(30) NOT NULL DEFAULT 'Outpatient',
  `Date_Time` datetime NOT NULL,
  `Status` varchar(15) DEFAULT 'active',
  PRIMARY KEY (`Patient_Bill_ID`),
  KEY `Registration_ID` (`Registration_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_blood_data`
--

DROP TABLE IF EXISTS `tbl_patient_blood_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_blood_data` (
  `Blood_ID` int(12) NOT NULL AUTO_INCREMENT,
  `Donor_ID` int(12) NOT NULL,
  `Blood_Group` varchar(10) NOT NULL,
  `Blood_Batch` varchar(20) NOT NULL,
  `Blood_Runner` varchar(150) NOT NULL,
  `Blood_Volume` int(45) NOT NULL,
  `Status` varchar(23) NOT NULL,
  `Date_Of_Transfusion` date NOT NULL,
  `Blood_Expire_Date` date NOT NULL,
  `Transfusion_Date_Time` datetime NOT NULL,
  `Employee_ID` int(12) NOT NULL,
  PRIMARY KEY (`Blood_ID`),
  KEY `Donor_ID` (`Donor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_cache_results`
--

DROP TABLE IF EXISTS `tbl_patient_cache_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_cache_results` (
  `Patient_Cache_Results_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Laboratory_Parameter_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Laboratory_Result` varchar(200) NOT NULL,
  `Result_Datetime` datetime NOT NULL,
  `Payment_Cache_ID` int(11) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  `Patient_ID` int(11) NOT NULL,
  PRIMARY KEY (`Patient_Cache_Results_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Laboratory_Parameter_ID` (`Laboratory_Parameter_ID`,`Payment_Cache_ID`,`Item_ID`,`Patient_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `Patient_ID` (`Patient_ID`),
  KEY `Payment_Cache_ID` (`Payment_Cache_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_cache_test`
--

DROP TABLE IF EXISTS `tbl_patient_cache_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_cache_test` (
  `Patient_Cache_Test_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Submited_Time` datetime NOT NULL,
  `View_Time` datetime NOT NULL,
  `Specimen_Taken` int(10) NOT NULL,
  `Specimen_Number` int(10) NOT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  PRIMARY KEY (`Patient_Cache_Test_ID`),
  KEY `Item_List_Cache_ID` (`Payment_Item_Cache_List_ID`),
  KEY `Employee_ID` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_cache_test_specimen`
--

DROP TABLE IF EXISTS `tbl_patient_cache_test_specimen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_cache_test_specimen` (
  `Patient_Cache_Test_Specimen_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Specimen_Status` varchar(45) NOT NULL,
  `Time_Taken` datetime NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Patient_Cache_Test_ID` int(11) NOT NULL,
  `Laboratory_Test_Specimen_ID` int(11) NOT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  PRIMARY KEY (`Patient_Cache_Test_Specimen_ID`),
  KEY `Patient_Item_Cache_LIst_ID` (`Payment_Item_Cache_List_ID`),
  KEY `Patient_Item_Cache_LIst_ID_2` (`Payment_Item_Cache_List_ID`),
  KEY `Laboratory_Test_Specimen_ID` (`Laboratory_Test_Specimen_ID`),
  KEY `Patient_CacheTest_ID` (`Patient_Cache_Test_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_disease`
--

DROP TABLE IF EXISTS `tbl_patient_disease`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_disease` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `record_date` datetime NOT NULL,
  `disease_id` int(11) NOT NULL,
  `disease_form` int(11) NOT NULL,
  `diagnosis_type` int(11) NOT NULL,
  `check_in_id` int(11) NOT NULL,
  `registration_id` int(11) DEFAULT NULL,
  `consultation_id` int(11) DEFAULT NULL,
  `round_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_disease_check_in_id_foreign` (`check_in_id`),
  KEY `patient_disease_employee_id_foreign` (`employee_id`),
  KEY `patient_disease_registration_id_foreign` (`registration_id`),
  KEY `patient_disease_disease_id_foreign` (`disease_id`),
  KEY `patient_disease_consultation_id_foreign` (`consultation_id`),
  KEY `patient_disease_round_id_foreign` (`round_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_file_view`
--

DROP TABLE IF EXISTS `tbl_patient_file_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_file_view` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Registration_ID` int(11) DEFAULT NULL,
  `view_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Registration_ID` (`Registration_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_histrory`
--

DROP TABLE IF EXISTS `tbl_patient_histrory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_histrory` (
  `hist_id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_id` int(11) DEFAULT NULL,
  `famscohist` varchar(200) NOT NULL,
  `pastobshist` varchar(200) NOT NULL,
  `pastpaedhist` varchar(200) NOT NULL,
  `pastmedhist` varchar(200) NOT NULL,
  `pastdenthist` varchar(200) NOT NULL,
  `pastsurghist` varchar(200) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `histr_date` datetime NOT NULL,
  PRIMARY KEY (`hist_id`),
  KEY `employee_id` (`employee_id`),
  KEY `registration_id` (`registration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_nursecare`
--

DROP TABLE IF EXISTS `tbl_patient_nursecare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_nursecare` (
  `Nursecare_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `employee_ID` int(11) DEFAULT NULL,
  `Days` datetime NOT NULL,
  `Nurse_Care` varchar(500) NOT NULL,
  `Objective_Care` varchar(500) NOT NULL,
  `Expect_Outcome` varchar(500) NOT NULL,
  `Nursing_Intervate` varchar(500) NOT NULL,
  `Evaluation` varchar(500) NOT NULL,
  PRIMARY KEY (`Nursecare_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `employee_ID` (`employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_past_medical_history`
--

DROP TABLE IF EXISTS `tbl_patient_past_medical_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_past_medical_history` (
  `Medical_history_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `Past_Illness` text NOT NULL,
  `Hospitalization` text NOT NULL,
  `Surgical_History` text NOT NULL,
  `Current_Medication` text NOT NULL,
  `Allergies` text NOT NULL,
  `Obsetric_Gynaecology` text NOT NULL,
  `Immunization_Democology` text NOT NULL,
  `Dental` text,
  `Others` text,
  PRIMARY KEY (`Medical_history_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `Employee_ID` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_payment_item_list`
--

DROP TABLE IF EXISTS `tbl_patient_payment_item_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_payment_item_list` (
  `Patient_Payment_Item_List_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Check_In_Type` varchar(60) DEFAULT NULL,
  `Category` varchar(100) DEFAULT 'indirect cash',
  `Item_ID` int(11) DEFAULT NULL,
  `Item_Name` varchar(200) DEFAULT NULL,
  `Discount` int(11) NOT NULL DEFAULT '0',
  `Price` float NOT NULL DEFAULT '0',
  `Quantity` int(11) NOT NULL DEFAULT '0',
  `Patient_Direction` varchar(150) DEFAULT NULL,
  `Consultant` varchar(100) DEFAULT NULL,
  `Consultant_ID` int(11) DEFAULT NULL,
  `Clinic_ID` int(11) DEFAULT NULL,
  `Status` varchar(40) DEFAULT 'active',
  `Patient_Payment_ID` int(11) DEFAULT NULL,
  `Transaction_Date_And_Time` datetime NOT NULL,
  `Process_Status` varchar(40) NOT NULL DEFAULT 'not served',
  `Nursing_Status` varchar(10) NOT NULL DEFAULT 'not served',
  `Sub_Department_ID` int(11) NOT NULL,
  `Signedoff_Date_And_Time` datetime DEFAULT NULL,
  `Record_Status` varchar(50) DEFAULT NULL,
  `Employee_Edited_ID` int(11) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `ServedDateTime` datetime DEFAULT NULL,
  `ServedBy` int(11) DEFAULT NULL,
  `ItemOrigin` varchar(20) NOT NULL DEFAULT 'Reception',
  `changed_By` int(11) DEFAULT NULL,
  `changed_Reasons` tinytext,
  `changed_Date` datetime DEFAULT NULL,
  `payment_type` varchar(9) NOT NULL,
  `Payment_Status` int(11) DEFAULT NULL,
  PRIMARY KEY (`Patient_Payment_Item_List_ID`),
  KEY `Patient_Payment_ID` (`Patient_Payment_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `ServedBy` (`ServedBy`),
  KEY `Consultant_ID` (`Consultant_ID`),
  KEY `Clinic_ID` (`Clinic_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_payment_test`
--

DROP TABLE IF EXISTS `tbl_patient_payment_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_payment_test` (
  `Patient_Payment_Test_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Submited_Time` datetime DEFAULT NULL,
  `View_Time` datetime NOT NULL,
  `Specimen_Taken` int(10) NOT NULL,
  `Specimen_Number` int(10) DEFAULT NULL,
  `Patient_Payment_Item_List_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  PRIMARY KEY (`Patient_Payment_Test_ID`),
  UNIQUE KEY `Patient_Payment_Item_List_ID_2` (`Patient_Payment_Item_List_ID`),
  KEY `Patient_Payment_Item_List_ID` (`Patient_Payment_Item_List_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Patient_Payment_Item_List_ID_3` (`Patient_Payment_Item_List_ID`),
  KEY `Employee_ID_2` (`Employee_ID`),
  KEY `Patient_Payment_Item_List_ID_4` (`Patient_Payment_Item_List_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_payment_test_specimen`
--

DROP TABLE IF EXISTS `tbl_patient_payment_test_specimen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_payment_test_specimen` (
  `Patient_Payment_Test_Specimen_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Specimen_Status` varchar(45) DEFAULT NULL,
  `Time_Taken` datetime DEFAULT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Patient_Payment_Test_ID` int(11) NOT NULL,
  `Laboratory_Test_specimen_ID` int(11) NOT NULL,
  `Payment_ID` int(11) NOT NULL,
  PRIMARY KEY (`Patient_Payment_Test_Specimen_ID`),
  KEY `fk_tbl_patient_payment_test_specimen_tbl_patient_payment_te_idx` (`Patient_Payment_Test_ID`),
  KEY `fk_tbl_patient_payment_test_specimen_tbl_laboratory_test_sp_idx` (`Laboratory_Test_specimen_ID`),
  KEY `EmpoyeeID` (`Employee_ID`),
  KEY `Payment_ID` (`Payment_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Patient_Payment_Test_ID` (`Patient_Payment_Test_ID`),
  KEY `Laboratory_Test_specimen_ID` (`Laboratory_Test_specimen_ID`),
  KEY `Payment_ID_2` (`Payment_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_payments`
--

DROP TABLE IF EXISTS `tbl_patient_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_payments` (
  `Patient_Payment_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `Supervisor_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Payment_Date_And_Time` datetime DEFAULT NULL,
  `Folio_Number` int(11) DEFAULT NULL,
  `Check_In_ID` int(11) DEFAULT NULL,
  `Claim_Form_Number` varchar(50) DEFAULT NULL,
  `Sponsor_ID` int(11) DEFAULT NULL,
  `Sponsor_Name` varchar(150) DEFAULT NULL,
  `Billing_Type` varchar(50) NOT NULL,
  `Receipt_Date` date NOT NULL,
  `Transaction_status` varchar(20) NOT NULL DEFAULT 'active',
  `Transaction_type` varchar(20) NOT NULL DEFAULT 'indirect cash',
  `payment_type` enum('post','pre') NOT NULL DEFAULT 'post',
  `Fast_Track` enum('1','0') NOT NULL DEFAULT '0',
  `branch_id` int(11) DEFAULT NULL,
  `Billing_Process_Status` varchar(50) DEFAULT NULL,
  `Billing_Process_Employee_ID` int(11) DEFAULT NULL COMMENT 'Employee who approved transaction',
  `Billing_Process_Date` date DEFAULT NULL COMMENT 'Date The transaction approved',
  `Bill_ID` int(11) NOT NULL DEFAULT '0' COMMENT 'foreign key referenced from tbl_bills',
  `payment_mode` varchar(20) NOT NULL DEFAULT 'normal',
  `Patient_Bill_ID` int(11) NOT NULL DEFAULT '0',
  `Transaction_Ref` varchar(150) DEFAULT NULL,
  `Transaction_Date` datetime DEFAULT NULL,
  `Payment_Code` varchar(100) DEFAULT NULL,
  `Currency_ID` int(11) DEFAULT NULL,
  `cancelation_reasons` text,
  `Sub_Department_ID` int(10) unsigned DEFAULT NULL,
  `previous_transaction_status` int(1) DEFAULT NULL,
  `tracking_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`Patient_Payment_ID`),
  KEY `Supervisor_ID` (`Supervisor_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Sponsor_ID` (`Sponsor_ID`),
  KEY `branch_id` (`branch_id`),
  KEY `Folio_Number` (`Folio_Number`),
  KEY `Check_In_ID` (`Check_In_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_progress`
--

DROP TABLE IF EXISTS `tbl_patient_progress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_progress` (
  `Progress_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Admision_ID` int(11) DEFAULT NULL,
  `Consultation_ID` int(11) DEFAULT NULL,
  `Checked_Date_Time` datetime DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Assessment` text,
  `Nursing_Diagnosis` text,
  `Goal` text,
  `Nursing_Interverntion` text,
  `Evaluation` text,
  `Registration_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Progress_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Admision_ID` (`Admision_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Consultation_ID` (`Consultation_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_registration`
--

DROP TABLE IF EXISTS `tbl_patient_registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_registration` (
  `Registration_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Old_Registration_Number` varchar(100) DEFAULT NULL,
  `Title` varchar(120) DEFAULT NULL,
  `Patient_Name` varchar(200) NOT NULL,
  `Date_Of_Birth` date NOT NULL,
  `Gender` varchar(7) NOT NULL,
  `Country` varchar(50) NOT NULL,
  `Region` varchar(70) NOT NULL,
  `District` varchar(40) NOT NULL,
  `Ward` varchar(50) NOT NULL,
  `Sponsor_ID` int(11) DEFAULT NULL,
  `Member_Number` varchar(100) DEFAULT NULL,
  `Member_Card_Expire_Date` date DEFAULT NULL,
  `Phone_Number` varchar(30) DEFAULT NULL,
  `Email_Address` varchar(60) DEFAULT NULL,
  `Occupation` varchar(100) DEFAULT NULL,
  `Employee_Vote_Number` varchar(150) DEFAULT NULL,
  `Emergence_Contact_Name` varchar(150) DEFAULT NULL,
  `Emergence_Contact_Number` varchar(30) DEFAULT NULL,
  `Company` varchar(150) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Registration_Date_And_Time` datetime NOT NULL,
  `Patient_Picture` varchar(15) NOT NULL DEFAULT 'default.png',
  `District_ID` int(11) DEFAULT NULL,
  `Registration_Date` date DEFAULT NULL,
  `Status` varchar(200) NOT NULL,
  `Tribe` varchar(30) NOT NULL,
  `Diseased` varchar(10) NOT NULL DEFAULT 'no',
  `Country_ID` int(11) NOT NULL,
  `Region_ID` int(11) NOT NULL,
  `Name_Prefix_ID` int(11) DEFAULT NULL,
  `Emergency_Contact_Name` varchar(150) DEFAULT NULL,
  `Emergency_Contact_Number` varchar(30) DEFAULT NULL,
  `Patient_Profile_Picture` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Registration_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Sponsor_ID` (`Sponsor_ID`),
  KEY `Region_ID` (`Region`),
  KEY `District_ID` (`District_ID`),
  KEY `Member_Number` (`Member_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_transfer`
--

DROP TABLE IF EXISTS `tbl_patient_transfer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_in_id` int(11) NOT NULL,
  `from_consultant_id` int(11) NOT NULL,
  `to_consultant_id` int(11) NOT NULL,
  `transfer_date` datetime NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_transfer_check_in_id_foreign` (`check_in_id`),
  KEY `patient_transfer_from_consultant_id_foreign` (`from_consultant_id`),
  KEY `patient_transfer_to_consultant_id_foreign` (`to_consultant_id`),
  KEY `patient_transfer_employee_id_foreign` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_vital`
--

DROP TABLE IF EXISTS `tbl_patient_vital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_vital` (
  `patient_vital_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `registration_id` int(11) NOT NULL,
  `taken_date` datetime NOT NULL,
  PRIMARY KEY (`patient_vital_id`),
  KEY `patient_vital_employee_id_foreign` (`employee_id`),
  KEY `patient_vital_registration_id_foreign` (`registration_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_patient_vital_value`
--

DROP TABLE IF EXISTS `tbl_patient_vital_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_patient_vital_value` (
  `patient_vital_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_vital_id` int(11) NOT NULL,
  `vital_id` int(11) NOT NULL,
  `vital_value` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`patient_vital_value_id`),
  KEY `patient_vital_value_patient_vital_id_foreign` (`patient_vital_id`),
  KEY `patient_vital_value_vital_id_foreign` (`vital_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_payment_cache`
--

DROP TABLE IF EXISTS `tbl_payment_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_payment_cache` (
  `Payment_Cache_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `consultation_id` int(11) DEFAULT NULL,
  `Round_ID` int(11) DEFAULT NULL,
  `Payment_Date_And_Time` datetime DEFAULT NULL,
  `Folio_Number` int(11) DEFAULT NULL,
  `Sponsor_ID` int(11) DEFAULT NULL,
  `Sponsor_Name` varchar(150) DEFAULT NULL,
  `Billing_Type` varchar(50) NOT NULL,
  `Receipt_Date` date NOT NULL,
  `Transaction_status` varchar(20) NOT NULL DEFAULT 'active',
  `Transaction_type` varchar(20) NOT NULL DEFAULT 'indirect cash',
  `Fast_Track` enum('1','0') NOT NULL DEFAULT '0',
  `Order_Type` enum('normal','post operative') NOT NULL DEFAULT 'normal',
  `branch_id` int(11) DEFAULT NULL,
  `Check_In_ID` int(11) DEFAULT NULL,
  `Consultant_ID` int(11) DEFAULT NULL,
  `Credit_Bill_Patient_Payment_ID` int(11) DEFAULT NULL,
  `Cash_Bill_Patient_Payment_ID` int(11) DEFAULT NULL,
  `Currency_ID` varchar(11) DEFAULT NULL,
  `tracking_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`Payment_Cache_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Sponsor_ID` (`Sponsor_ID`),
  KEY `branch_id` (`branch_id`),
  KEY `consultation_id` (`consultation_id`),
  KEY `Round_ID` (`Round_ID`),
  KEY `Consultant_ID` (`Consultant_ID`),
  KEY `Check_In_ID` (`Check_In_ID`),
  KEY `Credit_Bill_Patient_Payment_ID` (`Credit_Bill_Patient_Payment_ID`),
  KEY `Cash_Bill_Patient_Payment_ID` (`Cash_Bill_Patient_Payment_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_payment_item_list_cache`
--

DROP TABLE IF EXISTS `tbl_payment_item_list_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_payment_item_list_cache` (
  `payment_item_list_cache_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Employee_ID` int(11) NOT NULL,
  `Registration_ID` int(11) NOT NULL,
  `item_ID` int(11) NOT NULL,
  `Check_In_Type` varchar(20) NOT NULL,
  `Patient_Direction` varchar(20) NOT NULL,
  `Consultant_ID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Discount` int(11) DEFAULT NULL,
  `bill_type` varchar(20) NOT NULL,
  `Sub_Department_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`payment_item_list_cache_ID`),
  KEY `	Sub_Department_ID` (`Sub_Department_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_pending_purchase_order`
--

DROP TABLE IF EXISTS `tbl_pending_purchase_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pending_purchase_order` (
  `Pending_Purchase_Order_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Purchase_Order_ID` int(11) NOT NULL,
  `Order_Description` varchar(200) DEFAULT NULL,
  `Created_Date` datetime NOT NULL,
  `Sub_Department_ID` int(11) DEFAULT NULL,
  `Supplier_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Order_Status` varchar(30) DEFAULT 'pending',
  `Grn_Purchase_Order_ID` int(11) NOT NULL,
  PRIMARY KEY (`Pending_Purchase_Order_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_pending_purchase_order_items`
--

DROP TABLE IF EXISTS `tbl_pending_purchase_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pending_purchase_order_items` (
  `Order_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Purchase_Order_ID` int(11) NOT NULL,
  `Quantity_Required` int(11) NOT NULL,
  `Containers_Required` int(11) NOT NULL DEFAULT '0',
  `Items_Per_Container_Required` int(11) NOT NULL DEFAULT '0',
  `Price` double DEFAULT NULL,
  `Remark` varchar(120) DEFAULT NULL,
  `Item_ID` int(11) DEFAULT NULL,
  `Quantity_Supplied` int(11) DEFAULT NULL,
  `Quantity_Received` int(11) DEFAULT NULL,
  `Containers_Received` int(11) NOT NULL DEFAULT '0',
  `Items_Per_Container` int(11) NOT NULL DEFAULT '0',
  `Buying_Price` decimal(63,2) DEFAULT NULL,
  `Grn_Purchase_Order_ID` int(11) DEFAULT NULL,
  `Item_Status` varchar(30) NOT NULL DEFAULT 'active',
  `Expire_Date` date DEFAULT NULL,
  `Grn_Status` varchar(20) DEFAULT NULL,
  `Pending_Purchase_Order_ID` int(11) NOT NULL,
  `was_pending` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Order_Item_ID`),
  KEY `order_id` (`Purchase_Order_ID`,`Item_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `Pending_Purchase_Order_ID` (`Pending_Purchase_Order_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_permission`
--

DROP TABLE IF EXISTS `tbl_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `only_for_administrator` tinyint(1) NOT NULL DEFAULT '0',
  `permission_group_id` int(10) unsigned NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `permission_permission_group_id_foreign` (`permission_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1725 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_permission_block`
--

DROP TABLE IF EXISTS `tbl_permission_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_permission_block` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `only_for_administrator` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_permission_group`
--

DROP TABLE IF EXISTS `tbl_permission_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_permission_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `only_for_administrator` tinyint(1) NOT NULL DEFAULT '0',
  `permission_block_id` int(10) unsigned NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `permission_group_permission_block_id_foreign` (`permission_block_id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_pf3_patients`
--

DROP TABLE IF EXISTS `tbl_pf3_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pf3_patients` (
  `pf3_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Check_In_ID` int(11) NOT NULL,
  `pf3_attachment_file` varchar(100) NOT NULL,
  `pf3_Date_And_Time` datetime NOT NULL,
  `pf3_Description` varchar(500) NOT NULL,
  `Police_Station` varchar(50) NOT NULL,
  `Pf3_Number` varchar(50) NOT NULL,
  `Relative` varchar(50) NOT NULL,
  `Phone_No_Relative` int(20) NOT NULL,
  `Reason_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`pf3_ID`),
  KEY `Reason_ID` (`Reason_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_pf3_reasons`
--

DROP TABLE IF EXISTS `tbl_pf3_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pf3_reasons` (
  `Reason_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Reason_Name` varchar(100) NOT NULL,
  PRIMARY KEY (`Reason_ID`),
  UNIQUE KEY `Reason_Name` (`Reason_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_post_operative`
--

DROP TABLE IF EXISTS `tbl_post_operative`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_post_operative` (
  `Post_operative_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Procedure_Names` int(11) NOT NULL COMMENT 'foreign key from item_ID',
  `date_From` datetime NOT NULL,
  `Indication` varchar(200) NOT NULL,
  `Type_Of_Incision` varchar(200) NOT NULL,
  `Anaesthesia` varchar(200) NOT NULL,
  `Findings` varchar(500) NOT NULL,
  `Procedures` varchar(500) NOT NULL,
  `Closure` varchar(500) NOT NULL,
  `Drains` varchar(500) NOT NULL,
  `Post_Operative_Date_Time` datetime NOT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Registration_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Skin_Structure` varchar(500) NOT NULL,
  PRIMARY KEY (`Post_operative_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Procedure_Names` (`Procedure_Names`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_post_operative_attachment`
--

DROP TABLE IF EXISTS `tbl_post_operative_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_post_operative_attachment` (
  `Post_operative_ID` int(11) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  KEY `Post_operative_ID` (`Post_operative_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_post_operative_diagnosis`
--

DROP TABLE IF EXISTS `tbl_post_operative_diagnosis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_post_operative_diagnosis` (
  `Diagnosis_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Disease_ID` int(11) DEFAULT NULL,
  `Diagnosis_Type` varchar(50) DEFAULT NULL,
  `Post_operative_ID` int(11) DEFAULT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  PRIMARY KEY (`Diagnosis_ID`),
  KEY `Post_operative_ID` (`Post_operative_ID`),
  KEY `Disease_ID` (`Disease_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_post_operative_external_participant`
--

DROP TABLE IF EXISTS `tbl_post_operative_external_participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_post_operative_external_participant` (
  `External_Participant_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Post_operative_ID` int(11) DEFAULT NULL,
  `External_Surgeons` text,
  `External_Assistant_Surgeon` text,
  `External_Scrub_Nurse` text,
  `External_Anaesthetic` text,
  PRIMARY KEY (`External_Participant_ID`),
  KEY `Post_operative_ID` (`Post_operative_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_post_operative_notes`
--

DROP TABLE IF EXISTS `tbl_post_operative_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_post_operative_notes` (
  `Post_operative_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL COMMENT 'foreign key from tbl_item_list_cache',
  `Surgery_Date` date NOT NULL,
  `Surgery_Date_Time` datetime DEFAULT NULL,
  `Incision` varchar(100) NOT NULL,
  `Position` varchar(100) NOT NULL,
  `Type_Of_Anesthetic` varchar(30) DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Registration_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL COMMENT 'Doctor ID',
  `Procedure_Description` text,
  `Identification_Of_Prosthesis` text,
  `Estimated_Blood_loss` text,
  `Complications` text,
  `Drains` text,
  `Specimen_sent` text,
  `Postoperative_Orders` text,
  `Recommendations` text,
  `Post_Operative_Status` varchar(20) NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`Post_operative_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `Payment_Item_Cache_List_ID` (`Payment_Item_Cache_List_ID`),
  KEY `Registration_ID` (`Registration_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_post_operative_participant`
--

DROP TABLE IF EXISTS `tbl_post_operative_participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_post_operative_participant` (
  `Participant_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Post_operative_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Employee_Type` varchar(50) DEFAULT NULL,
  `Payment_Item_Cache_List_ID` int(11) NOT NULL,
  PRIMARY KEY (`Participant_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Post_operative_ID` (`Post_operative_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_pre_operative_checklist`
--

DROP TABLE IF EXISTS `tbl_pre_operative_checklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pre_operative_checklist` (
  `Pre_Operative_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Employee_ID` int(11) NOT NULL,
  `Registration_ID` int(11) NOT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Theatre_Time` time NOT NULL,
  `Ward_Signature` varchar(100) NOT NULL,
  `Theatre_Signature` varchar(100) NOT NULL,
  `Special_Information` varchar(100) NOT NULL,
  `Operative_Date_Time` datetime NOT NULL,
  PRIMARY KEY (`Pre_Operative_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `Registration_ID_2` (`Registration_ID`),
  KEY `Employee_ID_2` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_pre_operative_checklist_items`
--

DROP TABLE IF EXISTS `tbl_pre_operative_checklist_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pre_operative_checklist_items` (
  `Task_Name` varchar(50) NOT NULL,
  `Task_Value` varchar(50) NOT NULL,
  `Remark` varchar(100) NOT NULL,
  `Pre_Operative_ID` int(11) NOT NULL,
  KEY `Pre-Operative_ID` (`Pre_Operative_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_privileges`
--

DROP TABLE IF EXISTS `tbl_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_privileges` (
  `Given_Username` varchar(150) NOT NULL,
  `Given_Password` varchar(150) NOT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `Last_Password_Change` date NOT NULL,
  `Changed_first_login` varchar(10) NOT NULL DEFAULT 'no',
  `session_id` text,
  `last_logged_in_computer` text,
  `last_login_date` datetime DEFAULT NULL,
  UNIQUE KEY `Given_Username` (`Given_Username`),
  UNIQUE KEY `Employee_ID` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_procedure_post_operative_notes`
--

DROP TABLE IF EXISTS `tbl_procedure_post_operative_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_procedure_post_operative_notes` (
  `Post_operative_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Procedure_Date` datetime DEFAULT NULL,
  `Procedure_Date_Time` datetime DEFAULT NULL,
  `Incision` varchar(50) DEFAULT NULL,
  `Position` varchar(50) DEFAULT NULL,
  `Type_Of_Anesthetic` int(11) DEFAULT NULL,
  `Employee_ID` int(11) NOT NULL,
  `Procedure_Description` text,
  `Identification_Of_Prosthesis` varchar(50) DEFAULT NULL,
  `Estimated_Blood_loss` varchar(50) DEFAULT NULL,
  `Complications` varchar(50) DEFAULT NULL,
  `Drains` varchar(50) DEFAULT NULL,
  `Specimen_sent` text,
  `Postoperative_Orders` varchar(50) DEFAULT NULL,
  `Recommendations` text,
  `Post_Operative_Status` int(11) DEFAULT NULL,
  `Payment_Item_Cache_List_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Post_operative_ID`),
  KEY `Employee_ID` (`Employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_purchase_cache`
--

DROP TABLE IF EXISTS `tbl_purchase_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_purchase_cache` (
  `Purchase_Cache_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Quantity_Required` int(11) NOT NULL DEFAULT '0',
  `Item_Remark` varchar(200) DEFAULT NULL,
  `Item_ID` int(11) DEFAULT NULL,
  `Container_Qty` int(11) NOT NULL DEFAULT '0',
  `Items_Per_Container` int(11) NOT NULL DEFAULT '0',
  `Price` double NOT NULL DEFAULT '0',
  `Store_Need` int(11) NOT NULL,
  `Requisition_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `is_store_order` varchar(5) NOT NULL DEFAULT 'no',
  `Store_Order_Id` int(11) DEFAULT NULL,
  `Supplier_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Purchase_Cache_ID`),
  KEY `item_id` (`Item_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Store_Need` (`Store_Need`),
  KEY `Supplier_ID` (`Supplier_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_purchase_order`
--

DROP TABLE IF EXISTS `tbl_purchase_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_purchase_order` (
  `Purchase_Order_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Order_Description` varchar(200) DEFAULT NULL,
  `Created_Date` datetime NOT NULL,
  `Sent_Date` datetime DEFAULT NULL,
  `Sub_Department_ID` int(11) DEFAULT NULL,
  `Supplier_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Order_Status` varchar(30) DEFAULT 'pending',
  `Branch_ID` int(11) DEFAULT NULL,
  `Grn_Purchase_Order_ID` int(11) NOT NULL,
  `Approval_Level` int(11) NOT NULL DEFAULT '1',
  `Approvals` int(11) NOT NULL DEFAULT '2' COMMENT 'Number of approval steps',
  `Store_Order_ID` int(11) DEFAULT NULL,
  `Previous_Purchase_Order_Data` text,
  `Purchase_Order_Type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Purchase_Order_ID`),
  KEY `store_need` (`Sub_Department_ID`),
  KEY `store_need_2` (`Sub_Department_ID`),
  KEY `supplier_id` (`Supplier_ID`),
  KEY `Store_Need_3` (`Sub_Department_ID`),
  KEY `Sub_Department_ID` (`Sub_Department_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Branch_ID` (`Branch_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_purchase_order_approval_process`
--

DROP TABLE IF EXISTS `tbl_purchase_order_approval_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_purchase_order_approval_process` (
  `Purchase_Approval_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Purchase_Order_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Approval_ID` int(11) DEFAULT NULL,
  `Approval_Date_Time` datetime NOT NULL,
  `Approval_Title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Purchase_Approval_ID`),
  KEY `Purchase_Order_ID` (`Purchase_Order_ID`),
  KEY `Employee_ID` (`Employee_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_purchase_order_items`
--

DROP TABLE IF EXISTS `tbl_purchase_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_purchase_order_items` (
  `Order_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Purchase_Order_ID` int(11) NOT NULL,
  `Quantity_Required` int(11) NOT NULL,
  `Containers_Required` int(11) NOT NULL DEFAULT '0',
  `Items_Per_Container_Required` int(11) NOT NULL DEFAULT '0',
  `Price` double DEFAULT NULL,
  `Remark` varchar(120) DEFAULT NULL,
  `Item_ID` int(11) DEFAULT NULL,
  `Quantity_Supplied` int(11) DEFAULT NULL,
  `Quantity_Received` int(11) DEFAULT NULL,
  `Containers_Received` int(11) NOT NULL DEFAULT '0',
  `Items_Per_Container` int(11) NOT NULL DEFAULT '0',
  `Buying_Price` double DEFAULT NULL,
  `Grn_Purchase_Order_ID` int(11) DEFAULT NULL,
  `Item_Status` varchar(30) NOT NULL DEFAULT 'active',
  `Expire_Date` date DEFAULT NULL,
  `Grn_Status` varchar(20) DEFAULT NULL,
  `was_pending` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Order_Item_ID`),
  KEY `order_id` (`Purchase_Order_ID`,`Item_ID`),
  KEY `Item_ID` (`Item_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_radiology_discription`
--

DROP TABLE IF EXISTS `tbl_radiology_discription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_radiology_discription` (
  `Radiology_Description_ID` int(30) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  `Parameter_ID` int(11) DEFAULT NULL,
  `Patient_Payment_Item_List_ID` int(11) NOT NULL,
  `comments` text NOT NULL,
  `Radiology_Date` date NOT NULL,
  `Status` varchar(15) NOT NULL,
  PRIMARY KEY (`Radiology_Description_ID`),
  KEY `Parameter_ID` (`Parameter_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_radiology_discription_old`
--

DROP TABLE IF EXISTS `tbl_radiology_discription_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_radiology_discription_old` (
  `Radiology_Description_ID` int(30) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Parameter_ID` int(11) DEFAULT NULL,
  `Patient_Payment_Item_List_ID` int(11) NOT NULL,
  `comments` varchar(250) NOT NULL,
  `Radiology_Date` date NOT NULL,
  PRIMARY KEY (`Radiology_Description_ID`),
  KEY `Parameter_ID` (`Parameter_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_radiology_image`
--

DROP TABLE IF EXISTS `tbl_radiology_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_radiology_image` (
  `Pic_ID` int(30) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  `Radiology_Image` varchar(255) NOT NULL,
  `Patient_Payment_Item_List_ID` int(11) NOT NULL,
  `Upload_Date` datetime NOT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Pic_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_radiology_image_old`
--

DROP TABLE IF EXISTS `tbl_radiology_image_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_radiology_image_old` (
  `Pic_ID` int(30) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Radiology_Image` varchar(255) NOT NULL,
  `Patient_Payment_Item_List_ID` int(11) NOT NULL,
  `Upload_Date` datetime NOT NULL,
  PRIMARY KEY (`Pic_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_radiology_parameter`
--

DROP TABLE IF EXISTS `tbl_radiology_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_radiology_parameter` (
  `Parameter_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Parameter_Name` varchar(30) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  PRIMARY KEY (`Parameter_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=372 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_radiology_parameter_cache`
--

DROP TABLE IF EXISTS `tbl_radiology_parameter_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_radiology_parameter_cache` (
  `Parameter_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Parameter_Name` varchar(30) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  PRIMARY KEY (`Parameter_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_radiology_parameter_old`
--

DROP TABLE IF EXISTS `tbl_radiology_parameter_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_radiology_parameter_old` (
  `Parameter_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Parameter_Name` varchar(30) NOT NULL,
  PRIMARY KEY (`Parameter_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_radiology_patient_tests`
--

DROP TABLE IF EXISTS `tbl_radiology_patient_tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_radiology_patient_tests` (
  `Radiology_Test_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_ID` int(11) NOT NULL COMMENT 'Radiology Item ID',
  `Patient_Payment_Item_List_ID` int(11) NOT NULL,
  `Registration_ID` int(11) NOT NULL,
  `Status` varchar(50) NOT NULL,
  `Results_Status` varchar(20) NOT NULL,
  `Remarks` varchar(50) NOT NULL,
  `Classification` varchar(50) NOT NULL,
  `Sonographer_ID` int(11) DEFAULT NULL,
  `Radiologist_ID` int(11) DEFAULT NULL,
  `Reporteur` int(11) DEFAULT NULL,
  `Date_Time` datetime NOT NULL,
  `PatientOrigin` varchar(50) NOT NULL DEFAULT 'Doctor',
  PRIMARY KEY (`Radiology_Test_ID`),
  KEY `Reporteur` (`Reporteur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_referral_hosp`
--

DROP TABLE IF EXISTS `tbl_referral_hosp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_referral_hosp` (
  `hosp_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ref_hosp_name` varchar(150) NOT NULL,
  PRIMARY KEY (`hosp_ID`),
  UNIQUE KEY `Vital` (`ref_hosp_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_referral_patient`
--

DROP TABLE IF EXISTS `tbl_referral_patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_referral_patient` (
  `referr_id` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `Patient_Payment_Item_List_ID` int(11) DEFAULT NULL,
  `admission_ID` int(11) DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `referral_from` int(11) DEFAULT NULL,
  `referral_to` int(11) DEFAULT NULL,
  `transfer_date` date DEFAULT NULL,
  `diagnosis` text,
  `temp` varchar(50) DEFAULT NULL,
  `heatrate` varchar(50) DEFAULT NULL,
  `resprate` varchar(50) DEFAULT NULL,
  `bloodpressure` varchar(50) DEFAULT NULL,
  `mental_status` varchar(50) DEFAULT NULL,
  `alert` varchar(50) DEFAULT NULL,
  `patienthist` text,
  `chrnicmed` text,
  `medalergy` text,
  `pertnetfindings` text,
  `labresult` text,
  `radresult` text,
  `treatmentrendered` text,
  `reasonfortransfer` text,
  `doct_phone_number` varchar(50) DEFAULT NULL,
  `call_phone_number` varchar(50) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `trans_date` datetime DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'active' COMMENT 'active,served,removed',
  `proccessed_by` int(11) DEFAULT NULL,
  `date_processed` datetime DEFAULT NULL,
  PRIMARY KEY (`referr_id`),
  KEY `Registration_ID` (`Registration_ID`,`Patient_Payment_Item_List_ID`),
  KEY `employee_id` (`employee_id`,`proccessed_by`),
  KEY `Patient_Payment_Item_List_ID` (`Patient_Payment_Item_List_ID`),
  KEY `proccessed_by` (`proccessed_by`),
  KEY `referral_to` (`referral_to`),
  KEY `referral_from` (`referral_from`),
  KEY `admission_ID` (`admission_ID`,`consultation_ID`),
  KEY `consultation_ID` (`consultation_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_regions`
--

DROP TABLE IF EXISTS `tbl_regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_regions` (
  `Region_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Region_Name` varchar(100) NOT NULL,
  `Country_ID` int(11) NOT NULL,
  `Region_Status` varchar(20) NOT NULL DEFAULT 'Not Selected' COMMENT 'Control initial region to display during registration process',
  PRIMARY KEY (`Region_ID`),
  UNIQUE KEY `Region_Name` (`Region_Name`),
  KEY `Country_ID` (`Country_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_removed_specimen_patients`
--

DROP TABLE IF EXISTS `tbl_removed_specimen_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_removed_specimen_patients` (
  `removed_Specimen_Patients_Id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `Payment_id` int(11) NOT NULL,
  `time_removed` datetime NOT NULL,
  PRIMARY KEY (`removed_Specimen_Patients_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_requisition`
--

DROP TABLE IF EXISTS `tbl_requisition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_requisition` (
  `Requisition_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Requisition_Description` varchar(150) DEFAULT NULL,
  `Created_Date_Time` datetime DEFAULT NULL,
  `Created_Date` date NOT NULL,
  `Sent_Date_Time` datetime DEFAULT NULL COMMENT 'Submitted date',
  `Store_Need` int(11) DEFAULT NULL COMMENT 'foreign key (sub_department_id)',
  `Store_Issue` int(11) DEFAULT NULL COMMENT 'foreign key (sub_department_id)',
  `Employee_ID` int(11) DEFAULT NULL,
  `Supervisor_ID` int(11) DEFAULT NULL,
  `Approval_Date_Time` datetime DEFAULT NULL,
  `Supervisor_Comment` varchar(500) DEFAULT NULL,
  `Requisition_Status` varchar(40) DEFAULT 'pending',
  `Branch_ID` int(11) NOT NULL,
  `Previous_Requisition_Data` text,
  PRIMARY KEY (`Requisition_ID`),
  KEY `Store_Need` (`Store_Need`),
  KEY `Store_Issue` (`Store_Issue`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Branch_ID` (`Branch_ID`),
  KEY `Supervisor_ID` (`Supervisor_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_requisition_items`
--

DROP TABLE IF EXISTS `tbl_requisition_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_requisition_items` (
  `Requisition_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Requisition_ID` int(11) DEFAULT NULL,
  `Quantity_Required` int(11) NOT NULL,
  `Container_Qty` int(11) NOT NULL DEFAULT '0',
  `Items_Qty` int(11) NOT NULL DEFAULT '0',
  `Items_Per_Container` int(11) NOT NULL DEFAULT '0' COMMENT 'Items per containers issued',
  `Item_Remark` varchar(60) DEFAULT NULL,
  `Item_ID` int(11) DEFAULT NULL,
  `Quantity_Issued` int(11) NOT NULL DEFAULT '0',
  `Container_Issued` int(11) NOT NULL DEFAULT '0',
  `Quantity_Received` int(11) NOT NULL DEFAULT '0',
  `Last_Buying_Price` double NOT NULL DEFAULT '0',
  `Issue_ID` int(11) DEFAULT NULL,
  `Item_Status` varchar(20) NOT NULL DEFAULT 'active',
  `Status` varchar(10) NOT NULL DEFAULT 'active',
  `Procurement_Status` varchar(20) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`Requisition_Item_ID`),
  KEY `Requizition_ID` (`Requisition_ID`),
  KEY `item_id` (`Item_ID`),
  KEY `Issue_ID` (`Issue_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_return_inward`
--

DROP TABLE IF EXISTS `tbl_return_inward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_return_inward` (
  `Inward_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Inward_Date` date DEFAULT NULL,
  `Store_Sub_Department_ID` int(11) NOT NULL,
  `Return_Sub_Department_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Inward_Status` varchar(20) NOT NULL DEFAULT 'pending',
  `Created_Date_And_Time` datetime DEFAULT NULL,
  `Previous_Return_Inward_Data` text,
  PRIMARY KEY (`Inward_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Supplier_ID` (`Return_Sub_Department_ID`),
  KEY `Sub_Department_ID` (`Store_Sub_Department_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_return_inward_items`
--

DROP TABLE IF EXISTS `tbl_return_inward_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_return_inward_items` (
  `Inward_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Inward_ID` int(11) DEFAULT NULL,
  `Item_ID` int(11) DEFAULT NULL,
  `Quantity_Returned` int(11) DEFAULT NULL,
  `Item_Remark` varchar(250) DEFAULT NULL,
  `Buying_Price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`Inward_Item_ID`),
  KEY `Outward_ID` (`Inward_ID`),
  KEY `Item_ID` (`Item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_return_outward`
--

DROP TABLE IF EXISTS `tbl_return_outward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_return_outward` (
  `Outward_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Outward_Date` date DEFAULT NULL,
  `Sub_Department_ID` int(11) NOT NULL,
  `Supplier_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Outward_Status` varchar(20) NOT NULL DEFAULT 'pending',
  `Created_Date_And_Time` datetime DEFAULT NULL,
  `Previous_Return_Outward_Data` text,
  PRIMARY KEY (`Outward_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Supplier_ID` (`Supplier_ID`),
  KEY `Sub_Department_ID` (`Sub_Department_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_return_outward_items`
--

DROP TABLE IF EXISTS `tbl_return_outward_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_return_outward_items` (
  `Outward_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Outward_ID` int(11) DEFAULT NULL,
  `Item_ID` int(11) DEFAULT NULL,
  `Quantity_Returned` int(11) DEFAULT NULL,
  `Item_Remark` varchar(250) DEFAULT NULL,
  `Buying_Price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`Outward_Item_ID`),
  KEY `Outward_ID` (`Outward_ID`),
  KEY `Item_ID` (`Item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_scheduler`
--

DROP TABLE IF EXISTS `tbl_scheduler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_scheduler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_specimen_results`
--

DROP TABLE IF EXISTS `tbl_specimen_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_specimen_results` (
  `result_ID` int(11) NOT NULL AUTO_INCREMENT,
  `payment_item_ID` int(11) NOT NULL,
  `Specimen_ID` int(11) NOT NULL,
  `collection_Status` varchar(30) NOT NULL,
  `specimen_results_Employee_ID` int(11) NOT NULL,
  `TimeCollected` datetime NOT NULL,
  `BarCode` varchar(50) NOT NULL,
  `rejected_reason` varchar(100) NOT NULL,
  `rejected_by` int(11) DEFAULT NULL,
  `Date_Rejected` datetime NOT NULL,
  `Rejection_Status` varchar(20) NOT NULL,
  `remark` text,
  PRIMARY KEY (`result_ID`),
  KEY `payment_item_ID` (`payment_item_ID`,`Specimen_ID`,`specimen_results_Employee_ID`),
  KEY `specimen_results_Employee_ID` (`specimen_results_Employee_ID`),
  KEY `collection_Status` (`collection_Status`),
  KEY `Specimen_ID` (`Specimen_ID`),
  KEY `collection_Status_2` (`collection_Status`),
  KEY `payment_item_ID_2` (`payment_item_ID`),
  KEY `specimen_results_Employee_ID_2` (`specimen_results_Employee_ID`),
  KEY `collection_Status_3` (`collection_Status`),
  KEY `rejected_by` (`rejected_by`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_spectacles`
--

DROP TABLE IF EXISTS `tbl_spectacles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_spectacles` (
  `Spectacle_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) DEFAULT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Date_Time` datetime DEFAULT NULL,
  `Patient_Type` varchar(20) NOT NULL DEFAULT 'Outpatient',
  `Spectacle_Status` varchar(20) NOT NULL DEFAULT 'pending',
  `Employee_ID` int(11) DEFAULT NULL,
  `Round_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Spectacle_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Round_ID` (`Round_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `Employee_ID` (`Employee_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sponsor`
--

DROP TABLE IF EXISTS `tbl_sponsor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sponsor` (
  `Sponsor_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Guarantor_Name` varchar(150) NOT NULL,
  `Postal_Address` varchar(100) DEFAULT NULL,
  `Region` varchar(100) DEFAULT NULL,
  `District` varchar(100) DEFAULT NULL,
  `Ward` varchar(100) DEFAULT NULL,
  `Phone_Number` varchar(40) DEFAULT NULL,
  `Membership_Id_Number_Status` varchar(50) DEFAULT NULL,
  `Claim_Number_Status` varchar(50) DEFAULT NULL,
  `Folio_Number_Status` varchar(50) DEFAULT NULL,
  `Require_Document_To_Sign_At_receiption` varchar(50) DEFAULT NULL,
  `Benefit_Limit` int(11) DEFAULT NULL,
  `Support_Patient_From_Outside` varchar(10) NOT NULL DEFAULT 'no',
  `HasAccountacy` enum('0','1') DEFAULT '0',
  `consult_per_day` int(11) DEFAULT NULL,
  `enab_auto_billing` varchar(10) NOT NULL DEFAULT 'no',
  `Exemption` varchar(5) NOT NULL DEFAULT 'no',
  `Billing_Type_Mode` varchar(15) NOT NULL,
  `Verification_System` varchar(100) DEFAULT NULL,
  `Employee_Vote_Number` varchar(15) DEFAULT 'Not Mandatory',
  `Currency_ID` int(11) DEFAULT NULL,
  `Status` varchar(10) DEFAULT 'ACTIVE',
  `benefit_package_code` varchar(100) DEFAULT NULL,
  `scheme_code` varchar(100) DEFAULT NULL,
  `parent_sponsor_id` int(11) DEFAULT NULL,
  `Claim_System` varchar(100) DEFAULT NULL,
  `invoice_allowed_due_days` varchar(3) DEFAULT '30',
  `invoice_notification_emails` text,
  `sponsor_scheme` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`Sponsor_ID`),
  UNIQUE KEY `Guarantor_Name` (`Guarantor_Name`),
  KEY `sponsor_parent_sponsor_id_foreign` (`parent_sponsor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sponsor_allow_zero_items`
--

DROP TABLE IF EXISTS `tbl_sponsor_allow_zero_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sponsor_allow_zero_items` (
  `sponsor_id` int(11) NOT NULL DEFAULT '0',
  `item_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sponsor_id`,`item_ID`),
  UNIQUE KEY `sponsor_id` (`sponsor_id`,`item_ID`),
  KEY `item_ID` (`item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sponsor_non_supported_items`
--

DROP TABLE IF EXISTS `tbl_sponsor_non_supported_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sponsor_non_supported_items` (
  `sponsor_id` int(11) NOT NULL DEFAULT '0',
  `item_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sponsor_id`,`item_ID`),
  UNIQUE KEY `sponsor_id` (`sponsor_id`,`item_ID`),
  KEY `item_ID` (`item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sponsor_pre_approval_items`
--

DROP TABLE IF EXISTS `tbl_sponsor_pre_approval_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sponsor_pre_approval_items` (
  `sponsor_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  KEY `sponsor_pre_approval_items_item_id_foreign` (`sponsor_id`),
  KEY `sponsor_pre_approval_items_sponsor_id_foreign` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sponsor_verification_details`
--

DROP TABLE IF EXISTS `tbl_sponsor_verification_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sponsor_verification_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `Sponsor_ID` int(11) NOT NULL,
  `auth_date` datetime NOT NULL,
  `auth_status` varchar(100) NOT NULL,
  `auth_number` varchar(100) NOT NULL,
  `auth_remark` text,
  `member_card_no` varchar(100) DEFAULT NULL,
  `auth_visit_type` varchar(100) DEFAULT NULL,
  `auth_referral_no` varchar(100) DEFAULT NULL,
  `Check_In_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Sponsor_ID` (`Sponsor_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_stock_balance_sub_departments`
--

DROP TABLE IF EXISTS `tbl_stock_balance_sub_departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stock_balance_sub_departments` (
  `Stock_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sub_Department_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Stock_ID`),
  KEY `Sub_Department_ID` (`Sub_Department_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_stock_ledger_controler`
--

DROP TABLE IF EXISTS `tbl_stock_ledger_controler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stock_ledger_controler` (
  `Controler_ID` int(50) NOT NULL AUTO_INCREMENT,
  `Item_ID` int(11) DEFAULT NULL,
  `Sub_Department_ID` int(11) DEFAULT NULL,
  `Movement_Type` varchar(50) NOT NULL,
  `Internal_Destination` int(11) DEFAULT NULL COMMENT 'Sub department that requested',
  `External_Source` int(11) DEFAULT NULL COMMENT 'Item Supplier',
  `Registration_ID` int(11) DEFAULT NULL,
  `Pre_Balance` int(11) DEFAULT NULL,
  `Post_Balance` int(11) NOT NULL,
  `Movement_Date` date NOT NULL,
  `Document_Number` int(11) DEFAULT NULL,
  `Movement_Date_Time` datetime NOT NULL,
  `Buying_Price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`Controler_ID`),
  KEY `Sub_Department_ID` (`Sub_Department_ID`),
  KEY `Internal_Destination` (`Internal_Destination`),
  KEY `Item_ID` (`Item_ID`),
  KEY `External_Source` (`External_Source`),
  KEY `Registration_ID` (`Registration_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_stocktaking`
--

DROP TABLE IF EXISTS `tbl_stocktaking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stocktaking` (
  `Stock_Taking_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Stock_Taking_Status` varchar(30) NOT NULL DEFAULT 'pending',
  `Stock_Taking_Date` date NOT NULL,
  `Stock_Taking_Description` varchar(200) DEFAULT NULL,
  `Sub_Department_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Created_Date` date NOT NULL,
  `Created_Date_And_Time` datetime NOT NULL,
  `Supervisor_ID` int(11) DEFAULT NULL,
  `Branch_ID` int(11) DEFAULT NULL,
  `Previous_Stock_Taking_Data` text,
  PRIMARY KEY (`Stock_Taking_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Supervisor_ID` (`Supervisor_ID`),
  KEY `Sub_Department_ID` (`Sub_Department_ID`),
  KEY `Branch_ID` (`Branch_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_stocktaking_items`
--

DROP TABLE IF EXISTS `tbl_stocktaking_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stocktaking_items` (
  `Stock_Taking_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_ID` int(11) NOT NULL,
  `Over_Quantity` int(11) DEFAULT NULL,
  `Under_Quantity` int(11) DEFAULT NULL,
  `Item_Remark` varchar(200) DEFAULT NULL,
  `Stock_Taking_ID` int(11) NOT NULL,
  `Buying_Price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`Stock_Taking_Item_ID`),
  KEY `Item_ID` (`Item_ID`),
  KEY `Stock_Taking_ID` (`Stock_Taking_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_store_order_approval_process`
--

DROP TABLE IF EXISTS `tbl_store_order_approval_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_store_order_approval_process` (
  `Store_Approval_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Store_Order_ID` int(11) DEFAULT NULL,
  `Employee_ID` int(11) DEFAULT NULL,
  `Approval_ID` int(11) DEFAULT NULL,
  `Approval_Date_Time` datetime NOT NULL,
  `Approval_Title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Store_Approval_ID`),
  KEY `Store_Order_ID` (`Store_Order_ID`),
  KEY `Employee_ID` (`Employee_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_store_order_items`
--

DROP TABLE IF EXISTS `tbl_store_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_store_order_items` (
  `Order_Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Store_Order_ID` int(11) DEFAULT NULL,
  `Quantity_Required` int(11) NOT NULL,
  `Container_Qty` int(11) NOT NULL DEFAULT '0',
  `Items_Qty` int(11) NOT NULL DEFAULT '0',
  `Items_Per_Container` int(11) NOT NULL DEFAULT '0' COMMENT 'Items per containers issued',
  `Last_Buying_Price` int(11) NOT NULL DEFAULT '0',
  `Item_Remark` varchar(60) DEFAULT NULL,
  `Item_ID` int(11) DEFAULT NULL,
  `Quantity_Issued` int(11) NOT NULL,
  `Container_Issued` int(11) NOT NULL DEFAULT '0',
  `Quantity_Received` int(11) NOT NULL,
  `Issue_ID` int(11) DEFAULT NULL,
  `Item_Status` varchar(20) NOT NULL DEFAULT 'active',
  `Status` varchar(10) NOT NULL DEFAULT 'active',
  `Procurement_Status` varchar(20) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`Order_Item_ID`),
  KEY `Requizition_ID` (`Store_Order_ID`),
  KEY `item_id` (`Item_ID`),
  KEY `Issue_ID` (`Issue_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_store_orders`
--

DROP TABLE IF EXISTS `tbl_store_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_store_orders` (
  `Store_Order_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Order_Description` varchar(150) DEFAULT NULL,
  `Created_Date_Time` datetime DEFAULT NULL,
  `Created_Date` date NOT NULL,
  `Sent_Date_Time` datetime DEFAULT NULL COMMENT 'Submitted date',
  `Sub_Department_ID` int(11) DEFAULT NULL COMMENT 'Store Need / Requested',
  `Employee_ID` int(11) DEFAULT NULL,
  `Order_Status` varchar(40) DEFAULT 'pending',
  `Control_Status` varchar(20) NOT NULL DEFAULT 'available',
  `Branch_ID` int(11) NOT NULL,
  `Store_Order_Type` varchar(50) DEFAULT NULL,
  `Approval_Level` int(11) DEFAULT NULL,
  `Approvals` int(11) DEFAULT NULL,
  PRIMARY KEY (`Store_Order_ID`),
  KEY `Store_Need` (`Sub_Department_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Branch_ID` (`Branch_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sub_department`
--

DROP TABLE IF EXISTS `tbl_sub_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sub_department` (
  `Sub_Department_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Department_ID` int(11) DEFAULT NULL,
  `Sub_Department_Name` varchar(150) NOT NULL,
  PRIMARY KEY (`Sub_Department_ID`),
  UNIQUE KEY `Sub_Department_Name` (`Sub_Department_Name`),
  KEY `Department_ID` (`Department_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_supplier`
--

DROP TABLE IF EXISTS `tbl_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_supplier` (
  `Supplier_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Supplier_Name` varchar(120) NOT NULL,
  `Supplier_Address` varchar(200) DEFAULT NULL,
  `Mobile_Number` varchar(40) DEFAULT NULL,
  `Supplier_Email` varchar(100) DEFAULT NULL,
  `Physical_Address` text,
  `Postal_Address` text,
  `Telephone` varchar(255) DEFAULT NULL,
  `Fax` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Supplier_ID`),
  UNIQUE KEY `Supplier_Name` (`Supplier_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_system_configuration`
--

DROP TABLE IF EXISTS `tbl_system_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_system_configuration` (
  `Configuration_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Branch_ID` int(11) DEFAULT NULL,
  `Centralized_Collection` varchar(20) DEFAULT NULL,
  `Departmental_Collection` varchar(20) DEFAULT NULL,
  `Reception_Picking_Items` varchar(10) NOT NULL DEFAULT 'no',
  `Show_Pharmaceutical_Before_Payments` varchar(10) NOT NULL DEFAULT 'yes',
  `Display_Send_To_Cashier_Button` varchar(10) NOT NULL DEFAULT 'no',
  `Enable_Add_More_Medication` varchar(10) NOT NULL DEFAULT 'no',
  `Enable_Inpatient_To_Check_Again` varchar(10) NOT NULL DEFAULT 'no',
  `Allow_Direct_Cash_Outpatient` varchar(10) NOT NULL DEFAULT 'no',
  `Change_Medication_Location` varchar(10) NOT NULL DEFAULT 'no',
  `Filtered_Pharmacy_Patient_List` varchar(10) NOT NULL DEFAULT 'yes',
  `Allow_Cashier_To_Approve_Pharmaceutical` varchar(10) NOT NULL DEFAULT 'no',
  `Pharmacy_Patient_List_Displays_Only_Current_Checked_In` varchar(10) NOT NULL DEFAULT 'no',
  `Mobile_Payment` varchar(20) DEFAULT 'no',
  `Inpatient_Prepaid` varchar(20) NOT NULL DEFAULT 'yes',
  `price_precision` varchar(20) NOT NULL DEFAULT 'no',
  `Imbalance_Discharge` varchar(20) NOT NULL DEFAULT 'no',
  `Departmental_Stock_Movement` varchar(10) NOT NULL DEFAULT 'yes',
  `Hospital_Name` varchar(70) DEFAULT NULL,
  `Box_Address` varchar(50) DEFAULT NULL,
  `Telephone` varchar(50) DEFAULT NULL,
  `Cell_Phone` varchar(50) DEFAULT NULL,
  `Fax` varchar(50) DEFAULT NULL,
  `Tin` varchar(50) DEFAULT NULL,
  `Direct_departmental_payments` varchar(10) NOT NULL DEFAULT 'no',
  `Approval_Levels` int(11) NOT NULL DEFAULT '0',
  `currency_id` int(11) DEFAULT NULL,
  `Editable_Quantity_Received` varchar(10) NOT NULL DEFAULT 'no',
  `Store_Order_Add_Items_By_Pop_Up` varchar(4) NOT NULL DEFAULT 'no',
  `enable_receive_by_package` varchar(10) NOT NULL DEFAULT 'no',
  `enable_zeroing_price` varchar(10) NOT NULL DEFAULT 'no',
  `Expire_Password_Days` int(11) NOT NULL,
  `Allow_login_failure_Count` varchar(10) NOT NULL DEFAULT 'no',
  `minimum_password_length` int(11) NOT NULL,
  `alphanumeric_password` varchar(10) NOT NULL DEFAULT 'no',
  `Change_password_first_login` varchar(10) NOT NULL DEFAULT 'no',
  `All_Items_Payments` varchar(10) NOT NULL DEFAULT 'no',
  `Reception_Must_Fill_Exemption_Missing_Information` varchar(5) NOT NULL DEFAULT 'no',
  `Dispense_Credit_Patients_after_24_hrs` varchar(5) NOT NULL DEFAULT 'no',
  `Allow_Pharmaceutical_Dispensing_Above_Actual_Balance` varchar(5) NOT NULL DEFAULT 'yes',
  `Allow_Aditional_Instructions_On_Pharmacy_Menu` varchar(5) NOT NULL DEFAULT 'no',
  `Allow_Pharmacy_To_Dispense_Multiple_Patients` varchar(5) NOT NULL DEFAULT 'no',
  `Pharmacy_Additional_Instruction` varchar(50) NOT NULL DEFAULT '50',
  PRIMARY KEY (`Configuration_ID`),
  KEY `Branch_ID` (`Branch_ID`),
  KEY `currency_id` (`currency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_system_notification`
--

DROP TABLE IF EXISTS `tbl_system_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_system_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_system_notification_employee`
--

DROP TABLE IF EXISTS `tbl_system_notification_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_system_notification_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_notification_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `system_notification_employee_employee_id_foreign` (`employee_id`),
  KEY `system_notification_employee_system_notification_id_foreign` (`system_notification_id`),
  CONSTRAINT `system_notification_employee_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`Employee_ID`),
  CONSTRAINT `system_notification_employee_system_notification_id_foreign` FOREIGN KEY (`system_notification_id`) REFERENCES `tbl_system_notification` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_system_notification_log`
--

DROP TABLE IF EXISTS `tbl_system_notification_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_system_notification_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_notification_id` int(11) NOT NULL,
  `audit_log_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `system_notification_log_audit_log_id_foreign` (`audit_log_id`),
  KEY `system_notification_log_system_notification_id_foreign` (`system_notification_id`),
  CONSTRAINT `system_notification_log_audit_log_id_foreign` FOREIGN KEY (`audit_log_id`) REFERENCES `tbl_audit_log` (`id`),
  CONSTRAINT `system_notification_log_system_notification_id_foreign` FOREIGN KEY (`system_notification_id`) REFERENCES `tbl_system_notification` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_system_setting`
--

DROP TABLE IF EXISTS `tbl_system_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_system_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `property_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `property_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_test_result_modification`
--

DROP TABLE IF EXISTS `tbl_test_result_modification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_test_result_modification` (
  `result_modification_ID` int(11) NOT NULL AUTO_INCREMENT,
  `test_result_ID` int(11) NOT NULL,
  `Parameter` int(11) NOT NULL,
  `employee_ID` int(11) NOT NULL,
  `result` varchar(50) NOT NULL,
  `timeModified` datetime NOT NULL,
  PRIMARY KEY (`result_modification_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_test_results`
--

DROP TABLE IF EXISTS `tbl_test_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_test_results` (
  `test_result_ID` int(11) NOT NULL AUTO_INCREMENT,
  `payment_item_ID` int(11) NOT NULL,
  `Employee_ID` int(11) NOT NULL,
  `TimeSubmitted` datetime NOT NULL,
  `organismName` varchar(100) NOT NULL,
  `reagentUsed` varchar(100) NOT NULL,
  `resultType` varchar(30) NOT NULL,
  `datesaved` datetime NOT NULL,
  `remarks` text NOT NULL,
  `date_removed` datetime DEFAULT NULL,
  `staff_removed` int(11) DEFAULT NULL,
  `removed_status` varchar(10) NOT NULL DEFAULT 'No',
  `labTechn` int(11) NOT NULL,
  PRIMARY KEY (`test_result_ID`),
  KEY `staff_removed` (`staff_removed`),
  KEY `payment_item_ID` (`payment_item_ID`),
  KEY `Employee_ID` (`Employee_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_testing_record`
--

DROP TABLE IF EXISTS `tbl_testing_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_testing_record` (
  `Testing_record_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Registration_ID` int(11) NOT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `employee_ID` int(11) DEFAULT NULL,
  `Days` varchar(25) NOT NULL,
  `date` datetime NOT NULL,
  `type` varchar(25) NOT NULL,
  `meal` varchar(25) NOT NULL,
  `notes` varchar(500) NOT NULL,
  `Glucose` varchar(25) NOT NULL,
  PRIMARY KEY (`Testing_record_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `employee_ID` (`employee_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_tests_parameters`
--

DROP TABLE IF EXISTS `tbl_tests_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tests_parameters` (
  `ref_parameter_ID` int(11) NOT NULL,
  `ref_item_ID` int(11) NOT NULL,
  PRIMARY KEY (`ref_item_ID`,`ref_parameter_ID`),
  KEY `ref_parameter_ID` (`ref_parameter_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_tests_parameters_results`
--

DROP TABLE IF EXISTS `tbl_tests_parameters_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tests_parameters_results` (
  `ref_test_result_ID` int(11) NOT NULL,
  `parameter` int(11) NOT NULL,
  `result` varchar(50) NOT NULL,
  `modified` varchar(10) NOT NULL DEFAULT 'No',
  `Validated` varchar(10) NOT NULL DEFAULT 'No',
  `Submitted` varchar(10) NOT NULL DEFAULT 'No',
  `status` varchar(30) NOT NULL,
  `Saved` varchar(10) NOT NULL DEFAULT 'No',
  `TimeSubmitted` datetime DEFAULT NULL,
  `TimeValidate` datetime DEFAULT NULL,
  `SavedBy` int(11) DEFAULT '0',
  `ValidatedBy` int(11) DEFAULT '0',
  `results_description` text,
  PRIMARY KEY (`ref_test_result_ID`,`parameter`),
  KEY `parameter` (`parameter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_tests_specimen`
--

DROP TABLE IF EXISTS `tbl_tests_specimen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tests_specimen` (
  `tests_specimen_ID` int(11) NOT NULL AUTO_INCREMENT,
  `tests_item_ID` int(11) NOT NULL,
  `ref_specimen_ID` int(11) NOT NULL,
  PRIMARY KEY (`tests_specimen_ID`),
  KEY `tests_item_ID` (`tests_item_ID`),
  KEY `ref_specimen_ID` (`ref_specimen_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=307 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_tibanova_invoice`
--

DROP TABLE IF EXISTS `tbl_tibanova_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tibanova_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(50) DEFAULT NULL,
  `invoice_client_id` varchar(50) NOT NULL,
  `invoice_client` varchar(250) DEFAULT NULL,
  `month_serial` varchar(50) NOT NULL,
  `invoice_date` date NOT NULL,
  `invoice_patient_rate` int(11) NOT NULL,
  `invoice_total_patient_visit` int(11) NOT NULL,
  `invoice_amount` float NOT NULL,
  `invoice_email` text NOT NULL,
  `status` int(11) NOT NULL,
  `extended_due_date` date DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `invoice_minimum_patient_visit` int(11) NOT NULL,
  `invoice_summary_patient_visit` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_tracking_number`
--

DROP TABLE IF EXISTS `tbl_tracking_number`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tracking_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `month_year` varchar(6) NOT NULL,
  `tracking_number` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tracking_number_employee_id_foreign` (`employee_id`),
  KEY `tracking_number_sponsor_id_foreign` (`sponsor_id`),
  KEY `tracking_number_branch_id_foreign` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_user_role`
--

DROP TABLE IF EXISTS `tbl_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_user_role_permission`
--

DROP TABLE IF EXISTS `tbl_user_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user_role_permission` (
  `user_role_id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  KEY `user_role_permission_user_role_id_foreign` (`user_role_id`),
  KEY `user_role_permission_permission_id_foreign` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_vital`
--

DROP TABLE IF EXISTS `tbl_vital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_vital` (
  `vital_id` int(11) NOT NULL AUTO_INCREMENT,
  `vital` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `unit_of_measure` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`vital_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_ward_round`
--

DROP TABLE IF EXISTS `tbl_ward_round`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ward_round` (
  `Round_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Employee_ID` int(11) NOT NULL,
  `Registration_ID` int(11) NOT NULL,
  `consultation_ID` int(11) DEFAULT NULL,
  `Patient_Payment_ID` int(11) DEFAULT NULL,
  `Findings` longtext,
  `Comment_For_Laboratory` mediumtext,
  `Comment_For_Radiology` mediumtext,
  `Comment_For_Procedure` varchar(200) DEFAULT NULL,
  `Comment_For_Surgery` varchar(200) DEFAULT NULL,
  `investigation_comments` varchar(45) DEFAULT NULL,
  `remarks` text,
  `Process_Status` varchar(45) DEFAULT NULL,
  `Ward_Round_Date_And_Time` datetime DEFAULT NULL,
  `Patient_Type` varchar(45) DEFAULT NULL,
  `Comment_For_Pharmacy` int(1) DEFAULT NULL,
  `Admision_ID` int(11) NOT NULL,
  `Payment_Cache_ID` int(11) NOT NULL,
  `Cash_Payment_Cache_ID` int(11) DEFAULT NULL,
  `Comments_For_Pharmacy` text,
  `Comments_For_Procedure` text,
  `Comments_For_Surgery` text,
  `Require_Spectacle` varchar(2) DEFAULT NULL,
  `Need_Dialysis` varchar(2) DEFAULT NULL,
  `Need_ED` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`Round_ID`),
  KEY `consultation_ID` (`consultation_ID`),
  KEY `Registration_ID` (`Registration_ID`),
  KEY `Employee_ID` (`Employee_ID`),
  KEY `Patient_Payment_ID` (`Patient_Payment_ID`),
  KEY `ward_round_admision_id_foreign` (`Admision_ID`),
  KEY `ward_round_payment_cache_id_foreign` (`Payment_Cache_ID`),
  KEY `Cash_Payment_Cache_ID` (`Cash_Payment_Cache_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_ward_round_disease`
--

DROP TABLE IF EXISTS `tbl_ward_round_disease`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ward_round_disease` (
  `Ward_Round_Disease_ID` int(11) NOT NULL AUTO_INCREMENT,
  `disease_ID` int(11) NOT NULL,
  `Round_ID` int(11) NOT NULL,
  `diagnosis_type` varchar(45) DEFAULT NULL,
  `Round_Disease_Date_And_Time` datetime DEFAULT NULL,
  PRIMARY KEY (`Ward_Round_Disease_ID`),
  KEY `disease_ID` (`disease_ID`),
  KEY `Round_ID` (`Round_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `view_last_buying_price`
--

DROP TABLE IF EXISTS `view_last_buying_price`;
/*!50001 DROP VIEW IF EXISTS `view_last_buying_price`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_last_buying_price` AS SELECT 
 1 AS `Item_ID`,
 1 AS `Quantity_Received`,
 1 AS `Buying_Price`,
 1 AS `Created_Date_Time`,
 1 AS `Supplier_ID`,
 1 AS `Supplier_Name`,
 1 AS `Product_Name`,
 1 AS `Product_Code`,
 1 AS `Unit_Of_Measure`,
 1 AS `Classification`,
 1 AS `Grn_Status`,
 1 AS `Doc_ID`,
 1 AS `Doc_Type`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_last_buying_price`
--

/*!50001 DROP VIEW IF EXISTS `view_last_buying_price`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_last_buying_price` AS (select `wpoi`.`Item_ID` AS `Item_ID`,`wpoi`.`Quantity_Required` AS `Quantity_Received`,`wpoi`.`Price` AS `Buying_Price`,`grn`.`Grn_Date_And_Time` AS `Created_Date_Time`,`grn`.`Supplier_ID` AS `Supplier_ID`,`s`.`Supplier_Name` AS `Supplier_Name`,`i`.`Product_Name` AS `Product_Name`,`i`.`Product_Code` AS `Product_Code`,`i`.`Unit_Of_Measure` AS `Unit_Of_Measure`,`i`.`Classification` AS `Classification`,`grn`.`Grn_Status` AS `Grn_Status`,`wpoi`.`Grn_ID` AS `Doc_ID`,'GRN WITHOUT PURCHASE ORDER' AS `Doc_Type` from (((`tbl_grn_without_purchase_order_items` `wpoi` join `tbl_grn_without_purchase_order` `grn`) join `tbl_supplier` `s`) join `tbl_items` `i`) where ((`wpoi`.`Purchase_Order_Item_ID` is not null) and (`wpoi`.`Grn_ID` = `grn`.`Grn_ID`) and (`wpoi`.`Item_ID` = `i`.`Item_ID`) and (`grn`.`Supplier_ID` = `s`.`Supplier_ID`))) union all (select `ppoi`.`Item_ID` AS `Item_ID`,`ppoi`.`Quantity_Received` AS `Quantity_Received`,`ppoi`.`Buying_Price` AS `Buying_Price`,`grn`.`Created_Date_Time` AS `Created_Date_Time`,`po`.`Supplier_ID` AS `Supplier_ID`,`s`.`Supplier_Name` AS `Supplier_Name`,`i`.`Product_Name` AS `Product_Name`,`i`.`Product_Code` AS `Product_Code`,`i`.`Unit_Of_Measure` AS `Unit_Of_Measure`,`i`.`Classification` AS `Classification`,'saved' AS `Grn_Status`,`ppoi`.`Grn_Purchase_Order_ID` AS `Doc_ID`,'GRN WITH PURCHASE ORDER' AS `Doc_Type` from ((((`tbl_pending_purchase_order_items` `ppoi` join `tbl_purchase_order` `po`) join `tbl_grn_purchase_order` `grn`) join `tbl_supplier` `s`) join `tbl_items` `i`) where ((`ppoi`.`Order_Item_ID` is not null) and (`ppoi`.`Grn_Purchase_Order_ID` = `grn`.`Grn_Purchase_Order_ID`) and (`ppoi`.`Purchase_Order_ID` = `po`.`Purchase_Order_ID`) and (`po`.`Supplier_ID` = `s`.`Supplier_ID`) and (`ppoi`.`Item_ID` = `i`.`Item_ID`) and (`ppoi`.`Grn_Status` in ('RECEIVED','PENDING')))) union all (select `poi`.`Item_ID` AS `Item_ID`,`poi`.`Quantity_Received` AS `Quantity_Received`,`poi`.`Buying_Price` AS `Buying_Price`,`grn`.`Created_Date_Time` AS `Created_Date_Time`,`po`.`Supplier_ID` AS `Supplier_ID`,`s`.`Supplier_Name` AS `Supplier_Name`,`i`.`Product_Name` AS `Product_Name`,`i`.`Product_Code` AS `Product_Code`,`i`.`Unit_Of_Measure` AS `Unit_Of_Measure`,`i`.`Classification` AS `Classification`,'saved' AS `Grn_Status`,`grn`.`Grn_Purchase_Order_ID` AS `Doc_ID`,'GRN WITH PURCHASE ORDER' AS `Doc_Type` from ((((`tbl_purchase_order_items` `poi` join `tbl_purchase_order` `po`) join `tbl_grn_purchase_order` `grn`) join `tbl_supplier` `s`) join `tbl_items` `i`) where ((`poi`.`Order_Item_ID` is not null) and (`grn`.`Purchase_Order_ID` = `poi`.`Purchase_Order_ID`) and (`grn`.`Purchase_Order_ID` = `po`.`Purchase_Order_ID`) and (`po`.`Supplier_ID` = `s`.`Supplier_ID`) and (`poi`.`Item_ID` = `i`.`Item_ID`) and (`poi`.`Grn_Status` in ('RECEIVED','PENDING')))) union all (select `gobi`.`Item_ID` AS `Item_ID`,`gobi`.`Item_Quantity` AS `Item_Quantity`,`gobi`.`Buying_Price` AS `Buying_Price`,`gob`.`Created_Date_Time` AS `Created_Date_Time`,'' AS `Supplier_ID`,'' AS `Supplier_Name`,`i`.`Product_Name` AS `Product_Name`,`i`.`Product_Code` AS `Product_Code`,`i`.`Unit_Of_Measure` AS `Unit_Of_Measure`,`i`.`Classification` AS `Classification`,'saved' AS `Grn_Status`,`gob`.`Grn_Open_Balance_ID` AS `Doc_ID`,'GRN AS OPEN BALANCE' AS `Doc_Type` from ((`tbl_grn_open_balance_items` `gobi` join `tbl_grn_open_balance` `gob`) join `tbl_items` `i`) where ((`gobi`.`Open_Balance_Item_ID` is not null) and (`gobi`.`Grn_Open_Balance_ID` = `gob`.`Grn_Open_Balance_ID`) and (`gobi`.`Item_ID` = `i`.`Item_ID`) and (`gob`.`Grn_Open_Balance_Status` in ('saved','edited')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-23 15:28:30
