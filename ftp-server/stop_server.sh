#!/bin/bash

echo "STOP CONTAINER tibanova-installation"
docker stop tibanova-installation

echo "REMOVE CONTAINER tibanova-installation"
docker rm tibanova-installation