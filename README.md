# TIBANOVA INSTALLATION

This is the tibanova installation scripts
Please fun the following to execute the installation

## Initialization setup
1. Download the tibanova.installation.zip
2. Unzip tibanova.installation.zip
3. using terminal
3.1 cd tibanova.installation
3.2 sudo chmod +x install*
3.3 sudo ./install.sh

## Initialize database
1. using terminal
1.1 cd tibanova.installation
1.2 sudo chmod +x install*
1.3 edit BRANCH_NAME AND COMPANY_NAME in install_database.sh
1.4 sudo ./install_database.sh

## Updating
1. Download the tibanova.installation.zip
2. Unzip tibanova.installation.zip
3. using terminal
3.1 cd tibanova.installation
3.2 sudo chmod +x update*
3.3 edit htaccess-project | change APP_PROJECT_PATH "project" to correct project
3.4 edit htaccess-project | change APP_DB_HOST "localhost" to correct mysql ip address obtained after initializing database
or just (sudo chmod +x get_mysql_ip.sh && sudo ./get_mysql_ip.sh)
3.5 sudo ./update.sh