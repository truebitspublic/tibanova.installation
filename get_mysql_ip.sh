#!/bin/bash

echo "GET MYSQL DATABASE IP"
sudo docker inspect --format '{{ .NetworkSettings.IPAddress }}' tibanova-mysql > mysql_ip
mysqlip=$(<mysql_ip)
echo "MYSQL IP ADDRESS IS $mysqlip"
rm -rf mysql_ip
