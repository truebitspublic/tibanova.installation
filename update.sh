#!/bin/bash

echo "CLEAN TIBANOVA"
sudo rm -rf tibanova*

echo "DOWNLOAD TIBANOVA"
sudo wget https://gitlab.com/truebitspublic/tibanova_app/-/archive/php7/tibanova_app-php7.zip

echo "UNZIP TIBANOVA"
sudo unzip tibanova_app-php7.zip
sudo mv tibanova_app-php7 tibanova

echo "COPY TIBANOVA APPLICATION"
sudo docker cp tibanova tibanova-webserver:/home/app/htdocs/

echo "COPY TIBANOVA HTACCESS"
sudo docker cp htaccess-project tibanova-webserver:/home/app/htdocs/tibanova/.htaccess
#sudo docker exec tibanova-webserver bash -c "cp -r /home/app/htdocs/vendor/ /home/app/htdocs/tibanova/"

echo "RESTART TIBANOVA-WEBSERVER"
sudo docker restart tibanova-webserver

echo "CLEAN RUBBISH"
sudo rm -rf tibanova*

echo "RE-RUN COMPOSER INSTALL"
sudo ./install_vendor.sh
