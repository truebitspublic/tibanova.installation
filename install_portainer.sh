#!/bin/bash

echo "INSTALL Portainer"
sudo docker run -d -p 1988:8000 -p 1410:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer