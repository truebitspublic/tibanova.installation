#!/bin/bash

echo "IF MYSQL CONTAINER EXIST STOP AND REMOVE"
sudo docker stop tibanova-mysql
sudo docker rm tibanova-mysql

echo "CREATE AND START MYSQL DOCKER CONTAINER"
sudo docker run -p 3306:3306 --name tibanova-mysql -e MYSQL_ROOT_PASSWORD=PA55w0rd -e TZ="Africa/Dar_es_Salaam" -d --restart always -v /home/app/tools/mysql/data:/var/lib/mysql -v /home/app/tools/mysql/conf:/etc/mysql/conf.d mysql:5.7

echo "ADD MYSQL CONFIG"
sudo cp my.cnf /home/app/tools/mysql/conf

echo "RESTART MYSQL"
sudo docker restart tibanova-mysql

echo "ENSURE ADDED TO NETWORK"
sudo chmod +x install_network.sh
sudo ./install_network.sh
