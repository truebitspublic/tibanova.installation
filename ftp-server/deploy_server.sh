#!/bin/bash

echo "STOP CONTAINER tibanova-installation"
docker stop tibanova-installation

echo "REMOVE CONTAINER tibanova-installation"
docker rm tibanova-installation

echo "BUILD CONTAINER tibanova-installation"
docker run -d --name=tibanova-installation -p 21:21 -p 21000-21010:21000-21010 -e USERS="tibanova|tibanovaPA55w0rd|/home/tibanova" -v /home/app/tools/tibanova-installation/home:/home --restart unless-stopped delfer/alpine-ftp-server