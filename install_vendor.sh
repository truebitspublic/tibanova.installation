#!/bin/bash

echo "RUNNNG COMPOSER INSTALL"
sudo docker exec tibanova-webserver bash -c "cd /home/app/htdocs/tibanova && rm -rf composer.lock && composer install"

echo "CHANGE PERMISSION ON MPDF TEMP FOLDER"
sudo docker exec tibanova-webserver bash -c "chmod 777 /home/app/htdocs/tibanova/vendor/mpdf/mpdf/tmp"
