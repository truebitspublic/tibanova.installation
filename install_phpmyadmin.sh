#!/bin/bash

echo "REMOVE IF EXIST"
sudo rm -rf phpMyAdmin*

echo "DOWNLOAD PHPMYADMIN"
wget https://files.phpmyadmin.net/phpMyAdmin/4.9.5/phpMyAdmin-4.9.5-all-languages.zip

echo "EXTRACT PHPMYADMIN"
unzip phpMyAdmin-4.9.5-all-languages.zip

echo "GET MYSQL DATABASE IP"
sudo docker inspect --format '{{ .NetworkSettings.IPAddress }}' tibanova-mysql > mysql_ip

echo "CHANGE CONFIG FILE"
mv phpMyAdmin-4.9.5-all-languages/config.sample.inc.php phpMyAdmin-4.9.5-all-languages/config.inc.php

mysqlip=$(<mysql_ip)
echo "MYSQL IP ADDRESS IS $mysqlip"

echo "PUT NEW IP ADDRESS"
sudo awk -v mysqlip="$mysqlip" '{gsub("localhost", mysqlip)}1'  phpMyAdmin-4.9.5-all-languages/config.inc.php > temp.txt && mv temp.txt  phpMyAdmin-4.9.5-all-languages/config.inc.php
#cat  phpMyAdmin-4.9.5-all-languages/config.inc.php

#echo "PUT NEW PORT"
#sudo awk '{gsub("3306", 3307)}1'  phpMyAdmin-4.9.5-all-languages/config.inc.php > temp.txt && mv temp.txt  phpMyAdmin-4.9.5-all-languages/config.inc.php

echo "INSTALL PHPMYADMIN"
sudo docker exec -it tibanova-webserver rm -rf /home/app/htdocs/myadmin
sudo docker cp  phpMyAdmin-4.9.5-all-languages tibanova-webserver:/home/app/htdocs/myadmin

echo "CLEAN"
sudo rm -rf phpMyAdmin*
sudo rm -rf mysql_ip
