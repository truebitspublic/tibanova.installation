#!/bin/bash

echo "IF TIBANOVA WEBSERVER CONTAINER EXIST STOP AND REMOVE"
sudo docker stop tibanova-webserver
sudo docker rm tibanova-webserver

echo "CREATE AND START TIBANOVA WEBSERVER DOCKER CONTAINER"
sudo docker run -d -p 8080:80 --restart always --name tibanova-webserver tibanova-webserver:latest

echo "ENSURE ADDED TO NETWORK"
sudo chmod +x install_network.sh
sudo ./install_network.sh
