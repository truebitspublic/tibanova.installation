#!/bin/bash

#INSTALL DOCKER FIRST
sudo ./install_docker.sh

#PREPARE FOLDER
mkdir /home/app/
mkdir /home/app/tools

#PULL MYSQL DOCKER IMAGE
sudo docker pull mysql:5.7

#IF MYSQL CONTAINER EXIST STOP AND REMOVE
#sudo docker stop tibanova-mysql
#sudo docker rm tibanova-mysql

#CREATE AND START MYSQL DOCKER CONTAINER
#sudo docker run -p 3306:3306 --name tibanova-mysql -e MYSQL_ROOT_PASSWORD=PA55w0rd -d --restart always -v /home/app/tools/mysql/data:/var/lib/mysql -v /home/app/tools/mysql/conf:/etc/mysql/conf.d mysql:5.7

#ADD MYSQL CONFIG
#sudo cp my.cnf /home/app/tools/mysql/conf

#RESTART MYSQL
#sudo docker restart tibanova-mysql
echo "RUN MYSQL SCRIPT"
sudo chmod +x restart_mysql.sh
sudo ./restart_mysql.sh


#PULL PHP-APACHE IMAGE
sudo docker pull php:7.2-apache

#BUILD TIBANOVA WEBSERVER
sudo docker build -t tibanova-webserver .

#IF TIBANOVA WEBSERVER CONTAINER EXIST STOP AND REMOVE
#sudo docker stop tibanova-webserver
#sudo docker rm tibanova-webserver

#CREATE AND START TIBANOVA WEBSERVER DOCKER CONTAINER
#sudo docker run -d -p 80:80 --restart always --name tibanova-webserver tibanova-webserver:latest
echo "RUN WEBSERVER SCRIPT"
sudo chmod +x restart_webserver.sh
sudo ./restart_webserver.sh

sudo rm -rf tibanova*
sudo wget https://gitlab.com/truebitspublic/tibanova_app/-/archive/php7/tibanova_app-php7.zip
sudo unzip tibanova_app-php7.zip
sudo mv tibanova_app-php7 tibanova

sudo docker exec -it tibanova-webserver mkdir -p /home/app/htdocs
sudo docker cp tibanova tibanova-webserver:/home/app/htdocs/
sudo docker cp htaccess tibanova-webserver:/home/app/htdocs/tibanova/.htaccess
sudo docker cp loader-wizard.php tibanova-webserver:/home/app/htdocs/
sudo docker cp ioncube tibanova-webserver:/usr/local/lib/php/extensions/no-debug-non-zts-20170718
sudo docker cp docker-php-ext-ioncube.ini tibanova-webserver:/usr/local/etc/php/conf.d

sudo docker restart tibanova-webserver
