#!/bin/bash

echo "CREATE TIBANOVA NETWORK"
sudo docker network create tibanova

echo "ADD WEBSERVER TO TIBANOVA NETWORK"
sudo docker network connect tibanova tibanova-webserver

echo "ADD MYSQL TO TIBANOVA NETWORK"
sudo docker network connect tibanova tibanova-mysql
