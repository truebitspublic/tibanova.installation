#!/bin/bash

REMOTE_PASS=PA55w0rd
REMOTE_USER=root
REMOTE_HOST=apps.truebitstech.com
REMOTE_DB=tibanova

LOCAL_PASS=PA55w0rd
LOCAL_USER=root
LOCAL_HOST=127.0.0.1
LOCAL_DB=tibanova

LOCAL_NEW_USER=tibanova
LOCAL_NEW_PASS=tibanova#123

DUMP_FILE=/home/structure_data.sql
EXCLUDED_TABLES=(
tbl_employee
tbl_privileges
tbl_branch_employee
tbl_audit_trail
tbl_pre_operative_checklist
tbl_pf3_patients
tbl_patient_cache_results
tbl_items_template_results
tbl_store_orders
tbl_check_in_details
tbl_folio_number
tbl_patient_payment_test
tbl_hospital_course_injuries
tbl_nursecommunication_observation
tbl_issues
tbl_tests_specimen
tbl_git_post_operative_participant
tbl_company
tbl_radiology_image
tbl_doctor_rch
tbl_sponsor_allow_zero_items
tbl_dialysis_patient_assessment
tbl_patient_blood_data
tbl_consultation
tbl_attachment
tbl_items_template
tbl_check_in
tbl_referral_patient
tbl_post_operative_participant
tbl_folio
tbl_patient_payment_item_list
tbl_gti_post_operative_diagnosis
tbl_nurse_notes
tbl_issuemanual_items
tbl_clinic_employee
tbl_radiology_discription_old
tbl_labour_ward_notes_history
tbl_dialysis_observation_chart
tbl_budget
tbl_tests_parameters_results
tbl_patient_bill
tbl_approval_level
tbl_items_balance
tbl_budget_items
tbl_referral_hosp
tbl_post_operative_notes
tbl_store_order_items
tbl_grnissue
tbl_family_social_history
tbl_patient_past_medical_history
tbl_radiology_discription
tbl_labour_ward_notes
tbl_spectacles
tbl_dialysis_notes
tbl_broadcast_message
tbl_clinic_assign_consultation_category_fee
tbl_approval_employee
tbl_radiology_patient_tests
tbl_store_order_approval_process
tbl_grn_without_purchase_order_items_history
tbl_disease_consultation
tbl_excuse_of_duty
tbl_patient_nursecare
tbl_return_outward_items
tbl_purchase_order_items
tbl_labour
tbl_specimen_results
tbl_dialysis_machine_assessment
tbl_branches
tbl_testing_record
tbl_clinic
tbl_radiology_parameter_old
tbl_pending_purchase_order_items
tbl_paediatric_observation
tbl_admission
tbl_grn_without_purchase_order_items
tbl_employee_type
tbl_patient_histrory
tbl_nurse_care
tbl_stocktaking_items
tbl_laboratory_test_specimen
tbl_scheduler
tbl_dialysis_heparain
tbl_test_results
tbl_claim_process_status_employee
tbl_return_outward
tbl_purchase_order_approval_process
tbl_patient_vital
tbl_radiology_parameter_cache
tbl_pending_purchase_order
tbl_paediatric_history
tbl_item_report_template
tbl_doctor_consultation_fee
tbl_grn_without_purchase_order
tbl_patient_file_view
tbl_nurse
tbl_stocktaking
tbl_post_operative_external_participant
tbl_dialysis_dialysis_orders
tbl_beds
tbl_test_result_modification
tbl_claim_process_map_sponsor_items
tbl_return_inward_items
tbl_purchase_order
tbl_laboratory_test_parameters
tbl_radiology_parameter
tbl_payment_item_list_cache
tbl_paediatric
tbl_item_report
tbl_patient_transfer
tbl_discharge_summary
tbl_employee_document
tbl_patient_disease
tbl_notification_user
tbl_stock_ledger_controler
tbl_grn_purchase_order
tbl_dialysis_details
tbl_baby_care_health
tbl_claim_process_claim_progress_status
tbl_return_inward
tbl_purchase_cache
tbl_laboratory_specimen
tbl_post_operative_diagnosis
tbl_payment_cache
tbl_optical_items_list_cache
tbl_item_price
tbl_disposal_items
tbl_patient_registration
tbl_discharge_reason
tbl_employee_assign_consultation_category_fee
tbl_patient_cache_test_specimen
tbl_notification_template
tbl_stock_balance_sub_departments
tbl_grn_purchase_cache
tbl_system_configuration
tbl_claim_item_price
tbl_requisition_items
tbl_procedure_post_operative_notes
tbl_post_operative_attachment
tbl_baby_care_exam
tbl_patient_vital_value
tbl_ogd_post_operative_notes
tbl_item_list_cache
tbl_disposal
tbl_patient_progress
tbl_notification
tbl_sponsor_verification_details
tbl_ward_round_disease
tbl_grn_open_balance_items
tbl_claim_item
tbl_requisition
tbl_post_operative
tbl_debt
tbl_baby_care
tbl_supplier
tbl_input_output_nursecommunication
tbl_ogd_post_operative_diagnosis
tbl_git_post_operative_notes
tbl_patient_payments
tbl_mulnutrition_observation
tbl_sponsor_pre_approval_items
tbl_tracking_number
tbl_ward_round
tbl_grn_open_balance
tbl_email_recepients
tbl_removed_specimen_patients
tbl_pre_operative_checklist_items
tbl_pf3_reasons
tbl_patient_cache_test
tbl_laboratory_parameters
tbl_claim_batch
tbl_nursecommunication_paediatric
tbl_issuesmanual
tbl_disease_sponsor_icd_standard
tbl_general_item_price
tbl_patient_payment_test_specimen
tbl_hospital_ward
tbl_directory
tbl_tibanova_invoice
tbl_grn_issue_note
tbl_radiology_image_old
tbl_document_library
tbl_message
tbl_sponsor_non_supported_items
)

IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_TABLES[@]}"
do :
   IGNORED_TABLES_STRING+=" --ignore-table=${REMOTE_DB}.${TABLE}"
done

BRANCH_NAME="MAIN BRANCH"
COMPANY_NAME="COMPANY NAME"

echo "DUMP STRUCTURE"
sudo docker exec -i tibanova-mysql bash -l -c "mysqldump -h ${REMOTE_HOST} -u ${REMOTE_USER} -p${REMOTE_PASS} ${REMOTE_DB} --single-transaction --no-data > ${DUMP_FILE}"

echo "DUMP DATA"
sudo docker exec -i tibanova-mysql bash -l -c "mysqldump -h ${REMOTE_HOST} -u ${REMOTE_USER} -p${REMOTE_PASS} ${REMOTE_DB} --no-create-info ${IGNORED_TABLES_STRING} >> ${DUMP_FILE}"

echo "DROP DATABASE ${REMOTE_DB}"
sudo docker exec -it tibanova-mysql mysql -h ${LOCAL_HOST} -u ${LOCAL_USER} -p${LOCAL_PASS} -e "DROP DATABASE ${LOCAL_DB}"

echo "CREATE DATABASE ${REMOTE_DB}"
sudo docker exec -it tibanova-mysql mysql -h ${LOCAL_HOST} -u ${LOCAL_USER} -p${LOCAL_PASS} -e "CREATE DATABASE ${LOCAL_DB}"

echo "CREATE USER"
sudo docker exec -it tibanova-mysql mysql -h ${LOCAL_HOST} -u ${LOCAL_USER} -p${LOCAL_PASS} -e "CREATE USER '${LOCAL_NEW_USER}'@'%' identified by '${LOCAL_NEW_PASS}'"
sudo docker exec -it tibanova-mysql mysql -h ${LOCAL_HOST} -u ${LOCAL_USER} -p${LOCAL_PASS} -e "GRANT ALL ON ${LOCAL_DB}.* to '${LOCAL_NEW_USER}'@'%'"

echo "RESTORE DATABASE"
sudo docker exec -i tibanova-mysql bash -l -c "mysql -h ${LOCAL_HOST} -u ${LOCAL_USER} -p${LOCAL_PASS} ${LOCAL_DB} < ${DUMP_FILE}"
sudo docker exec -i tibanova-mysql bash -l -c "rm -rf ${DUMP_FILE}"

echo "START DATA"
sudo docker exec -it tibanova-mysql mysql -h ${LOCAL_HOST} -u ${LOCAL_USER} -p${LOCAL_PASS} ${LOCAL_DB} -e "INSERT INTO tbl_employee (Employee_ID, Employee_Name, Employee_Type, Employee_Number, Employee_Title, Employee_Job_Code, Employee_Branch_Name, Department_ID, Account_Status, Branch_ID, user_role_id, Phone_Number, Registration_Number, written_employee_name, written_employee_signature, written_employee_qualification, written_employee_mct_registration_no, written_employee_phone_number) VALUES (1, 'TIBANOVA APP', 'Pharmacist', '', 'ICT', 'Others', 'TRUEBITS', '1', 'active', '1', '39', '255657878299', 'NA', NULL, NULL, NULL, NULL, NULL)"
sudo docker exec -it tibanova-mysql mysql -h ${LOCAL_HOST} -u ${LOCAL_USER} -p${LOCAL_PASS} ${LOCAL_DB} -e "INSERT INTO tbl_privileges (Given_Username, Given_Password, Employee_ID, last_modified_by, Last_Password_Change, Changed_first_login, session_id, last_logged_in_computer, last_login_date) VALUES ('truebits', 'c53e479b03b3220d3d56da88c4cace20', '1', '1', '0000-00-00', 'no', '221f97bb042afb5a53df6eb8f5506698', '192.168.0.57', '2020-05-31 10:35:49')"
sudo docker exec -it tibanova-mysql mysql -h ${LOCAL_HOST} -u ${LOCAL_USER} -p${LOCAL_PASS} ${LOCAL_DB} -e "INSERT INTO tbl_company (Company_ID, Company_Name) VALUES (1, '${COMPANY_NAME}')"
sudo docker exec -it tibanova-mysql mysql -h ${LOCAL_HOST} -u ${LOCAL_USER} -p${LOCAL_PASS} ${LOCAL_DB} -e "INSERT INTO tbl_branches (Branch_ID, Branch_Name, BannerLink, Company_ID) VALUES (1, '${BRANCH_NAME}', NULL, 1)"
sudo docker exec -it tibanova-mysql mysql -h ${LOCAL_HOST} -u ${LOCAL_USER} -p${LOCAL_PASS} ${LOCAL_DB} -e "INSERT INTO tbl_branch_employee (Employee_ID, Branch_ID) VALUES (1, 1)"
